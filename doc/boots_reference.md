# Boots reference

EARLY NOTES. PROJECT IS IN THE PLANNING STAGE.

Version: unreleased (0.0.0-0)

Boots is a low-level 'assembly language'-style virtual machine (VM) that is easy to implement.

## Introduction

Boots is a target language that is easy to implement on a wide variety of platforms, even 'on top of'/within an existing high-level languages such as Python.

Highlights:
- Architecture:
  - 3-operand fixed-length register machine
  - 60 instructions, strictly typed
  - signed 32-bit integers
  - RISC-like (addressing mode is determined by the instruction, and only load/store instructions access non-register memory)
  - Some details:
    - 31 registers, and a zero register
    - 32-bit instruction encoding
    - branch range +- 64 instructions
    - jump range   +- ~512 million instructions (+-2GiB) (via signed 32-bit jump offsets embedded in instruction stream)
- Portable:
  - all control flow is PC-relative
  - opaque pointer representation
  - integers and pointers both have implementation-dependent sizes in memory, and may be different sizes
- Extensions:
  - the Boot extension specifies additional instructions, encodings, functionality, and conventions (see boot_reference.md)
  - Boot can be further extended to LOVM, another 'assembly language'-style virtual machine which is more featureful but more complex (see lovm_reference.md)

## Table of Contents
[[_TOC_]]

## Architecture

###### Instruction encoding
4 bytes per instruction. The bytes contain fields:
- first byte:  opcode
- second byte: opfield0
- third byte:  opfield1
- fourth byte: opfield2

Some instructions take an additional 32-bit constant immediately following them. In such cases that 32-bit constant is in little-endian format.

Although each operand field spans a byte, the values inside it are limited to the range 0 <= value <= 127, and the most-significant-bit is always 0.

When two or more immediate operand fields are combined into a larger immediate operand field, the fields are in little-endian format. So, when operand fields opfield1 and opfield2 are combined for an #imm14 (for example, in instruction li), opfield1 is the low-order bits and opfield2 is the high order bits: imm14 = (opfield2 << 8) + op1. Similarly when three immediate operand fields are combined into an "#imm21" (for example, in instruction j21f): imm21 = (opfield2 << 16) + (op1 << 8) + opfield0.

###### Datatypes

Four datatypes:
- int32  (32-bit integers)
- ptr    (pointer to data)
- label  (pointer to code)
- handle (opaque type)


###### Registers
32 registers; a constant zero register, and 31 writable registers.

The notation:
- $n refers to the n-th register, treated as an integer
- &n refers to the n-th register, treated as an ptr
- @n refers to the n-th register, treated as an label
- ~n refers to the n-th register, treated as an handle

Register numbers are written with hexadecimal base. Register number n is sometimes notated Rn, e.g. R2.

For example the first and last registers, as integers, are: $0, $1d.

The zero-th register is a constant and writes to this "register" has no effect. The zero-th register, ..., when treated as an ..., is the constant ...:
- $0, int32:  zero
- &0, ptr:    null pointer
- &0, label:  RESERVED
- ~0, handle: STDIN/STDOUT device

## Instructions

49 instructions

|  |  |
| --- | --- |
| annotation | ann
| load constants | li lin li32 llf llb ll32
| loads and stores and copies | l8 l8u l16 l16u l32 lp s8 s16 s32 sp cp cpp ll sl cpl lh sh cph
| arithmetic of ints | add32 sub32 mul32
| bitwise arithmetic | and or xor shl shrs shru
| adding ints to pointers | ap
| comparision control flow | bltf bltuf beqf beqpf beqlf beqhf
| other control flow | j21 j32 jy 
| system call | syscall
| I/O | in1 out1
| misc | sinfo break

Notation for the instruction list below: #imm7 #imm14 #imm21 are unsigned immediate constants with maximum values of 127, 16383, and 2097151 respectively, $X/&X/@X/~X is an register or its contents, 0 is an unused argument that must always be 0 (other values are RESERVED for future use), and similarly for &2 and #f (immediate constant 16, written in hex). All signed immediate constants are two's-complement. "; #imm32" denotes that the 32-bit constant immediately follows the instruction in the instruction stream, in little-endian format.

From left to right, the arguments are called operands op0, op1, op2 (for example, in "cp $1 $2 0", the instruction mnemonic is 'cp', op0 is $1, op1 is $2, op2 is 0). Immediate operands are always on the right (the highest-numbered operand). When there are three operands, op0 goes into opfield0, op1 goes into opfield1, op2 goes into opfield2. When there are two operands, op0 goes into opfield0, op1 goes into opfield1 and opfield2. When there is one operand, op0 goes into opfield0 and opfield1 and opfield2.

Jump, branch, and load-label (llf/llb/ll32) immediate offsets are in units of bytes in the Boots code, where '0' indicates the instruction location after this instruction (in the case of j32 and ll32, that means the location after the embedded 32-bit immediate offset). JREL and branch immediates must not jump into a location in the middle of an instruction.

Platforms which compile, transform, or represent Boots code in memory in ways such that some instructions span more or less than 4 memory locations will alter the byte distance between instructions. Those implementations must remember to adjust the jump/branch immediate offsets accordingly before executing them (so as to preserve which instruction was targeted by each branch or jump).

Our convention is to try to order operands so that immediate operands are last, and register destination operands are first.

Instruction list:

annotation:
- ann ? ? ?: ANNotation; no effect on execution

load constants:
- li $dest #imm14: Load Immediate int constant: $dest = #imm14
- lin $dest #imm14: Load Immediate int constant Negated: $dest = -#imm14
- li32 $dest 0 0; #imm32: $dest = #imm32
- llf @dest #imm14: Load Program Counter plus signed (Forward) immediate offset: &dest = PC + #imm14
- llb @dest #imm14: Load Program Counter minus signed (Backward) immediate offset: &dest = PC - #imm14
- ll32 @dest 0 0; #imm32: Load Program Counter plus 32-bit signed immediate offset: &dest = PC + #imm32
- lih @dest #imm14: Load predefined HANDle: &dest = handle #imm14

register loads and stores:
- l32  $dest &addr 0     : Load  Word        int32  from memory addr &addr
- l16  $dest &addr 0     : Load  Halfword    int16  from memory addr &addr
- l16u $dest &addr 0     : Load  Unsigned    int16  from memory addr &addr
- l8   $dest &addr 0     : Load   Byte       int8   from memory addr &addr
- l8u  $dest &addr 0     : Load   unsigned   int8   from memory addr &addr
- lp   &dest &addr 0     : Load              Ptr    from memory addr &addr
- ll   @dest &addr 0     : Load              Label  from memory addr &addr
- lh   ~dest &addr 0     : Load              Handle from memory addr &addr
- s32  $src  &addr 0     : Store Word        int32  to   memory addr &add
- s16  $src  &addr 0     : Store Halfword    int16  to   memory addr &addr
- s8   $src  &addr 0     : Store Byte        int8   to   memory addr &addr
- sp   &src  &addr 0     : Store             Ptr    to   memory addr &addr
- sl   @src  &addr 0     : Store             Label  to   memory addr &addr
- sh   ~src  &addr 0     : Store             Handle to   memory addr &addr

register copies:
- cp   $dest $src 0: CoPy int:    $dest = $src
- cpp  &dest &src 0: CoPy Ptr:    &dest = &src
- cpl  @dest &src 0: CoPy Label:  @dest = @src
- cph  ~dest &src 0: CoPy Handle: ~dest = ~src

arithmetic of ints (result always defined and all results mod 2^32):
- add32   $dest $src1 $src2:  $dest = $src1 + $src2
- sub32   $dest $src1 $src2:  $dest = $src1 - $src2
- mul32   $dest $src1 $src2:  $dest = $src1 * $src2

bitwise arithmetic:
- shl  $dest $src 0: Shift Left (C's '<<' operator) by 1 bit
- shru $dest $src 0: Shift Right Unsigned (logical shift) by 1 bit
- shrs $dest $src 0: Shift Right Signed (arithmetic shift) by 1 bit
- and  $dest $src1 $src2
- or   $dest $src1 $src2
- xor  $dest $src1 $src2

Adding ints to Pointers:
- addp     &dest &src1 $src2: &dest = &src1 + $src2
- subp     &dest &src1 $src2: &dest = &src1 - $src2

forward conditional branches:
- beqf  $src0 $src1 #imm7: Branch-if-EQual, forward
- beqpf &src0 &src1 #imm7: Branch-if-EQual on Ptrs, forward
- beqlf @src0 @src1 #imm7: Branch-if-EQual on Labels, forward
- beqhf ~src0 ~src1 #imm7: Branch-if-EQual on Handles, forward
- bltf  $src0 $src1 #imm7: Branch-if-Less-Than, forward
- bltuf $src0 $src1 #imm7: Branch-if-Less-Than-Unsigned, forward

unconditional jumps and other control flow:
- j21f #imm21: unconditional Jump forwards  (range~ +2MiB)
- j21b #imm21: unconditional Jump backwards (range~ -2MiB)
- j32 0 0 0; #imm32:  unconditional Jump; 32-bit signed offset (range~ +- 2GiB)
- jy @target 0 0: Jump dYnamic (indirect)

system call:
- syscall #imm21: CALL SYStem function number #imm21

I/O:
- in1: $dest ~device 0
- out1: $src ~device 0

misc:
- sinfo $dest #imm14: $dest = System INFOrmation query
- break ? ? ?: breakpoint
- feat #imm7 #imm7 #imm7: feature check; branch forward if features not supported
- hint ? ? ?: no-op
- impl ? ? ?: implementation-defined

Notes on certain instructions:

- ann: implementations may ignore or elide/remove ann instructions
- TODO: should we do this? mb not: addp, subp: the int32 argument $src2 is interpreted as signed, so although only addition is provided, subtraction can also be accomplished
- beqf, beqpf, beqlf, beqhf, bltf, bltuf: forward branch to an offset of unsigned #imm7 bytes relative to the instruction after the branch
- blt: the int32 arguments (in $src0 and $src1) are signed
- bltu: the int32 arguments (in $src0 and $src1) are unsigned
- break: implementation-defined debugging functionality
- feat: op0 specifies a range of eight features (op0=0 means flags 0 thru 7, op0=1 means flags 8 thru 15, etc). op1 is a bitmask whose bits correspond to the first seven features in the range, from least-significant-bit (LSB) to MSB; if there is any '1' bit in op1 such that the corresponding feature is absent, then branch forward with offset op2.
- hint: intended for forward compatibility; later versions of the specification may define semantics for hint with the understanding that some implementations may execute them as no-ops. Tooling that does not recognize a hint may treat it as a no-op (ignore it), but should not elide/remove it from the instruction stream.
- j21f, j21b, j32: jumps target an offset relative to the next instruction
- j32, ll32: 32-bit signed constant is embedded in instruction stream immediately following instruction
- j32: jump targets an offset relative to the next instruction after the embedded offset
- l8: guaranteed to produce values between -128 and 127, inclusive (when interpreted as signed).
- l8u: guaranteed to produce values between 0 and 255, inclusive (when interpreted as unsigned)
- l16: guaranteed to produce values between -32768 and 32767, inclusive (when interpreted as signed).
- l16u: guaranteed to produce values between 0 and 65535, inclusive (when interpreted as unsigned)
- li32: 32-bit constant is embedded in instruction stream immediately following instruction
- llf, llb: offset is relative to the next instruction
- lpc, lwc, spc, swc: op1 must always be &2. The immediate #imm7 in op2, the offset, is required to be an integer between 0 and 15, inclusive. 
- s8, s16: stores the least-significant 8- and 16- bits, respectively, from the 32-bit $src register
- sinfo: See below for defined queries
- syscall: See below for defined syscall numbers.

## sinfo queries

when op1 = ..., sinfo returns ...:

- 0: VERSION  (currently always 0)
- 1: PTR_SIZE,  the number of memory locations per ptr, label, or handle
- 2: INT16_SIZE, the number of memory locations per int16
- 3: INT32_SIZE, the number of memory locations per int32

All others are currently RESERVED.

Note that the sinfo query results defined above (and possibly others) are static -- they must never change during the execution of a program.

Boots guarantees that:

- INT8_SIZE = 1
- 1 <= INT16_SIZE <= 2
- 1 <= INT32_SIZE <= 4
- 1 <= PTR_SIZE
- INT16_SIZE <= INT32_SIZE <= 2*INT16_SIZE  (an int32 is not smaller than an int16, and an int32 can fit within the size of two int16s)
- INT16_SIZE <= PTR_SIZE (an int16 can fit within the size of a pointer)

Note: because INT16_SIZE <= PTR_SIZE, a program can opt to use fixed-size 'slots' of size PTR_SIZE to hold things. A slot of size PTR_SIZE is guaranteed to fit any of: an int8, and int16, a ptr, a label, a handle. Two slots of PTR_SIZE can fit an int32.



## syscalls
The argument specifies which system function is called.

Number 0 thru 1 are defined below and 2 thru 65535 are RESERVED for extensions. sycall numbers 65536 and above are implementation-defined and may be used to access linked libraries, if the implementation supports that.

### syscall 0: exit(RESULT : uint32)

Terminate program, with RESULT code passed in register 5. The result code is interpreted in a platform-specific way (however, most typically, success is indicated with a result code of 0).

### syscall 1: memcpy(DST : ptr, SRC : ptr, SIZE : uint32)
Copy SIZE bytes starting at memory location SRC to memory starting at memory location DST.

Arguments are passed as follows: DST in register 5, SRC in register 6, SIZE in register 7. Registers 5,6,7 might be overwritten during the call.

### syscall 2: malloc(SIZE : uint32)
Allocate SIZE bytes of memory. SIZE is passed in register 5. If allocation fails, the null pointer is returned in register 5, otherwise a pointer to the newly allocated memory block is returned in register 5.

### syscall 3: free(BLOCK: PTR)
Free the block of memory starting at BLOCK. BLOCK is passed in register 5. Freeing the null pointer is illegal (note that this is different from POSIX). Freeing any pointer which was not ultimately created by 'malloc' is illegal. Freeing the same pointer twice is illegal (unless the same pointer value was returned again by malloc after being freed).

## Extensions
The document you are reading defines Boots without any extensions. Various optional Boots
extensions are defined in the document boots_extension_reference.md. A program can detect which extensions are present by testing for the presence or absence of various numbered 'features'. In Boots without any extensions, no features are present.

### The feat instruction

The 'feat' instruction allows the program to check for the presence of feature extensions. The way that it works is that each feature extension is assigned a feature number. The first operand of 'feat' chooses a range of 8 feature numbers (for example, op0=0 chooses the first eight features (features 0 thru 7), op1=1 chooses the next eight features (features 8 thru 15), etc). The second operand is a bitmask with '1's indicating desired features; each bit corresponds to one feature in the range, starting with the least-significant-bit (LSB). The third operand is a forward branch offset to take if any desired features are not present. Note that since operands are only permitted range thru 127, the second operand is effectively a bitmask of 7 bits instead of 8, so any feature number which is a multiple of 8 cannot actually be accessed.

For example, 'feat 2 6 8' is interpreted as follows. op0=2 means that we are looking at feature range 14-20. op1=6 is a bitmask with '1' in the 2 bit and the 4 bit, which are the 2nd and 3rd bit, counting from LSB; therefore they correspond to features 15 and 16. So, if both of features 15 and 16 are present, then the instruction does nothing. But if either feature 15 or feature 16 are absent, the instruction branches forward by op2 bytes, which is 8. 8 bytes is two Boots instructions (and since the counting starts with the instruction after this, an 8 byte forward branch indicates the third Boots instruction after this). So so summarize, the instruction "feat 2 6 8" means "if either of feature 15 or feature 16 is absent, then branch forward to the third Boots instruction after this one.".


## Tables of opcodes

    0      ann
    1      hint
    2      break
    3      impl
    4      j32
    5      li32
    6      ll32

    8      feat

    9      j21b
    10     j21f
    11     syscall

    16     llb
    17     llf
    18     li
    19     lin
    20     lih
    21     sinfo
    
    32     beqf
    33     bnef
    34     bltf
    35     bltuf
    36     befpf
    37     beqlf
    38     beqhf

    128    l32
    129    l16
    130    l16u
    131    l8
    132    l8u
    133    lp
    134    ll
    135    lh
    136    s32
    137    s16

    139    s8

    141    sp
    142    sl
    143    sh
    144    cp
    145    cpp
    146    cpl
    147    cph
    148    shl
    149    shru
    150    shrs
    151    add32
    152    sub32
    153    mul32
    154    and
    155    or
    156    xor
    157    addp
    158    subp
    159    jy
    160    in1
    161    out1

Opcodes 0 thru 3 can be skipped or are implementation-defined:

    ann, hint, break, impl

Opcodes 4 thru 6 have a 32-bit immediate embedded in the instruction stream:

    j32, li32, ll32

Opcode 8 has three 7-bit immediates:

    feat
    
Opcodes 9 thru 11 have a 21-bit immediate in op0:

    j21b, j21f, syscall

Opcodes 16 thru 20 have a 14-bit in op1:

    llb, llf, li, lin, lih, sinfo

Opcodes 32 thru 38 have a 7-bit immediate in op2:

    beqf, bnef, bltf, bltuf, beqpf, beqlf, beqhf
    
Opcodes starting with 128 have no immediate:

    l32, l16, l16u, l8, l8u, lp, ll, lh, s32, s16, RESERVED_138, s8, RESERVED_140, sp, sl, sh, cp, cpp, cpl, cph, shl, shru, shrs, add32, sub32, mul32, and, or, xor, addp, subp, jy, in1, out1

All opcodes, as a list:

    ann, hint, break, impl, j32, li32, ll32, RESERVED_7, feat, j21b, j21f, syscall, RESERVED_12, RESERVED_13, RESERVED_14, RESERVED_15, llb, llf, li, lin, lih, sinfo, RESERVED_21, RESERVED_22, RESERVED_23, RESERVED_24, RESERVED_25, RESERVED_26, RESERVED_27, RESERVED_28, RESERVED_29, RESERVED_30, RESERVED_31, beqf, bnef, bltf, bltuf, beqpf, beqlf, beqhf, RESERVED_39, RESERVED_40, RESERVED_41, RESERVED_42, RESERVED_43, RESERVED_44, RESERVED_45, RESERVED_46, RESERVED_47, RESERVED_48, RESERVED_49, RESERVED_50, RESERVED_51, RESERVED_52, RESERVED_53, RESERVED_54, RESERVED_55, RESERVED_56, RESERVED_57, RESERVED_58, RESERVED_59, RESERVED_60, RESERVED_61, RESERVED_62, RESERVED_63, RESERVED_64, RESERVED_65, RESERVED_66, RESERVED_67, RESERVED_68, RESERVED_69, RESERVED_70, RESERVED_71, RESERVED_72, RESERVED_73, RESERVED_74, RESERVED_75, RESERVED_76, RESERVED_77, RESERVED_78, RESERVED_79, RESERVED_80, RESERVED_81, RESERVED_82, RESERVED_83, RESERVED_84, RESERVED_85, RESERVED_86, RESERVED_87, RESERVED_88, RESERVED_89, RESERVED_90, RESERVED_91, RESERVED_92, RESERVED_93, RESERVED_94, RESERVED_95, RESERVED_96, RESERVED_97, RESERVED_98, RESERVED_99, RESERVED_100, RESERVED_101, RESERVED_102, RESERVED_103, RESERVED_104, RESERVED_105, RESERVED_106, RESERVED_107, RESERVED_108, RESERVED_109, RESERVED_110, RESERVED_111, RESERVED_112, RESERVED_113, RESERVED_114, RESERVED_115, RESERVED_116, RESERVED_117, RESERVED_118, RESERVED_119, RESERVED_120, RESERVED_121, RESERVED_122, RESERVED_123, RESERVED_124, RESERVED_125, RESERVED_126, RESERVED_127, l32, l16, l16u, l8, l8u, lp, ll, lh, s32, s16, s16u, s8, s8u, sp, sl, sh, cp, cpp, cpl, cph, shl, shru, shrs, add32, sub32, mul32, and, or, xor, addp, subp, jy, in1, out1


## Semantics details



### Integers

#### mod 32
Int32 overflow on addition, subtraction, multiplication wraps around (that is, mathematically the operations are done mod 2^32).

#### Signed vs unsigned
Note that that the operations of add32, add32m, sub32, mul32 give valid results whether you consider the int32 operands to be signed two's complement or unsigned, as long as you consider the result to be similarly unsigned or signed.

On many platforms, it may be easiest to implement add32, add32m, sub32, mul32 by viewing the int32s as unsigned integers and then applying unsigned arithmetic operations, because many platforms don't implement wrap-around signed numbers.


#### Integer bitwidths

The instructions lb, lbu, lh, lhu guarantee that the numbers read into registers are in certain ranges that fit in 8- and 16-bits, respectively. lb and lh sign-extend the number read to 32-bits, and lbu and lhu zero-extend the number read to 32-bits.

However, lb and lh result in signed two's complement representations in the destination register; note that the bit pattern of a small negative number in a 32-bit register, when coded with signed two's complement, is equivalent to a number larger than 16 bits if interpreted as unsigned. For example, a -1 in a register, signed, would be viewed as (2**32 - 1 = 4294967295) unsigned.

Boots guarantees that bytes (8-bit integers) have a size 1 in memory (meaning that values that are stored with sb occupy one memory location).

#### (not) mixing integer bitwiths
When in registers and being operated upon, the internal representation of int32s is a defined sequence of bits, however, when in memory, the internal representation of integers may vary across different target platforms. For example, if a memory location x contains a 32-bit integer, and you read it with lh or lhu, the value that is read is unspecified other than that it's no larger than 16 bits. Similarly, if a memory location contains an 8-bit integer and you read it using l32, the value that is read is unspecified other than that it's some int32 (also, reading a byte using l32 near the edge of accessible memory will cause undefined behavior if there are less than INT32_SIZE memory locations in accessible memory, starting with the location read). You cannot write a sequence of bytes (8-bit integers) into memory and then reliably read it back using l32, and you cannot write a 32-bit integer into memory and then reliably read out its component bytes.

Furthermore there is no guarantee that 32-bit integers occupy more than one memory location, or that larger integer bitwidths occupy more memory locations than smaller; it's possible for both of INT16_SIZE, INT32_SIZE to be identically 1 (this can happen if the implementation chooses to make each single memory location large enough to store 32 bits of data). Larger integer bitwidths are guaranteed to occupy at least as many memory locations as smaller.

### I/O

The in1 and out1 instructions read a single integer word of input or write a single integer word of output to a device.

The availability, identity, and semantics of devices is platform-specific. Some devices may read and write only 8-bit characters; in this case, the character to be written should be passed in the lowest 8-bits of the word passed to out1.

If the target platform supports STDIN and STDOUT, then in1 with a device of ~0 should read from STDIN, and out1 with a device of ~0 should write to STDOUT. ~0 can be accessed by giving the zero register as the device argument to in1 and out1, or by lih with an immediate argument of 0. in1 and out1 on ~0 should be unbuffered, if possible.

TODO: result/error code? What happens if the read returns immediately with no input (as happens upon EOF on many systems)?

Since the devices supported are implementation-dependent, some implementations may not support any I/O devices at all. Upon encountering in1 or out1 with an unsupported device, implementations may ignore or may report an error, or in the case of in1 may return an arbitrary value.

### Equality

- beqb, beqpb, beqlb, beqhb, beqf, beqpf, beqlp, beqhf: equality is guaranteed to be reflexive (x = x), symmetric (x = y implies y = x), and transitive (x = y and y = z implies x = z)
- bneb, bnepb, bnelb, bnehb, bnef, bnepf, bnelp, bnehf: each not-equals comparison is guaranteed to be true if and only if the corresponding equals comparison is false


### Undefined/arbitrary/reserved

These lists are probably accidentially incomplete right now, but we hope to make this list comprehensive as time goes on.

#### Undefined behavior

The following are undefined behaviors in Boots. Any program containing undefined behavior on any codepath has undefined behavior as a whole:
- branching or jumping to a location outside the bounds of the program (unless the location was provided in an implementation-dependent manner for the purpose of being jumped to)
- branching or jumping to a location in the middle of an instruction
- creating a pointer to or accessing memory that was not provided to the Boots program by an external function, unless the platform permits this in an implementation-dependent way
  - an exception is the null pointer, which can be created (by reading from &0) but not legally dereferenced
  - TODO tighten up this wording
  - TODO permit pointer one past accessible memory, like C
- a branch or jump instruction which branches or jumps back to itself
- any instruction that does not have a 0, for an operand listed with a 0 in the instruction listing above
- performing arithmetic on a pointer that would cause the result to point outside of memory to which the program has access
- assuming that pointers 'wrap-around'; adding an integer to a pointer which is greater than the distance between that pointer and the top of accessible memory, or subtracting an integer to a pointer which is greater than the distance between that pointer and the bottom of accessible memory
- accessing a pointer that points to memory to which the program doesn't have access
- an instruction with a value outside of the range {0, 64 thru 94} in a register operand
- any opcode or instruction encoding which is RESERVED or is not defined
- operation on a value with the wrong type
- setting register 2 to any value other than a valid pointer
- loading an integer as a non-integer
- accessing the null pointer
- freeing the null pointer
- freeing a pointer which was not returned by malloc
- freeing the same pointer twice (unless it was returned again by malloc, after free it)
- calling an undefined syscall
- accessing an undefined device
- reading an uninitialized register

#### Arbitrary values

The following do not cause undefined behavior and do not make the whole program invalid, but do not define the resulting values of certain operations:
- loading part of an integer by using l16, l16u, l8, l8u on a larger-bitwidth integer (this is guaranteed to produce an int16 for l16 and l16u, and an int8 for l8 and l8u, but otherwise the value produced is not specified)
- loading a larger bitwidth than was stored by using l32, l16, l16u on a smaller-bitwith integer (this is guaranteed to produce an int32 for l32, and an int16 for l8 and l8u, but otherwise the value produced is not specified)
- loading from or storing to the middle of an integer
- loading a non-integer as an integer
- reading an undefined sinfo number


#### Reserved and implementation-defined items

Implementations must not define or use items which are reserved for extensions, or items which are reserved for future use; if they do so, they risk incompatibility with extensions or future Boots versions. Extension languages must not define or use items which are defined in Boots to be implementation-dependent.



## Boots Assembly
Boots Assembly is a plaintext syntax for Boots.

### General syntax
Boots Assembly is ASCII text. Each line is processed separately; lines are delimited by the newline character, '\n' (a byte with the value 10). Whitespace is defined as one of the characters: ' \t\n\r\f\v' (where \t indicates tab, \n indicates newline, etc). Lines which are all whitespace, or which begin with a semicolon, are skipped. Trailing whitespace on any line is ignored. The usable information in any line may be followed by whitespace which may be followed by a semicolon. After any semicolon the rest of the line is a comment (all characters except newline are ignored), up to the first newline, which still terminates the line.

The last line in the file must end in a newline, unless the last line is all whitespace.

### Data lines (.d)
Some lines may begin with '.d' (a 'data directive'). This is followed by a space and then an 8-bit, 16-bit, or 32-bit unsigned hexadecimal number (1, 2, or 4 pairs of characters which are each digits or letters within a-f).

Therefore, data lines must match the following regular expression (regex):

    ^\.d (([0-9a-f][0-9a-f]){1,4})\s*(;.*)?$

### Instruction lines

Other lines (an 'instruction line') begin with an instruction mnemonic. Instruction mnemonics are at most 12 characters, and are all lowercase alphanumeric. Mnemonics begin with an alphabetic character and are followed by one or more alphanumeric characters. This is followed by the first operand, op0, represented as exactly one space, followed by a hexadecimal number (a string of digits from 0 to 9 and lowercase letters from a to f). This may be followed by another space and a second number (op1), and maybe by a third space and a third number (op2). After three operands the usable information in the line is exhausted and the assembler may skip to the next line. If the instruction is followed by a 32-bit embedded immediate constant, this must be included manually on the next line using '.d'.

Operands are hexadecimal integer in base 16. Immediate operand ranges are (using hexadecimal notation):

- #imm7:  0 thru 7f
- #imm14: 0 thru 3fff
- #imm21: 0 thru 1fffff

Operands of register type must be in the range 0 to 7, inclusive (they are not prefixed with a type sigil).

Therefore, instruction lines must match the following regular expression (regex):

    ^([a-z][a-z0-9]+) ([0-9a-f]+)( ([0-9a-f]+))?( ([0-9a-f]+))?\s*(;.*)?$


## Subsets and extensions

### Tiny Boots subset
The Tiny Boots subset excludes the instructions: mul32, li32, ll32, j32.

### Static Control Flow Boots subset
The Static Control Flow Boots subset excludes the instructions: lpcf, lpcb, lpc32, jy.

### Pure Boots subset
The Pure Boots subset excludes the instructions: in1, out1.

Pure Boots also excludes any implementation-dependend lib calls which:
- have side effects, or which read input, or are nondeterministic

Implementations may define regions of memory such that reading/writing from/to those regions is allowable in 'Pure Boots' (that is, not considered to be a side effect), and other regions to which access is not allowed in Pure Boots.

### Vanilla Boots extensions
A Boots implementation supports "Vanilla 32-bit little-endian" if:

- integers are represented using little-endian with twos-complement for signed values
- pointers are defined to be represented as 32-bit integers, and integer operations can be performed on pointers
- INT32_SIZE is 4
- INT16_SIZE is 2
- PTRD_SIZE is 4
- mixing integer bitwidths (writing to memory with one bitwidth and reading with another) is allowed and has the expected result

A Boots implementation which supports supports "Vanilla 64-bit little-endian" if it has the same properties of "Vanilla 32-bit little-endian", except:

- integers are represented using little-endian with twos-complement for signed values
- pointers are represented as 64-bit integers, and integer operations can be performed on pointers
- PTRD_SIZE is 8

Vanilla k-bit big-endian extensions are defined analogously to the corresponding little-endian styles, except the representation is big-endian.

Note that Boots programs which assume a Vanilla extension are not portable/not compatible with implementation that do not support the same Vanilla extension.



## Tips for implementors and programmers

### Decoding implementation tips

- The first three (most significant) bits of the first byte are always 1,1,0
- when implementing Boots on a little-endian system, recall that if you read or write all 4 bytes of a Boots instruction at once as a single 32-bit integer, the last byte will be treated as the most-significant-byte and the first byte as the least-significant-byte, for the purposes of the value of that 32-bit integer. If this bullet point is confusing to you, then it will probably be easier for you to decode Boots by reading and writing each byte separately.

### Arithmetic signed vs unsigned examples

For example, for multiplication, imagine if we had 3-bit integers instead of 32-bit integers. If we multiply the unsigned representations of 2 * 3, that is, 010 * 011, the result is 6, that is, 110. In two's complement, 010 represents 2 and 011 also represents 3, and 110 represents -2; and 2 * 3 = 6 = -2 mod 2^3. To give another example, if we multiply the unsigned representations of 6 * 2, that is, 110 * 010, the result is 12, and 12 mod 2^3 is 4, that is, 100 in binary. In two's complement, 110 represents -2 and 010 represents 2, and the result, 100, represents -4; and -2 * 2 = -4 mod 2^3 = 4. Do note that these are only correct modulo the bitwidth; for example, 2 * 3 = 6, but mul 010 011 = 110, which when viewed as two's complement yields 2 * 3 = -2, an incorrect result in ordinary arithmetic, but -2 is equivalent to 6 mod 8, so the result is correct in mod 8 arithmetic. In the examples in this paragraph we used mod 8 for 3-bit integers, but in reality, we are using mod 2^32 for 32-bit integers, not mod 8.


### Boots programs can't do much with pointers, labels, handles

Note that many arithmetic operations are provided only for integers; the only arithmetic you can do to pointers is add signed integers to them, and compare them for equality.

You can't do any arithmetic at all on labels or handles except for equality comparisons. The only things you can do with labels are execute a dynamic jump to them ('jy' instruction), load them, store them, and copy them. The only things you can do with handles are load them, store them, and copy them.

### What is a handle?
A handle is a totally opaque piece of data that was created by some external library and passed to you. For example, if you open a file, you may acquire a file handle. To do further operations on the file, you would pass that file handle to other external library functions.

The reason we do not just use integers or pointers is because we want to be agnostic about whether the handle is actually an integer or actually a pointer. This may make for more efficient cross-platform support. For example, on Linux a file handle is an integer, but on Windows it is an opaque type called a "handle". We could have just said that all handles are pointers, and then made the handle a pointer to whatever the external system really returned (for example, on Linux we could have made the 'open' syscall function return a pointer to an integer), but when handles are actually integers, this would be needlessly inefficient.

In Boots, handles are always the same size as pointers; if the actual handle used by an external library is a larger object, then a Boots implementation may have to allocate memory store it elsewhere and return some sort of reference as the handle to the Boots program. For example, the handle might be a pointer to implementation-allocated memory, or might be an index into a table maintained by the implementation.

There may be different kinds of handles out there which are not interchangable and which may cause undefined behavior if mixed up (for example, a handle to a file vs. a handle to a graphical display control device), and some Boots implementations may be aware of handle internals to a greater or lesser degree, but as far as the Boots specification is concerned they are all just handles.
      
### To blindly copy memory
Use the 'memcpy' syscall.

----
----
----

## TODO

- the above is (what i used to call Boots1). Also define Boots1A (what i used to call Boots0)
- consider adding a 'syscall' variant with arguments (so that in Boot (BootX) we can give immediates there). Maybe go back to having both 'syscall' and 'lib'; 'lib' can have all immediates, and 'syscall' can have one immediate and two arguments (or all arguments; in which case in Boot(X) only the first 64 syscalls could be given immediately; an advantage of all arguments is that the total number of possible syscalls is 2**16 rather than 128; also having it be immediate may be nice for static compilation but really only Boot(X) implementations will probably be able to take advantage of that; but i worry that platforms without computed goto won't be able to handle it; otoh could just implement that with a switch of length 128, which is no worse that restricting syscalls to 128 anyhow. Anyhow maybe we should really try to specify only 64 syscalls? Alternately, have syscall with a 14-bit immediate and one argument, so that Boot(X) can pass stuff like exit codes in the immediate, but can choose from 2**14 syscalls -- leaning towards that).
- rename INTX_SIZE to IX_SIZE
- consider defining everything as signed, arbitrary bitwidth, with only the range -2**15 thru 2**15-1 permitted. Remove l16u, bltuf, shru, j32, li32, ll32 (is this only for Boots0 or for Boots1 too?). Also, define a way to detect overflow/underflow: for each arith operation, see what happens when the inputs are in that range and the mathematical result is less than -2**15 or greater than 2**15-1 with: 16-bits, 32-bits, and find an instruction sequence that can detect this condition. Also, specify the necessary instructions for keeping things in the range (e.g. bitwise AND after each operation).
  - could this be simplified even further by using only unsigned integers range 0 thru 2**15-1? And instead of ap, we could provide addp and subp. I like this because it probably means even less thought is required by implementors on some platforms (e.g. platforms that only offer unsigned integers).
  - mb both signed and unsigned can coexist if we simply say that wraparound is not allowed, or maybe just reflects arbitrary bitwidths? need to take another look at those examples i found before of observable breakage
- mb make one of the syscall operands be an immediate that passes information about which registers are in use, though, like we were planning to do with CALL.
- do we want labels to be PTR_SIZE? On Harvard machines these sizes could be different

- consider:
  - changing the name of the 'handle' data type to 'opaque'
  - allowing the use of 'opaque' to opaquely copy things, instead of memcpy
  - creating an instruction for 'exit'
  - adding 'malloc' or a way to access the data register (mb an sinfo) plus 'sbrk' so that a streaming compiler can allocate some memory?!?
  - the goal here is to: (a) allow a compiler to allocate memory, and (b) remove the need for calling convention stuff in Boots
  
- todo finish the writeup in ootAssemblyNotes27
- start copying in from = General architecture from other file
- implement this new version

- should note somewhere the amount of hidden state that BootX requires. I think it's just the PC.
  - no also at least need scratch for loading immediates, loading from stack cache, swaps, mv loops in idiv? and mb software floating point?
- mb take some of the strictly unnecessary 'macro' instructions (eg. some of the comparisons; the immediate and indexed add pointers) and move them into BootX

- bootx already have idiv, should we move 'MUL' there? i'm thinking not but not sure

- malloc? sbrk? gp and sinfo to see how much memory is available?

- decide on calling convention for syscalls. Are there caller-save registers? Do we need to tell them which registers are in use?

- add profile/extension/'capability' (mb cap is not the best name since Ovm will have the other kind of capabilities) indicator sinfo

- add in, out syscalls

- change add32 to add, etc; relationship between signed and unsigned int is either unknown or twos-complement with unknown bitwidth; wraparound arithmetic is either UB or twos-complement with unknown bitwidth. Only 16 bit is initially provided.

- add memsz. keep memcpy.
- add in,out (io syscall extension)
- change malloc/free to memgrow

- probably need PC-relative loads and stores eg https://smist08.wordpress.com/2021/01/08/apple-m1-assembly-language-hello-world/

- labels on their own line (if not already)

- this is all still too complicated. i think we can cut a number of these instructions
- do we really need feature testing?