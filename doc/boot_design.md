# Boot design

This document explains the 'why' behind Boot design choices.

[[_TOC_]]

# Big picture


## Context

Boot is part of the LOVM project, which aims to create a portable target language that high-level languages can be written on top of.


Two of the goals of LOVM are to be:
* easily portable
* an interoperable glue language

The idea is that a high-level language can target LOVM, and write its runtime (if any) in LOVM, and in this way be able to be easily ported to many platforms. These platforms can themselves be high-level languages, and code targetting LOVM will be able to interoperate with the underlying platform (calling platform functions, being called by platform functions, and passing high-level data structures).

The plan is to build two layers:
- Boot, an assembly language
- LOVM,  a flexible target language (LOVM stands for LOw-level Virtual Machine) which is an extension of Boot, and can be compiled to Boot

Boot is not just a substrate of LOVM, but also a subset of it (although some semantics details differ). In addition to being the platform that the reference implementation of LOVM is implemented in, the Boot instruction and register set are a subset of LOVM's, and the Boot encoding format is part of the LOVM encoding format

It is not the case that all implementations of LOVM will be built on top of Boot. In fact, good interoperability and performance can most likely only be achieved by implementing LOVM directly on a target platform, or even by skipping the LOVM project entirely and directly implementing higher-level-languages. However, Boot provides a 'good enough' platform that is quicker to port. In addition, because LOVM is an extension of Boot, the effort spent to create a Boot implementation can for the most part be re-used towards creating a LOVM implementation.

### Once you have Boot, what's the point of LOVM?
Boot only provides assembly-language-level constructs, but LOVM provides some higher-level constructs. This allows a platform native LOVM implementation to:
- interoperate better with the underlying platform by making these higher-level constructs compatible with the platform's native analogs
- efficently make use of high-level platform primitives, when available

### Compare Boots to BootS
Boots is a smaller, simpler ISA that is easier to implement. It is a RISC fixed-width load/store architecture.

BootS is a slightly more complex ISA that may permit better code density and performance. BootS supports a mixed-width instruction stream of 8-bit, 16-bit, and 32-bit instructions.


# Overall Architecture

## Encoding

### What are the constant '110' bits at the beginning of each 32-bit instruction for?
We call those 'encoding format bits'. The idea is that other instruction encodings can be defined which include Boots as a subset. These other encodings are then free to define new instruction formats as long as those other formats don't start with '110'.

We put the 3 encoding format bits (110) first so that something which is reading the instruction stream byte-by-byte can tell that this is a 32-bit instruction as soon as possible.

### Why is the opcode near the first bits instead of the last bits, or the least-significant bits?
The first priority is for something which is reading the instruction stream byte-by-byte to be able to tell the instruction format as soon as possible. The next priority is for a bytewise reader to be able to identify the opcode as soon as possible.

By contrast, the RISC-V architecture puts the opcode in the least-significant bits, but assumes a little-endian byte ordering, so that the least-significant bits appear in the first byte. We don't do this because we want to have space for 256 opcodes, which requires 8 bits, but there are also the 3 encoding format bits, so we can't fit our whole opcode in the first byte.


### Why is the all-zero eight-bit instruction illegal?
TODO

### Why multiple instruction lengths?

Multiple instruction lengths allow greater code density at the expense of more complexity and possibly slower decoding (particularly parallel speculative decoding in hardware).

### Why is code density important to this project?
One of my goals is to support massively-concurrent, "brain-like" computing. The brain appears to have massive concurrency at the cost of relatively slow serial execution speed of each CPU.

In order to support more cores (here, i am using the word 'core' to refer to the unit which is being repeated for massive concurrency; a CPU paired with some local memory and networking capability) in a given die area or thermal envelope, at the cost of slower serial execution speed, one might not be doing fancy stuff like parallel speculative decoding anyways.

Multiple instruction lengths permit higher instruction density, and this allows one to give less local memory to each core, which would allow more cores to fit in a given die area or thermal envelope.

Similarly, in software, less memory pressure may allow more greenthreads at once.

### Why are the offsets in stack cache, frame cache, and displacement addressing mode coded the way that they are?

That is, why do we represent offsets as linear combinations of INT32_SIZE and PTR_SIZE?

First, observe that although these addressing modes can only represent certain offsets, the others can all be reached by using sequences of instructions. So the choice of encoding for addressing mode offsets affects only code density, not expressability.

Broadly, there were a few options here:

1) We could have encoded displacements as number of memory locations. In this case, portable programs using integer data types other than int8 could only index over this data if they padded them to their maximum allowed size (2 for INT16_SIZE, 4 for INT32_SIZE, 8 for INT64_SIZE). However, since PTR_SIZE has no maximum allowed size, programs then could not use these offsets to index over pointers at all. Non-portable programs (programs which assume fixed values for INT16_SIZE, INT32_SIZE, INT64_SIZE, and PTR_SIZE) could do more with these, but we're not interested in supporting non-portable programs.

2) We could have added the concept of structs to the language, and then specified offsets in terms of number of fields in the struct. That is too high-level for Boot, however.

3) We could have encoded displacements as scaled by a fixed number. This would allow large offsets into data structures consisting of fixed-size 'slots'. For example if the slot size were max(INT64_SIZE, PTR_SIZE), then any 64-bit or less integer or pointer (or label or handle, since those are also PTR_SIZE) could fit within one slot. However this is not useful for programs not using fixed-size slots of the chosen size. Programs which do use slots throughout use memory less efficency that programs with variable-sized data. In addition, one slot size choice expected to be relatively common, namely, (max(INT32_SIZE, PTR_SIZE)) is always equal to PTR_SIZE and hence can be represented as a linear combinations of INT32_SIZE and PTR_SIZE.

4) Or we could do what we did, that is, represent offsets as linear combinations of size constants. This reduces the range of offsets compared to the other options but it is portable, low-level, and is useful without fixed-size slots.

Having chosen (4), why did we choose INT32_SIZE and PTR_SIZE as the constants for the linear combination? Why not also or instead of use INT8_SIZE or INT16_SIZE? Because, with 4-bit offset fields, using INT8_SIZE or INT16_SIZE would greatly reduce the range of the offset when applied to int32s, which is expected to be more common than int8s or int16s. When int8s or int16s are present, the offset can still be applied the long way, without using the special addressing modes; the addressing modes are just there to make the code a little more dense in some of the most common cases.

Why not also use INT64_SIZE? It's true that the only way to use what is given to index over int64s is to allow 2*INT32_SIZE space for each INT64, which on high-level-language platforms (where one memory cell can hold one of any of our types) is wasteful. But, first, the program can just not use the offsets in this case. And, second, this is not wasteful on hardware platforms, where 2*INT32_SIZE = INT64_SIZE, and hardware platforms are where you care the most about efficiency anyways.



## Register-offset cache

### Why register-offset cache?

The motivations for the stack cache (register-offset cache with stack pointer base register) are:
- the Boot calling convention passes function arguments in the stack cache. This allows each implementation to choose to pass all arguments on the stack, or to pass some arguments in platform registers; that is, we avoid the specifying how many arguments are passed in registers in the calling convention.
- compilers can assume that writes to a register-offset cache may or may not be visible in memory until the next flush. This means that they can model a register-offset cache as a fixed array of registers (of whatever size they choose), and that cache reads/writes beyond the bounds of this array go directly to memory. Because writes to this array are not visible in memory until a flush, the compiler can optimize computations involving this array between flushes the same way they optimize computations in registers. In other words, in some cases, compilers may be able to 'compile away' operations on the register-offset cache.
- for greater code density, we would like to have the possibility of a number of 'stack machine-like' 8-bit Boot instructions that operate on a stack. Stacks allow for higher code density, compared to adding an additional number of registers equal to the stack depth. This is because with registers, instruction encodings must specify which register is being used, but you don't have to specify which stack location you are using because it is assumed to be the top of the stack. We already have a stack in memory, so they may as well operate on this stack. But if all such operations must read/write memory rather than registers, that would be slow.
- we also want to computation on the stack to be fast because we want to encourage 'stack-machine-like' computations because they allow for slightly easier program analysis in some cases, because items are popped off of the stack when they are used for the last time, instead of continuing to sit in a register after their last use, whereas in register-machine computations it is more work to determine when a value in a register has already been used for the last time and is now an unneeded leftover.

This does not impose mandatory complexity on implementations because it is legal for an implemention to just not maintain a cache at all. The complexity imposed on programs is also minimal; they just have to use flushroc2mem and flushmem2roc at appropriate times, which should be easy because programs won't typically be interleaving accesses to their stack via memory vs. via cache. It does make the specification longer, however.

### Design criteria for ROC cache

- Implementations must be allowed to choose to not implement an ROC cache at all (to translate ROC loads/stores to memory loads/stores and to translate the ROC flush operations as no-ops).
- Implementations must be allowed to cause the first few arguments to functions (in the Boot calling convention) to be stored in physical registers, so that these memory doesn't need to be accessed at all for these arguments.
- Typically Boot programs will access the stack frame exclusively through the ROC and through special stack frame instructions. It should be possible for Boot programs to access stack frames in memory, but this is allowed to be inefficient and more complex (therefore, we mandate usage of the flush instructions in these cases)
- For Boot programmers, the semantics should suggest at least one obvious way to correctly accomplish what they want (even if that is not the most efficient way)
  - the obvious way is: just do all your reads and writes to the stack cache/frame cache, and only update the sp and fp registers using special Boot instructions. If you ever need to access stack frames via ordinary memory or need to manually alter the sp and fp registers, surround with flushes.
- For Boot implementors, the semantics should be flexible
  - the semantics allow implementors a lot of freedom about when to actually flush/spill the stack/frame caches to memory


## Calling convention
### Why a separate return address stack?

First, this allows less overhead in function calls in some situations. For example, when factoring a function which doesn't use callee-save registers and which calculates using the stack into nested subroutines, if there is a separate data stack, then these subroutines can simply manipulate that data stack without any overhead aside from storing/restoring the return address and jumping; whereas if the return address is stored in the same stack as the data, either the subroutines must be modified to look past the return addresses (which breaks the 'stack frame' abstraction), or they must duplicate the data to provide arguments before each function call.

A more general way of looking at this is the concept of 'concatenative programming languages' (for example, Forth is a concatenative programming language), in which the juxtaposition of expressions denotes function composition. If the effect of a function call on program state is confined to pushing a return address to a call stack and jumping, then any subset of a function can be moved into its own function without alteration, and still perform the same operation on program state; that is to say, code which was previously juxtaposed (eg two sequences of instructions which were next to each other) can be refactored into function composition without alteration.

Second, this makes it slightly harder for stack overflow bugs to lead to security exploits.







# TODO


- boots is no longer a subset of boot; it has a different encoding now. The INSTRUCTIONS are a subset, though, with the same opcodes and semantics.

- bootS no longer exists, instead both boots and boot have corresponding extensions (boot0 is like the old bootS)

## future stuff
notes from old design doc. Not sure if/at what level we'll do this stuff, so just putting it here for now:

=== Why have devices instead of additional opcodes?

First, there are limited opcodes. It's efficient to save these opcodes for frequently used, fast operations, and to use something more cumbersome, like devices, for operations which are relatively slow (like I/O) or infrequent.

Second, the notion of a 'space' of devices located by device id provides conceptual economy. We can reuse the same device API for many instances of this object; for example, we have one channel control device API that we then use for every channel. At the same time, different types of devices can have different device APIs.

Third, all of these device APIs use the same few device opcodes, allowing code analysis tools to make some basic assumptions about which registers and memory addresses might be affected by a given device instruction even without understand the device type.

=== Why don't signals have attached data? Why don't you have a count of pending signals, instead of just a bit?
If signals had attached data, then the implementation might:
* store the attached data somewhere
* block the sender and store a record that says that that sender is waiting to deliver this signal
* block the sender, not record that, and do expensive polling when the signal is caught to find the sender and get their data

The last option is complex and inefficient, and all the others require the implementation to maintain some sort of buffer that could fill up if too many signals come in faster than they can be caught. Another option could be to provide an attached data option but not guarantee delivery; for example, if another signal of the same type comes in before the first one is handled, the latter signal's data could overwrite the former's. This would make the attached data less useful, however.

All of these options add edge cases for programs to work around, and all of them add implementation complexity (and also increase the latency of sending and receiving a signal). And if threads or processes want to send messages to each other, there is already a mechanism provided for that: channels.

Therefore, i chose for the effect of a signal only to set a one-bit flag (and to call the signal handler when the flag is set and the signal is unmasked). Signals are not mailboxes; signals are like the little flag on the mailbox.

Similarly, a count of pending signals could overflow or saturate.

=== Why doesn't receipt of a signal spawn a new thread, instead of interrupting an existing thread?
The whole point of signals is to provide a mechanism to interrupt processing in a single thread. If you want a new thread to handle some event, you can spawn one explicitly.

=== 11 GPRs is too few. Why aren't there more






=== Why do you allow pointer subtraction?
First, this allows us to serialize complex data structures containing pointers (assuming all of the pointers are 'derived' from a common ancestor) by converting the pointers into integer offsets from each other.

Second, this allows you to check how much memory is left until you run out of room when you have, e.g., a stack or array pointer, and a base pointer to the memory region the stack is in, and knowledge of how big that region is.

Third, this allows you to add an index to a base pointer, store both the base pointer and the new pointer but throw away the index, and then later recover the index.

=== Why do you require derived pointers for LEQ-PTR and SUB-PTR?
As noted above, pointers are supposed to be opaque, both to help prevent pointer errors, and also to allow the topology of memory to be something other than a single large array. A pointer and another pointer derived from it CAN be thought of as existing at integer offsets within a single larger array, but this is not generally true of any two pointers.

=== Why do you require that, even if pointers aren't derived, LEQ-PTR can't say both P1 <= P2 and also P2 <= P1?
Because, if the pointers were derived, then this would imply that they are equal. That could lead to confusing bugs.

=== Why do you require that, even if pointers aren't derived, SUB-PTR can't say both P1 < P2 and also P2 < P1?
Because in this case < seems similar to <=, and if it were <=, and if the pointers were derived, then this would imply that they are equal. So this could also lead to confusing bugs.




