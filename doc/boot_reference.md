# Boot reference

Version: unreleased (0.0.0-0)

VERY EARLY NOTES. PLANNING FOR A FUTURE PROJECT.

Boot is an 'assembly language'-style virtual machine (VM). It is an extension of Boots.

## Introduction

Boot is a platform-independent target language.

Highlights:
- 3-operand register machine
- hybrid/variable length instructions (8-, 16-, and 32-bits); the first byte of the instruction determines the length in a regular way.
- signed, unsigned, 8-, 16-, 32-, or 64- bit integers, and 32- or 64-bit floats
- integers and pointers both have implementation-dependent sizes in memory, and may be different sizes
- opaque pointer representation
- 32 general-purpose registers, out of which 6 registers have special uses (such as the stack pointer)
- 5 addressing modes: immediate, register, stack-pointer immediate offset, frame-pointer immediate offset, and stack push/pop
- provides both jumps and also structured control flow instructions
- annotation facility for labeling code with metadata or comments
- standard library with things like arrays, string processing, hashtables, map, reduce, and a garbage collector
- Boot is an extension of the Boots virtual machine -- Boots standard programs will also run on Boot

There is (will be) a defined textual assembly language for LOVM and there (will be) a macro-assembler available.

### Why not X instead of Boot?


- Boots: Boots is a simple 'assembly language'-style VM suitable for initial bootstrapping, offering 32-bit integer arithmetic and a single character-based I/O primitive. Boot, by contrast, is a comprehensive target language, with floating-point, 64-bit integers, and a variety of system calls. The Boot encoding is more complicated but more featureful than Boot's.
- LLVM: LOVM's encoding format is simpler than LLVM. LOVM also does not require the program to be in SSA CFG form.
- WASM: LOVM directly supports arbitrary 'assembly-like' control flow (jumps, conditional jumps, dynamic jumps), whereas WASM prohibits non-reducible control flow by only providing structured programming primitives.
- CLR: LOVM is much simpler than the CLR.
- JVM: LOVM is lower-level than the JVM. LOVM supports arbitrary 'assembly-like' control flow (jumps and conditional jumps), whereas JVM supports only structured control flow.
- C, Zig, Crystal, Nim, etc: LOVM is lower-level, smaller, and simpler than C (or C competitors such a Zig, etc). LOVM is a virtual register machine with a linear instruction sequence in binary, which is quicker to decode and interpret or compile than C's textual encoding. LOVM doesn't provide named variables, structs, arrays, strings, etc (as part of the language itself; some routines dealing with common data structures such as strings may be found in the standard library). LOVM is certainly much simpler than C++ competitors such as Rust or D.


## Architecture

### Differences from Boots
For readers already familiar with Boot, in this section we list all of the differences between Boots and Boot. Note that Boots is a subset of Boot, and valid Boots instructions and programs are also valid Boot instructions and programs, with identical semantics.

Boot offers the following features beyond Boot:
- 64-bit integer arithmetic
- 64-bit floating point arithmetic
- 32 floating point registers
- 4 new addressing modes: immediate, stack-pointer immediate offset, frame-pointer immediate offset, and stack push/pop
- additional instructions
- additional system library calls
- additional sinfo queries
- the possibility of register-offset caches with registers other than sp (and in particular, with register fp)
- operations which can change a register without potentially invalidating an associated register-offset cache: pushc, popc, cpc, some non-branching conditionals, and the stack push/pop addressing mode.
- variable instruction lengths (8-, 16-, or 32-bit)
- new undefined behaviors:
  - division by zero
  - freeing a pointer that was not created by malloc
  - freeing a pointer more than once

The Boot project (will provide) a compiler from Boot to Boots, with the proviso that unless the Boots target supports at least Boots extension profile Boots5D, instructions which compile to instruction not present on the target will fail to execute, and, if 64-bit arithmetic is absent, arithmetic operations will have different limits.

### Instruction encoding
There are 3 instruction lengths, 8-bit, 16-bit, and 32-bit.

#### 8-bit instruction encoding
- first  bit: 0

The all-zero instruction (8 bits of zeros) is illegal.

The other details of the 8-bit instruction encoding are RESERVED.

For the 8-bit and 16-bit encodings, we plan to wait until we have statistics on the frequency of 32-bit Boots instructions, and then try to construct regular-ish encodings that provide shorter ways to express instructions that are statically and dynamically frequent. It would be nice if the 16-bit encoding was a Turing-complete 'basis set' that could express all Boots 32-bit integer programs (albeit with fewer registers), but I'm not sure if this will be a goal.

NOTE: RISC-V noted that they constrained their compressed instruction to only expand to a single regular instruction to facilitate hardware implementation. We can satisfy the same constraint by being careful to add a new regular instruction whenever we find a multi-instruction sequences that would be useful in the compressed encoding.

#### 16-bit instruction encoding
- first  bit: 1
- second bit: 0

16-bit instructions must always be 16-bit aligned.

The other details of the 16-bit instruction encoding are RESERVED.


#### 32-bit instruction encoding
The three most-significant-bits (the first bit of the first byte) are always 110.

32-bit instructions must always be 32-bit aligned.

4 bytes (32-bits) per instruction.

- first 3 bits: 110
- 8 opcode bits
- 7 op0 bits (2 addr mode bits and 5 operand data bits)
- 7 op1 bits (2 addr mode bits and 5 operand data bits)
- 7 op2 bits (2 addr mode bits and 5 operand data bits)

So, listing the bits in each byte from most-significant-bit to least-significant-bit:
- the first byte  contains: bits 1,1,0 and then the most-significant 5 bits of the opcode
- the second byte contains: the least significant 3 bits of the opcode, followed by the most-significant 5 bits of op0
- the third byte  contains: the least significant 2 bits of the op0, followed by the most-significant 6 bits of op1
- the fourth byte contains: the least significant bit of op1, followed by op2

Some instructions take an additional 32-bit constant immediately following them. In such cases that 32-bit constant is in little-endian format.

When two immediate operands are combined into an #imm14 (as with instruction li), op1 is the high-order bits and op2 is the low order bits (imm14 = (op1 << 7) + op2). Similarly for #imm21 (imm21 = (op0 << 14) + (op1 << 7) + op2).

### Registers

32 'general purpose' registers:

- 0:     constant zero register. Writes to this register have no effect.
- 1:     return address (link register)  (caller-save)
- 2:     data stack pointer              (callee-save)
- 3:     return stack pointer            (callee-save)
- 4:     frame pointer                   (callee-save)
- 5:     result register                 (caller-save)
- 6:     scale register                  (caller-save)
- 7:     platform register               (caller-save)  TODO: mb this is silly and we should use the stack
- 8-17:  temporary general-purpose       (caller-save)
- 18-28: enduring  general-purpose       (callee-save)
- 29:    read-only pointer               (fixed)
- 30:    global pointer                  (fixed)
- 31:    thread pointer                  (fixed)

TODO: no need for a link register if we have a return stack right? b/c implementations can easily cache return stack TOS. but mb keep it there for the sake of implementation flexibility. also actually can't 'easily cache' if return stack is observable in memory.

General purpose registers are referred to by Rn where n is the register number, e.g. r0, r1, ..., r31.

Here is the intended usage of some of the registers. The link register holds the return address after a function call. The data stack pointers points to the top item on the data stack (which grows downwards in memory), and the return stack pointer points above the top item on the return stack (which grows upwards in memory). The frame pointer points to the first item on the stack on top of the arguments from the previous function. Sometimes instructions which don't have enough operands to specify where to put some results place them into the result register. The platform register is reserved for use by Boot tooling. The temporary general-purpose registers may be overwritten by function callees. The enduring general-purpose registers should hold the same values after a function call as they held before that function call. The read-only pointer points to the read-only segment of the executable. The global pointer points to a memory storage area for holding global values. The thread pointer points to a memory storage area for this thread.

Registers also have secondary nicknames that assist in remembering their functions:
- 0:       r0
- ra:      r1
- sp:      r2
- gp:      r3
- tp:      r4
- t0..t1:  r5..r6
- fp:      r7
- e0..e5:  r8..r13
- t2..t12: r14..r24
- e6..e12: r25..r30

A separate bank of 32 floating-point registers:
- 0:     constant zero register. Writes to this register have no effect.
- 1-8:   temporary floating-point  (caller-save)
- 9-23:  enduring  floating-point  (callee-save)
- 24-31: temporary floating-point  (caller-save)


Floating point registers are referred to by Fn where n is the register number, e.g. f0, f1, ..., f31.

Floating point register nicknames:
- 0:         f0
- ft0..ft7:  f1  .. f8
- fe0..fe14: f9  .. f23
- ft8..ft15: f24 .. f31



#### Register-offset caches (R.O.C.)


###### Register-offset cache

mv'd from from boot_reference:
Some implementations may maintain a cache of 16 words of memory at and above the address held in register 2 (R2).

The instructions lwc, lpc, swc, spc load and store words to an offset of the address stored in register 2, potentially using caching. The reason that special instructions are provided is that values in this "register-offset cache" may visibly differ from values in the backing memory until the next flush. The ordinary load/store instructions bypass the register-offset cache.

The register-offset cache is flushed to the backing memory by the instruction 'flushroc2mem', and the cache is reloaded from ("flushed from") memory with 'flushmem2roc'.

end mv'd from boot_reference:


Some implementations may maintain caches of words of memory at and above the addresses held in various registers.

The instructions lwc, lpc, swc, spc load and store words to an offset of the address stored in a register, potentially using caching. The reason that special instructions are provided is that values in this "register-associated cache" may visibly differ from values in the backing memory until the next flush. The ordinary load/store instructions bypass the register-associated cache.

A register-offset cache is flushed to the backing memory by the instruction 'flushroc2mem', and the cache is reloaded from ("flushed from") memory with 'flushmem2roc'.

### Addressing modes

In the 32-bit instruction encoding, every operand is either immediate, or it supports addressing modes. Which operands are immediate and which support addressing modes varies by instruction.

In operands in which addressing modes are supported, various addressing modes are available:
- immediate
- register
- stack (cache)
- frame (cache)
- stack push/pop (cache)
- displacement
- pre-decrement/post-increment

#### Addressing mode encoding

- immediate: a signed 6-bit constant is embedded in the encoded instruction. Integer reads from an immediate-mode operand return the operand data. Immediate 0 is identical to R0. Immediate mode is only available in operands which are read; for op0 operands which are only written to, this encoding space is used for register-memory displacement addressing mode instead.
- register: read/write act on one of the 31 registers. This may be a general-purpose register or a floating point register, depending on the type of the operand in the given instruction.
- stack cache: reads/writes a cache of items near the top of the stack. The index of the item to be accessed is specified as a tuple [PTR_SIZE_offset, INT32_SIZE_offset, INT16_SIZE_offset, INT8_SIZE_offset], where each item of the pair is an integer in the range 0 thru 1. To access the item on top of the stack, the index would be s[0,0,0,0]. If the item on top of the stack is an INT32_SIZE, then to access the second item, the index would be s[0,1,0,0]. If the two items on top of the stack are INT32_SIZE and PTR_SIZE, then to access the third item, the index would be s[1,1,0,0]. This mode does not push or pop, it merely reads or overwrites items which are possibly deep within the stack cache.
- frame cache: like stack cache addressing mode, except based on the frame pointer instead of the stack pointer. These locations are written a[x,y], where a[0,0] refers to the item that the frame pointer points to, etc.
- stack cache push/pop: reads pop off the stack cache (before the rest of the instruction is executed), writes push to the stack cache (after the rest of the instruction has been executed). If multiple operands have this addressing mode, then before the rest of the instruction executed, reads are applied in the order op2, then op1, then op0; and after the rest of the instruction has been executed, writes are applied in the order op2, then op1, then op0.
  
An operand that supports addressing modes has the following encoding. The 7-bit operand is treated as an unsigned 7-bit integer. The meaning of the value of this integer is:

- 0 thru 63: immediate mode. See below for interpretation
- 64 thru 93: register addressing mode (64 = r1, 65 = r2, ..., 94 = r31)
- 95 thru 110: stack cache addressing mode (95 = s[0,0,0,0], 96 = s[0,0,0,1], ... 99 = s[0,1,0,0],  ... 110 = s[1,1,1,1])
- 111 thru 126: frame cache addressing mode (111 = a[0,0,0,0], 112 = a[0,0,0,1], ..., 115 = a[0,1,0,0], ... 126 = a[1,1,1,1])
- 127: stack cache push/pop addressing mode

##### Immediate mode, when the operand is op0 (register-memory displacement addressing mode)

In op0, immediate mode has a special meaning (register-memory displacement addressing mode):
- if the value is 0:  any writes to op0 are discarded
- if the value is 32: RESERVED

- if the value is 1 thru 31 and op0 and op1 have the same type, the value encodes a register R, both op0 and op1 are replaced by op2, and op2 is replaced by effective address: (R + op1) (where op1 is interpreted as type integer; except that if op1 is immediate mode, R + f(op1) is used instead, see below)
- if the value is 33 thru 63, and op0 and op2 have the same type, the (value - 32) encodes a register R, both op0 and op2 are replaced by op1, and op1 is replaced by effective address: (R + op2) (where op2 is interpreted as type integer; except that if op2 is immediate mode, R + f(op2) is used instead, see below)
- f() is: ((op & 63) >>> 5)*PTR_SIZE + ((op & 31) >>> 4)*INT32_SIZE + ((op & 15) >>> 2)*INT16_SIZE + (op & 3)*INT8_SIZE

If the value is 1 thru 31 or 33 thru 63 but the 'same type' constraint is not met, the semantics are RESERVED.

##### Immediate mode, for operands other than op0 when the operand 'op' is of type ptr (null ptr, postinc, predec)

- 0: the null pointer
- 32: RESERVED
- 1 thru 31: postincrement, where the operand 'op' specifies the register, and the amount of increment is:
  - 1 if op mod 4 == 0
  - INT32_SIZE if op mod 4 == 1
  - PTR_SIZE if op mod 4 == 2
  - the value of R6 ('scale register') if op mod 4 == 3
- 33 thru 63: predecrement, where the operand 'op' specifies the register, and the amount of decrement is:
  - 1 if op mod 4 == 0
  - INT32_SIZE if op mod 4 == 1
  - PTR_SIZE if op mod 4 == 2
  - the value of R6 ('scale register') if op mod 4 == 3
 

##### Immediate constants

###### Int type operands

The 6 bits are treated as a twos-complement signed 6-bit integer. That is, 0 encodes 0, 1 encodes 1, ... 31 encodes 31, 32 encodes -32, 33 encodes -31, ..., 63 encodes -1.

###### Label type operands
- all: RESERVED



###### Handle type operands
- 0: stdin (when read from) and stdout (when written to), where available
- 1: stdin, where available
- 2: stdout, where available
- 3: stderr, where available
- 4 thru 63: RESERVED


###### Floating point type operands

Mapped from the operand data represented as a 6-bit unsigned integer:

- 0:  +0.0
- 1:   0.0625 (subnormal)
- 2:   0.125  (subnormal)
- 3:   0.1875 (subnormal)
- 4:   0.25   (normalized number)
- 5:   0.3125
- 6:   0.375
- 7:   0.4375
- 8:   0.5
- 9:   0.625
- 10:  0.75
- 11:  0.875
- 12:  1.0
- 13:  1.25
- 14:  1.5
- 15:  1.75
- 16:  2.0
- 17:  2.5
- 18:  3.0
- 19:  3.5
- 20:  4.0
- 21:  5.0
- 22:  6.0
- 23:  7.0
- 24:  8.0
- 25: 10.0
- 26: 12.0
- 27: 14.0
- 28: +infinity
- 29: NaN (signaling)
- 30: NaN (quiet)
- 31: NaN (quiet)

32..63 repeat the same values, but negated:

- 32:  -0.0
- 33   -0.0625
- ...
- 63: NaN (quiet)

Although both quiet and signaling NaNs are listed here, it is implementation-dependent whether NaNs are distinguishable or whether signaling NaNs behave differently; implementations are permitted to treat all NaNs as quiet and/or to treat all NaNs the same and/or to replace any NaN with any other NaN, even nondeterministically.

This representation is like the IEEE 754 floating point representations, but with 6 bits: 1 sign bit, 3 exponent bits, 2 significand bits.



## Boot instructions

Notation for the instruction list below: #imm7 #imm14 #imm21 are unsigned immediate constants, $X/&X/@X/~X is an register or its contents, 0 is an unused argument that must always be 0 (other values are RESERVED for future use), and similarly for &2 and #f (immediate constant 16, written in hex). All signed immediate constants are two's-complement. "; #imm32" denotes that the 32-bit constant immediately follows the instruction in the instruction stream, in little-endian format.

From left to right, the arguments go into operands op0, op1, op2 (for example, in "cp $1 $2 0", the instruction mnemonic is 'cp', op0 is $1, op1 is $2, op2 is 0). Immediate operands are always on the right (the highest-numbered operand).


### Boots instructions
All Boots instructions are also Boot instructions, with the same semantics. See the Boots reference.

### TODO define 'core Boot', which is the subset that can be easily compiled to Boots

TODO
64 bit integer ops


### Floating-point instructions

The following new instructions are defined:
| integer division 1 | div32
| floating point 1 | fadd fsub fmul fdiv fcmp i32tof ftoi32 fcp fld fst beqf bltf floor ceil nearest trunc binf bnan

f32add f32sub f32mul f32div f32cmp i32tof32 f32toi32 fcp fld fst beqf bltf floor ceil nearest trunc binf bnan

TODO specialize instructions for fp32 and fp64

TODO lif32 and lif64 to load floating point values from 32- and 64-bit immediate constants embedded in the instruction stream

Notes:
- fcmp also serves as fclass; it returns a status word TODO
- the conversions f64tof32, f64toi, f32toi truncate



todo define behavior of immediates in i32tof and ftoi32

32-bit floating point ops:
64-bit floating point ops:



more floating point ideas:


### Floating point 1 instruction extension ###

| floating point | i2f lf sf ceil flor trunc nearest addf subf mulf divf copysign bnan binf beqf bltf fcmp 

could save a few fp opcodes by having ftoi take an immediate operand specifying rounding type: floor ceil round nearest. mb could elide either bltf beqf or fcmp. could compress isinf isnan into fclass like riscv does. so: fadd fsub fmul fdiv fclass fcmp itof ftoi fcp fld fst. thats still a lot tho.


### Floating point 2 instruction extension ###
Includes Floating point 1 and adds:

| additional floating point | remf sqrt minf maxf powf bnonfinite bgtf beqtotf blttotf ftotcmp

fma (a = a + b*c)
fma (a = a*b + c)

(there are apparently more fma instructions, compare eg VFMADD132SS to VFMADD213SS in eg https://en.wikipedia.org/wiki/FMA_instruction_set ; i don't understand why tho)


### Floating point Triglog instruction extension ###
Includes Floating point 2 and adds:

| additional floating point | TODO

trig, flog (floating point log), exp etc fns from math.h (at this point are we missing any other math from C library or math.h?)

### Floating point elusive eight instruction extension ###

| additional floating point | TODO

TODO

https://www.evanmiller.org/statistical-shortcomings-in-standard-math-libraries.html#functions





### I/O instructions
| I/O 1 | in out devop

- in &dest $size ~device:  read  $size bytes of input  from ~device to buffer &dest. Number of bytes read is pushed onto the stack cache. In case of an error, a zero or negative error code is pushed onto the stack.
- out &src  $size ~device: write $size bytes of output to ~device   from buffer &dest. Number of bytes written is pushed onto the stack cache. In case of an error, a negative error code is pushed onto the stack. In case of an error, a zero or negative error code is pushed onto the stack.
- devopi $dest $op ~device: implementation-dependent control operation $op on ~device. Result code stored to $dest.1
- devopp $dest &op ~device: implementation-dependent control operation on ~device; struct with data and/or operation 0found at &op. Result code stored to $dest.


---



todo this isn't quite enough return values; in case of an error where there was a partial write, for example, need to return both the number of bytes read, and the error code. Should either have implicit return regs, or return a pointer to a struct, or a packed struct in the dest register, or have a 'ioerror' function to fetch the error code.

todo in and out for p,l,h?

todo should in and out be syscalls? That would mean implementations could feel free to overwrite caller-saved regs. I'm thinking no; there are situations where I/O perf is critical.

todo right now i didn't specify blocking or nonblocking b/c i figure different platforms will offer different things. Is that what we want?


### Base atomics (release consistency)
* Atomics rc 1



### Atomics seqc 1 instruction extension ###

| atomics (sequential consistency) | lpsc lwsc spsc swsc casrmwsc casprmwsc fencesc

### Atomics seqc 2 instruction extension ###
| atomic additional rmw ops (sequential consistency) | addrmwa aprmwa andrmwa orrmwa xorrmwa


### Atomics rc 1 instruction extension ###

| atomics (release consistency) | lprc lwrc sprc swrc casrmwrc casprmwrc



### Atomics rc 2 instruction extension ###
| atomics (relaxed consistency) | lprlx lwrlx sprlx swrlx casrmwrlx crsprmwrlx
| atomic additional rmw ops (release consistency) | addrmwrc aprmwrc andrmwrc orrmwrc xorrmwrc addrmwrc
| atomic additional rmw ops (relaxed consistency) | addrmwrlx aprmwrlx andrmwrlx orrmwrlx xorrmwrlx addrmwrlx

TODO: aren't the normal Boots operations already relaxed consistency?


atomics ideas:


atomics (sequential consistency):
* casrmw{sc,rc,rlx} &dest $new $old: compare-and-swap atomic (must be within the same memory domain). Upon success, $3 = $new; otherwise, $3 = the contents of &dest. The sc/rc/rlx indicates one of sequential consistency, release consistency (casrmwrc is both an acquire and a release), or relaxed semantics.
* casrmw{sc,rc,rlx}p &dest &new &old is like casrmw{sc,rc,rlx}, but where the values are pointers instead of integers (and &3 is used instead of $3).
* fencesc $memory_domain _ _: instruction/memory access reordering barrier; prevents any memory operations on the given memory_domain from appearing to be reordered across the FENCE instruction. Sequential consistency semantics.
* malloc_shared &dest $size $memory_domain: Requests allocation of a block of $size bytes of memory in memory domain $memory_domain. If successful, a pointer to the new block is stored at &dest; otherwise the null pointer (&0) is stored as &dest. memory_domain is RESERVED for future use; always use $0 for now.
* malloc_local &dest $size: Like malloc_shared but the allocated memory must only be used for thread-local storage. All atomic operations lose their atomicity and ordering guarantees when acting on local memory (e.g. so lpsc becomes equivalent to ordinary lp, etc).
* mrealloc_local  &dest $newsize &oldptr: attempts to allocate a new block of local memory of size $newsize, copy the contents of the entire block at &oldptr into it, and then mfree &oldptr. If it succeeds, the new block is assigned to &dest; if it fails, the null pointer (&0) is assigned to &dest; in this case &oldptr is not mfree'd.
* mrealloc_shared &dest $newsize &oldptr: attempts to allocate a new block of memory of size $newsize &oldptr
* mfree &src: deallocates &src
* {lp,lw,sp,sw}{sc,rc,rlx} are like {lp,lw,sp,sw} but atomic, and with {sequential consistency, release consistency, or relaxed} semantics, respectively.

(also need an instruction to flush icache? this might belong in some sort of self-modifying extension tho b/c a boot->platform compiler/interpreter might not be available at runtime)


Relaxed semantics operations are atomic but provide no other guaranteed beyond their corresponding non-atomic variants. Release Consistency semantics are defined later but if you are familiar with it, they are RCpc; that is, the ordering operations themselves are ordered with Processor Consistency semantics. Release Consistency loads are acquires and stores are releases. Release Consistency also implies atomicity. Sequential Consistency operations provide the same guarantees as the corresponding Release Consistency operation, and in addition all Sequential Consistency operations also appear in program order in a single total order over this memory_domain observed by all threads along with all other sequentially consistent instructions.



The motivation for malloc_local is that, in order to be able to provide the concurrency guarantees required by this spec, some Boot implementations may create and use locks to control access to blocks of shared memory returned by malloc; in some cases even non-atomic, unordered load or store instructions could cause the Boot implementation to acquire a lock. malloc_local lets such an implementation know that it does not have to setup and use locks for this memory segment, and represents an assurance by the programmer that this memory segment will only ever be accessed by the same thread that called malloc_local.



Note that an implementation may legally provide sequential consistency when the program requests only release or relaxed consistency; furthermore, the additional RMW ops may be implemented using the CAS primitive; therefore, all of the atomics in the optional instructions may legally be implemented using only the atomics in the small profile as primitives.




- double-wide CAS?




### Bitwise
- not  $dest $src1 $cond: if $cond == 0, then $dest = bitwise_NOT($src1)


bitset
bitclr
bitinv (single bit invert)
bittst (single bit test)
clz
ctz
popcnt (pcnt/popcount)
pack (pack 2 32-bit integers in one 64-bit integer)
unpack

### Arith

div32 mod32/rem32
?divmod32?

floating point and other arith and other instrs from wasm and llvm

### Conversions

conversion:
- sext
- zext
- loads without any sign extension
- coerce between signed/unsigned integers
- what else?


"ARMv8 side-steps the whole problem by providing indexed memory addressing modes that include the complete suite of zero and sign extension of a narrow-width index in the load or store instruction itself."
"The problem is that providing extra bits for "sign-extension mode" and "read 32b or 64b" blows through the opcode space very quickly."



### Register-offset cache
| register-offset cache | lwc lpc llc lhc swc spc slc shc flushroc2mem flushmem2roc

- lwc  $dest &2    #imm7 : Load Word Cached         from memory addr &2 + #imm7*PTR_SIZE
- lpc  &dest &2    #imm7 : Load Ptr  Cached         from memory addr &2 + #imm7*PTR_SIZE
- llc  @dest &2    #imm7 : Load Label  Cached       from memory addr &2 + #imm7*PTR_SIZE
- lhc  ~dest &2    #imm7 : Load Handle Cached       from memory addr &2 + #imm7*PTR_SIZE
- swc  $src  &2    #imm7 : Store Word Cached        to   memory addr &2 + #imm7+PTR_SIZE
- spc  &src  &2    #imm7 : Store Ptr  Cached        to   memory addr &2 + #imm7+PTR_SIZE
- slc  @src  &2    #imm7 : Store Label Cached       to   memory addr &2 + #imm7+PTR_SIZE
- shc  ~src  &2    #imm7 : Store Handle Cached      to   memory addr &2 + #imm7+PTR_SIZE
- flushroc2mem 0 &2 #f: flush register-offset cache to memory
- flushmem2roc 0 &2 #f: flush memory to register-offset cache

    
    56,lwc,0,ri,rp,u7,1,0,0,1,0,0,0,0,0,1
    57,lpc,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,1
    58,llc,0,rl,rp,u7,1,0,0,1,0,0,0,0,0,1
    59,lhc,0,rh,rp,u7,1,0,0,1,0,0,0,0,0,1
    60,swc,0,ri,rp,u7,0,1,0,1,0,0,0,0,1,0
    61,spc,0,rp,rp,u7,0,1,0,1,0,0,0,0,1,0
    62,slc,0,rl,rp,u7,0,1,0,1,0,0,0,0,1,0
    63,shc,0,rh,rp,u7,0,1,0,1,0,0,0,0,1,0
    64,flushmem2roc,0,0,rp,u7,0,0,0,1,0,0,0,0,0,1
    65,flushroc2mem,0,0,rp,u7,0,0,0,1,0,0,0,0,1,0

### Adding ints to Pointers
| adding ints to pointers | app ap32 ap16 apm appm ap32m ap16m apmn appmn ap32mn ap16mn

- app    &dest &src1 $src2: &dest = &src1 + $src2*PTRD_SIZE
- ap64   &dest &src1 $src2: &dest = &src1 + $src2*INT64_SIZE
- ap32   &dest &src1 $src2: &dest = &src1 + $src2*INT32_SIZE
- ap16   &dest &src1 $src2: &dest = &src1 + $src2*INT16_SIZE
- apm    &dest &src1 #imm3: &dest = &src1 + #imm7
- appm   &dest &src1 #imm3: &dest = &src1 + #imm7*PTRD_SIZE
- ap64m  &dest &src1 #imm3: &dest = &src1 + #imm7*INT64_SIZE
- ap32m  &dest &src1 #imm3: &dest = &src1 + #imm7*INT32_SIZE
- ap16m  &dest &src1 #imm3: &dest = &src1 + #imm7*INT16_SIZE
- apmn   &dest &src1 #imm3: &dest = &src1 - #imm7
- appmn  &dest &src1 #imm3: &dest = &src1 - #imm7*PTRD_SIZE
- ap64mn &dest &src1 #imm3: &dest = &src1 - #imm7*INT64_SIZE
- ap32mn &dest &src1 #imm3: &dest = &src1 - #imm7*INT32_SIZE
- ap16mn &dest &src1 #imm3: &dest = &src1 - #imm7*INT16_SIZE


    66,19,apm,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    67,appm,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    68,ap32m,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    69,ap16m,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    70,apmn,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    71,appmn,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    72,ap32mn,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0
    73,ap16mn,0,rp,rp,u7,1,0,0,1,0,0,0,0,0,0

    144,42,app,0,rp,rp,ri,1,0,0,1,0,1,0,0,0,0
    145,43,ap32,0,rp,rp,ri,1,0,0,1,0,1,0,0,0,0
    146,44,ap16,0,rp,rp,ri,1,0,0,1,0,1,0,0,0,0

- mb also some stuff for larger 'displacement' addr-mode-like calc (since the above only gives 7 bit immediates). E.g. &srcdest #imm14. 
  - see below "bigger displacments (https://passlab.github.io/CSCE513/notes/lecture03_ISA_Principles.pdf "iron-code summary" suggests at least 12 bits"
- so mb also an overwriting load that does that:
  - load &srcdest #imm14
- and/or a load with a fixed src or dest register:
  - load &src  #imm14 (dest is implicitly push-to-stack)
  - load &dest #imm14 (src is implicitly push-to-stack)
  - load &src  #imm14 (dest is implicitly accumulator register)
  - load &dest #imm14 (src is implicitly accumulator register)
  - load &src  #imm14 (dest is implicitly TOS)
  - load &dest #imm14 (src is implicitly TOS)




- addoi32p &dest $int32s $ptrs:  &dest = &dest + int32s*INT32_SIZE + ptrs*PTR_SIZE
- addoi32pm &dest #int32s_imm7 #ptrs_imm7:  &dest = &dest + int32s_imm7*INT32_SIZE + ptrs_imm7*PTR_SIZE
- addeoi32pm &dest &src #imm7: &dest = &src + (imm7 >>> 3)*INT32_SIZE + (imm7 & 15)*PTR_SIZE


notes:
- ap, app, ap32, ap16: the int32 argument $src2 is interpreted as signed, so although only addition is provided, subtraction can also be accomplished


### Compare-and-branch
| comparision control flow | bnef bltf bltuf blef bleuf bnepf bnelf bnehf bneb bltb bltub bleb bleub beqb bnepb beqpb bnelb beqlb bnehb beqhb

forward conditional branches:
- bnef  $src0 $src1 #imm7: Branch-if-Not-eQual, forward
- bnepf &src0 &src1 #imm7: Branch-if-Not-Equal on Ptrs, forward
- bnelf @src0 @src1 #imm7: Branch-if-Not-Equal on Labels, forward
- bnehf ~src0 ~src1 #imm7: Branch-if-Not-Equal on Handles, forward

- blef  $src0 $src1 #imm7: Branch-if-Less-Than-Or-Equal, forward
- bleuf $src0 $src1 #imm7: Branch-if-Less-Than-Or-Equal-Unsigned, forward


    48,blef,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    49,bleuf,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0

    51,bnepf,0,rp,rp,u7,0,1,0,1,0,0,1,1,0,0
    53,bnelf,0,rl,rl,u7,0,1,0,1,0,0,1,1,0,0
    55,bnehf,0,rh,rh,u7,0,1,0,1,0,0,1,1,0,0


notes:
- bnef, bnepf, bnelf, bnehf, blef, bleuf: forward branch to an offset of unsigned #imm7 bytes relative to the instruction after the branch

backward conditional branches:
- beqb  $src0 $src1 #imm7: Branch-if-EQual, backward
- beqpb &src0 &src1 #imm7: Branch-if-EQual on Ptrs, backward
- beqlb @src0 @src1 #imm7: Branch-if-EQual on Labels, backward
- beqhb ~src0 ~src1 #imm7: Branch-if-EQual on Handles, backward
- bneb  $src0 $src1 #imm7: Branch-if-Not-eQual, backward
- bnepb &src0 &src1 #imm7: Branch-if-Not-Equal on Ptrs, backward
- bnelb @src0 @src1 #imm7: Branch-if-Not-Equal on Labels, backward
- bnehb ~src0 ~src1 #imm7: Branch-if-Not-Equal on Handles, backward
- bltb  $src0 $src1 #imm7: Branch-if-Less-Than, backward
- bltub $src0 $src1 #imm7: Branch-if-Less-Than-Unsigned, backward
- bleb  $src0 $src1 #imm7: Branch-if-Less-Than-Or-Equal, backward
- bleub $src0 $src1 #imm7: Branch-if-Less-Than-Or-Equal-Unsigned, backward

    32,beqb,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    33,bneb,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    34,bltb,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    35,bltub,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    36,bleb,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    37,bleub,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    38,beqpb,0,rp,rp,u7,0,1,0,1,0,0,1,1,0,0
    39,bnepb,0,rp,rp,u7,0,1,0,1,0,0,1,1,0,0
    40,beqlb,0,rl,rl,u7,0,1,0,1,0,0,1,1,0,0
    41,bnelb,0,rl,rl,u7,0,1,0,1,0,0,1,1,0,0
    42,beqhb,0,rh,rh,u7,0,1,0,1,0,0,1,1,0,0
    43,bnehb,0,rh,rh,u7,0,1,0,1,0,0,1,1,0,0


notes:
- beqb, beqpb, beqhb, beqhb, bneb, bnepb, bnelb, bnehb, bltb, bltub, bleb, bleub: backward branch to an offset of -(unsigned #imm7) bytes relative to the instruction after the branch
- blef, bleb: the int32 arguments (in $src0 and $src1) are signed
- bleuf, bleub: the int32 arguments (in $src0 and $src1) are unsigned

### Stack ops
pushi popi pushp popp pushisc popisc pushpsc poppsc

Stack ops (for each of: any pointer to any in-memory stack, stack pointer register-associated cache):
dup
drop
swap
over

rot
pick
roll
nip
tuck

stackwrite (like pick, but writing)
stack_sizeof

note: pick and roll with immediate constants generalize many of the others (eg dup swap over rot)


note: these ops need to be provided in various forms / to take operands specifying the size of the items being operated upon/moved over, to allow programs to deal with stacks with different sized items:
- all combinations of INT32_SIZEs and PTR_SIZEs
- slots of max(INT32_SIZE, PTR_SIZE), and double-slots
- slots of max(INT64_SIZE, PTR_SIZE)

TODO mb: Since the stack layout is opaque, this means that if these are used on user stacks, the layout of those stacks is opaque, too, and so those user stacks can only be manipulated with these primitives. stack_sizeof is then provided to compute the size of a given stack.



### constant tables


- | constants and constant tables | lki32 lkp lkl lkh jk

* lkei &dest #imm14: LoaD K-th Int   encoding immediate constant into &dest
* lkef &dest #imm14: LoaD K-th Float encoding immediate constant into &dest
* lkep &dest #imm14: LoaD K-th Ptr     encoding immediate constant into &dest
* lkel &dest #imm14: LoaD K-th Label   encoding immediate constant into &dest
* lkeh &dest #imm14: LoaD K-th Handle  encoding immediate constant into &dest (eg 10 = 7.5)

* lki32 &dest #imm14: LoaD K-th Int32   constant into &dest
* lki64 &dest #imm14: LoaD K-th Int64   constant into &dest
* lkf32 &dest #imm14: LoaD K-th Float32 constant into &dest
* lkf64 &dest #imm14: LoaD K-th Float64 constant into &dest
* lkp &dest #imm14: LoaD K-th Ptr     constant into &dest
* lkl &dest #imm14: LoaD K-th Label   constant into &dest
* lkh &dest #imm14: LoaD K-th Handle  constant into &dest
* jk #imm21: Jump to the #imm21-th label in the label constant table

The Boot standard does not provide a way to specify the values in the constant tables; this is implementation-dependent.


### function calls/activation records

If more variables are needed than can fit in registers, up to 255 local variables can be allocated using FENTRY. These are stored in the current stack frame.
* lki32 &dest #imm14: LoaD K-th Int32   constant into &dest
* lki64 &dest #imm14: LoaD K-th Int64   constant into &dest
* lkf32 &dest #imm14: LoaD K-th Float32 constant into &dest
* lkf64 &dest #imm14: LoaD K-th Float64 constant into &dest
* lkp &dest #imm14: LoaD K-th Ptr     constant into &dest
* lkl &dest #imm14: LoaD K-th Label   constant into &dest
* lkh &dest #imm14: LoaD K-th Handle  constant into &dest
* jk #imm21: Jump to the #imm21-th label in the label constant table

The Boot standard does not provide a way to specify the values in the constant tables; this is implementation-dependent.


### function calls/activation records

If more variables are needed than can fit in registers, up to 255 local variables can be allocated using FENTRY. These are stored in the current stack frame.

ideas (just copy-and-pasted from old LOVM draft, there may be some details here that no longer make sense):


Function calling:
- CALL
- RET
- RETLEAF
- FENTRY

- also want 'lower level' instructions making up the parts of the above (eg FALLOC)
  - mb have a form of falloc which can allocate a very high number of locals?
  - mb call FALLOC NEWFRAME

- also might need some functions for coroutines, and for saving/restoring during greenthread context switches?


In the prolog to a function, the FENTRY instruction:
- takes as immediate inputs the expected/needed number of input arguments (up to 16), callee-save registers (up to ??<16), primary stack capacity (up to 16), and locals (up to 255).
  - TODO: 1 bit left, i assume we want to RESERVE that bit
    - actually i think we should use that bit to indicate if this is a function that might ever calls another function (without tailcalling); if so, we need to push the link register onto the stack right now, and we must always use RET to return; if not, then we must not push the link register now, and we must always use RETLEAF to return.
  - TODO: no caller-save registers (up to ??<16)? so those are 'always on'?
  - TODO: shouldn't we provide some funky way to get more locals in the LOVM-managed activation frame if we really really want it? Is it as easy as moving SP down first? Also shouldn't there be a way to dynamically allocate activation frames, in case it's really needed?
    - leaning yes, there should be a way to allocate many many (2^16? 2^32? probably 2^16) locals, and to dynamically allocate locals
    - TODO typically must make room for at least 1 primary stack temporary, i guess, so that R1 can be used? Can we turn this off if no primary stack at all is allocated?
- maps the input arguments on the memory stack to the primary stack
- saves the caller's frame pointer to the memory stack, and then sets the frame pointer register to the stack pointer
  - FENTRY assumes that the place to spill to is whatever the stack pointer points to; this is how you could have an allocation record on the heap
- as needed, saves any other callee-save variables to the memory stack
- adjusts the capacity of the primary stack, the output stack, and the registers to the specified values
  - the 'capacity' of registers reflects TODO HERE

In a function call, the CALL/TAILCALL instruction:
- (if a tailcall) restores the caller's frame pointer
- (if a tailcall) restores other callee-save registers, if any
- flushes the primary stack and the output stack onto the memory stack
- (if not a tailcall): put the return address into the link register
- transfer control to the function
  - if the target of the CALL instruction is an ENTRY instruction, the implementation may combine the two for efficiency

In a function return, the RET/RETLEAF instruction:
- is passed the number of callee-save registers to restore
- restores any other needed callee-save registers
- copy the frame pointer into the stack pointer
- restores the caller's frame pointer
- restores the caller's link register (if not RETLEAF; if RETLEAF then we never changed it, so it shouldnt be restored)
- jumps to what the link register was before it restored the caller's link register

- also might need some functions for coroutines, and for saving/restoring during greenthread context switches




locals:
lloc
sloc

- alloca (dynamic allocation within stack frame)
  - note tradition alloca has trouble expressing when no memory is available, but our version could return a ptr and return the null ptr when that happens

- probably 'leaf function', besides being an actual leaf, should not use the stack at all. Mb find a different term for that. 

- two stacks: data stack and return stack (can allocate in same block of memory, with data stack starting at the top and growing down, and return stack starting at the bottom and moving up; this way it occupies the same amount of memory as if we just had one call stack and put both data and return addressess in the call stack as usual)
  - this makes it even more important to use a name other than 'leaf function'; functions which don't use any callee-save registers, even if not leaves, can pass their input arguments as arguments to functions they call without putting any saved registers in the middle, and they don't need to create a frame pointer; they can do Forth-like stack manipulation to compute. This means that refactoring a function into two parts, one of which is called by the other, assuming the callee takes the same params as were on the stack of the caller when the callee code was entered before refactoring, only has to have the overhead of pushing/popping the return address, and the call and return (just jumps) -- without the overhead of establishing frame pointers, arranging function arguments, saving and restoring registers other than the link register.



### other structured programming

structured programming/loop helpers:
(like in Lua)

switch


### arrays
TODO see llvm (and similar) for inspiration


### feature checking

- add query-sinfo-then-branch-forward-if-result-less-than-X, with the query and X as immediates (kinda like our version of .ifdef; because the compiler can deal with it at compile time). This could help us with detecting: Boots version #, profile, bitwidth. Is there a way to also make use of this to tell the compiler 'we demand this feature so don't compile if it's not present on the target'? Like, have the branch offset be 0 in that case? We don't want to make the compiler implementor work too hard though. Maybe add an 'error'/'panic' instruction? I dunno, the program can just use 'halt' for that already.
- add branch-forward-if-feature-not-present, with the feature in the immediate (kinda like our version of .ifdef)
- consider also adding variants of each of query-sinfo-then-branch-forward-if-result-less-than-X and branch-forward-if-feature-not-present that simply demand that the feature is present (to make it easy for the compiler to error out if the feature is not present on the target platform). Maybe this could just be one opcode, since we don't need to encode a branch offset. Is the compiler allowed to err out if this is anywhere in the program, or only if it will be executed? Maybe the latter, but then recommend that programmers put a stream of these at the beginning of the program (if a prefix of the program contains only these, then the compiler can assume that that entire prefix will be executed)


### misc



| misc | log hint0-15 impl0-15 nop

- nop ? ? ?: do nothing
- impl* ? ? ?: IMPLementation-dependent instruction
- hint* ? ? ?: HINTs may be executed as NOPs. They are intended for forward compatibility; later versions of the specification may define semantics for various HINTs with the understanding that some implementations may execute them as NOPs.
- log: TODO TODO TODO. Logging (operands should somehow specify at least loglevel and the thing to be logged. Should the thing to be logged be pointed to or should it be an immediate integer? Maybe we should have one pointer operand and one integer opeand and one loglevel/other metastuff operand? Or should we have one 'messagetype' operand and one 'messageargument' operand? I guess we don't need more than 16 loglevels (and even that is a lot, we really probably only need 8; however i guess no one will complain if we give the option of more)


### More/todo
* push, pop, mb something like 'call' and 'ret' (using the link register, i guess). mb have 16 regs and have 4 cp16 commands

64-bit jumps


* addi $dest $src1 #imm8: $dest = $src1 + #imm8

* jt $index #imm16: Jump to index within local jump table (jump table is embedded in instruction stream immediately following JT instruction; table length is #imm16 (so there are #imm16 32-bit entries in the table, taking up the same space as #imm16 Boot instructions). Each table entry is a 32-bit signed integer offset, in bytes, from the program location following the end of this jump table (since Boot instructions are always 32-bits, these offsets should always be a multiple of 4; if the platform stores Boot instructions in some other format it may need to adjust these offsets before executing the jump). The quantity $index is interpreted as an unsigned index into this table. If $index is less than #imm16, a jump is performed to the program location specified by the offset in the table entry at the given index; if the index provided is greater than or equal to #imm16, then execution continues from the program location following the end of this jump table (equivalent to a jump to a table entry of offset 0))

concurrency rw instructions (loads/stores with various memory orders)
concurrency rmw instructions (cas, etc)
concurrency process management instrs (fork etc)



### Notes

- shl, shru, shrs: even though op2 has 7 bits, the #imm5u must be within the range 0-31


## Boot instruction tables

TODO check the following tables


## 
New opcodes:
opcode, mnemonic, has_i32_data, op0_type, op1_type, op2_type, op0_w, op0_r, PC_w, PC_r, mem_w, mem_r
,div32,0,ri,ri,ri,1,0,0,0,0,0
,fadd,0,rf,rf,rf,1,0,0,0,0,0
,fsub,0,rf,rf,rf,1,0,0,0,0,0
,fmul,0,rf,rf,rf,1,0,0,0,0,0
,fdiv,0,rf,rf,rf,1,0,0,0,0,0
,fcmp,0,ri,rf,rf,1,0,0,0,0,0
,beqf
,bltf

2-operand ones(?):
,i32tof,0,rf,ri,u3,1,0,0,0,0,0
,ftoi32,0,ri,rf,u3,1,0,0,0,0,0
,fcp,0,rf,rf,ri,1,0,0,0,0,0
,fld,0,K,rf,rp,u3,1,0,0,0,0,1
,fst,0,rf,rp,u3,0,1,0,0,1,0
,floor
,ceil
,nearest
,trunc
,binf
,bnan



todo: replace Ks with constants

todo: expand headers to match metadata table in boot_reference.md
todo: make simplified 'decode table' like in boot_reference.md
todo: those all hold for the other tables in here, too



## sinfo queries


ideas:
- profiles
- capabilities/extensions? FEATURES bitmask?
- is instruction N supported? (subquery?)
- is syscall N supported? (subquery?)

more ideas:

- 04: INT64_SIZE
- 05: UINTMAX_16 (= 65535, 0xffff hex)
- 06: UINTMAX_32 (= 4294967296, 0xffffffff hex)
- 07: UINTMAX_64 (= todo decimal, hex)
- 08: INTMAX_16  (= 32767 decimal, 0x7fff hex)
- 09: INTMAX_32  (= 2147483647 decimal, 0x7fffffff hex)
- 0a: INTMAX_64  (= todo decimal, hex)
- 0b: INTMIN_16  (-32768 decimal, -0x8000 hex)
- 0c: INTMIN_32  (= -2147483648 decimal, -0x80000000 hex)
- 0d: INTMIN_64  (= todo decimal, hex)
- 0e-0x1fff: RESERVED for extensions
- 0x2000-0x3fff: implementation-defined

Boot guarantees that:

- INT8_SIZE = 1
- 1 <= INT16_SIZE <= 2
- 1 <= INT32_SIZE <= 4
- 1 <= INT64_SIZE <= 8
- 1 <= PTR_SIZE
- INT16_SIZE <= INT32_SIZE <= 2*INT16_SIZE  (an int32 is not smaller than an int16, and an int32 can fit within the size of two int16s)
- INT32_SIZE <= INT64_SIZE <= 2*INT32_SIZE (an int64 is not smaller than an int32, and an int64 can fit within the size of two int32s)
- INT32_SIZE <= PTR_SIZE (an int32 can fit within the size of a pointer)

Note: because INT32_SIZE <= PTR_SIZE, a program can opt to use fixed-size 'slots' of size PTR_SIZE to hold things. A slot of size PTR_SIZE is guaranteed to fit any of: an int8, and int16, an int32, a ptr, a label, a handle. Two slots of PTR_SIZE can fit an int64.


## Boot syscalls

### halt and memcpy
Both Boots syscalls (halt and memcpy) are also Boot syscalls. See the Boots reference.



## Local memory allocation

### lib 2: malloc(size: $5)
Memory allocate a new region of SIZE bytes and return a pointer to the beginning of it.


### lib 3: mfree(region: &5)
Free a region of memory beginning at pointer REGION.

REGION argument must have been returned by a previous malloc, and must not have been previously mfree'd.




## More syscalls TODO


memory: maybe some way to query how much memory is left, and how much stack space is left

* Clocks
* Environment Variables
* Filesys
file rw seek
file handle management open/close
file management mv attributes

* Process control
* Shared memory allocation
* TUI
* IPC 1
* IPC 2
networking
python event loop
nonblocking io


TODO syscalls inspiration:
plan9, klambda, posix, windows, macos, musl libc, android, ios, aws, freertos, python, l4, lists of frequent syscalls, wasm
- pico8 https://www.lexaloffle.com/bbs/?tid=28207


tui:
setcursorabsolute, setcursorrelative, getdimensions, setdimensions, clearscreen, printcharatcursor, getchar

graphics
setpixel, getpixel, setpalette, setmode, getmodes, setcustommode? (custom screen size, custom #s of colors)

audio






### Filesys syscall extension ###

Syscalls: TODO explain syscalls only; mb put these sort of extensions under a separate heading?
| filesys | read write open close seek flush poll

TODO check out Golang's new io.fs

### Environment variables extension ###


### IPC 1 syscall extension ###
TODO

### IPC 2 syscall extension ###
TODO

### TUI syscall extension ###
TODO


### Process control syscall extension ###
TODO

### Local memory allocation syscall extension ###
TODO

### Shared memory allocation syscall extension ###

| memory allocation | malloc_shared malloc_local

(TODO: which of malloc_shared/malloc_local is ordinary malloc? i think the ordinary malloc is already malloc_local)

or mb 'mallo' instead of 'malloc:
| memory allocation | mallo mfree


### Clocks syscall extension ###
TODO

- enumerate available clocks and/or request clock with capabilities

- default wallclock clock
- default monotonic clock (may reset upon each Boot invocation) (units unknown?)

- get current time of clock
- get info about clock (e.g. precision/units of clock)
- what about asking for 32-bit vs 64-bit precision? What about getting the date? Do we offer seconds since unix epoch? Nanoseconds?

- do timers and alarms go in here, or elsewhere? alarms seem like a process control thing
- is setting clocks allowed (probably not?)

see https://stackoverflow.com/questions/3523442/difference-between-clock-realtime-and-clock-monotonic https://man7.org/linux/man-pages/man2/clock_gettime.2.html











## The Boot calling convention


Upon function entry, register sp (stack pointer) holds a valid pointer which is naturally aligned with every data type.


TODO If the function requires a global pointer, then upon function entry, register gp holds a valid pointer which is naturally aligned with every data type. Upon function entry, if the function requires a thread pointer, then upon function entry, register tp holds a valid pointer which is naturally aligned with every data type.

All arguments are passed on the stack. Arguments are in right-to-left order (the first argument specified in the function documentation is lowest in memory, at the 'top' of the stack).

If there are or might be a variable number of arguments, then after the last fixed argument (so just above it in memory) there is an additional implicit argument giving the number of variable arguments, followed by (above in memory) the variable arguments.

Return values are placed on the stack, overwriting the arguments. For any particular function, the number of return values must be fixed.

All arguments and return values must be read and written from/to the stack using the stack cache. This means that the caller can arguments write to the stack cache and the callee can read them from the stack cache with no need for either to flush the stack cache.

The caller puts the return address into the link register (&1) before the call.

The callee cleans up (bumps up the stack pointer before returning, so that only the arguments being returned remain on the stack).

The actual layout of the stack in memory is implementation-dependent. However, to aid intuition we describe a default layout: The stack grows downwards (so the 'top' of the stack is 'lowest' in memory). INT8s and INT16s are padded so that INT8, INT16, and INT32s all occupy INT32_SIZE location when on the stack. INT64s occupy two INT32_SIZE 'slots' on the stack. Therefore each item on the stack occupies either a multiple of INT32_SIZE, or PTR_SIZE, and the distance from the stack pointer to any item on the stack can always be specified as a linear combination of INT32_SIZE and PTR_SIZE.

TODO opaque-ify the stack; specification is given in terms of the usage of operations like CALL, RET, LOAD-LOCAL, STORE-LOCAL, FNEW, FALLOC, FSIZE, and maybe even special functions to copy a stack frame to memory and to copy a stack frame from memory (and what about using an activation record directly from the heap? i guess just set SP to the lowest address of that activation record).
  actually one big disadvantage of that is that you can't then use the stack ops on ordinary memory for 'user stacks' (at least, not without making the layout of those opaque too) -- well, i guess making those layouts opaque isn't the end of the world.

TODO add xcall, xret, etc for extern calls/platform calling convention (e.g. to provide 16-byte stack alignment on linux and windows). Note that, due to the opaque-ified stack, some implementations may simply translate Boot ops to the native calling convention all the time, and implement xcall the same as call.

The callee will not cause unflushed writes to be lost from the stack roc or frame roc (although the callee may flush these, or any other roc).


## Vanilla-style Boots/Boot implementations

This section provides names for various useful guarantees on the behavior of Boots/Boot implementations or programs.

Note that implementation with these restrictions may be considered compliant implementations of Boots/Boot.

### Vanilla 32-bit flavor

A Boot(X) implementation is called 'vanilla 32-bit' if it guarantees that:

- integers are defined to be 32-bit, represented using little-endian with twos-complement for signed values
- pointers are defined to be represented as 32-bit integers
- INT32_SIZE is 4
- INT16_SIZE is 2
- PTRD_SiZE is 4
- mixing integer bitwidths (writing to memory with one bitwidth and reading with another) is allowed

### Vanilla 64-bit flavor

A Boot(X) implementation is called 'vanilla 64-bit' if it supports the 64-bit integer extension (see above), and if it also guarantees that:

- integers are defined to be 64-bit, represented using little-endian with twos-complement for signed values
- pointers are defined to be represented as 64-bit integers
- INT64_SIZE is 8
- INT32_SIZE is 4
- INT16_SIZE is 2
- PTRD_SiZE is 8
- mixing integer bitwidths (writing to memory with one bitwidth and reading with another) is allowed


## Boot subsets

This section provides names for various useful subsets of Boot.

The Boot subsets in this section omit various features that are required for an implementation to be compliant with the Boot standard. Therefore an implementation which implements only these cannot be said to be a full Boot implementation, but rather can only be claimed to implement a subset of Boot.

### Static control flow subset

An implementation or program is said to support/use only the 'static control flow' subset of Boot if it does not provide/use:
- lpcf, lpcb, lpc32, jy

### Pure

A Boot implementation or program is called 'pure' if it does not provide/use:
- any I/O devices
- any lib calls which have side effects, or which read input, or are nondeterministic

Note that writing to memory is allowed.

### Tiny subset

An implementation or program is said to support/use only the 'tiny' subset of Boot if it does not provide/use:

- any instruction not found in Boots
- the mul32 instruction
- any instruction that references a 32-bit integer immediately following within the instruction stream:
  - j32, li32, lpc32



## Details about semantics



### Avoiding tearing with natural alignment

#### Boot's anti-tearing guarantees
Boot guarantees:
- a write to a single memory location using a single Boot instruction will never cause tearing
- a write of a single value spread out over multiple consecutive memory locations using a single Boot instruction will never cause tearing if the first memory location being written to is "naturally aligned" (see below) for the object size being written to in that instruction
- the Boot standard library function 'memcpy' will never cause tearing by its writes


#### Boot's natural alignment guarantees
We guarantee natural alignment in the following situations:

1) 
- For any given integer $size, and any given pointer &base_pointer, let $padding be the integer result returned by the 'palign' instruction when passed &base_pointer and $size.
- (&base_ptr + $padding) is guaranteed to be naturally aligned for size $size

2) If &aligned_ptr is naturally aligned for objects of size $size, then the result (&aligned_ptr + any multiple of $size) is also naturally aligned for size $size.

3) PTR_SIZE is guaranteed to be the largest size object that can be written by any single Boot instruction, and therefore also, if &aligned_ptr is naturally aligned for objects of size $size, then the result (&aligned_ptr + any multiple of $PTR_SIZE) is also naturally aligned for size $size.



### Register-offset cache behavior



BEGIN mv'd from boot_reference
### Register-offset cache behavior

Boot implementations may but are not required to implement a register-offset cache. If you are implementing Boot, and you don't want to bother to implement a register-offset cache, just implement lwc/lpc/swc/spc the same way as l32/lp/s32/sp, and implement flushroc2mem and flushmem2roc as no-ops; if you are implementing Boot and you do this, you don't need to read any further in this section 'Register-offset cache behavior'.

The goal of the the register-offset cache is to provide the same performance benefits as calling conventions which pass some arguments in registers, but with a calling convention that does not actually specify how many arguments go into registers. The design allows the implementation to cache some of the stack in registers, without specifying how many registers, and provides the implementation flexibility regarding whether or not the cache is kept in-sync with the memory stack in between explicit flushes. The downside is that the program must use special instruction to explicitly indicate whether loads/stores operate on the cache (lwc/lpc/swc/spc) or on main memory (lw/lp/sw/sp), and to indicate when the flush the cache (flushroc2mem and flushmem2roc).

After a program writes to the register-offset cache ("cache"), it can execute flushroc2mem to make sure that outstanding writes are flushed to main memory, and after it writes to main memory, it can execute flushmem2roc to make sure that outstanding writes are flushed to the cache.

The implementation may (but is not required to) write changes from the cache to memory at any time (even before flushroc2mem is called); and it may (but is not required to) read changes from memory into the cache at any time. After either kind of flush, cache and memory contain the same values, and there are no more 'unflushed writes'.

If the value of the base register is changed, this invalidates the cache, and subsequent reads from the cache may return arbitrary values. Unflushed writes might be lost when the cache is invalidated, but another possibility is that they might stick around until the next flush. 

More generally, after a program writes to the cache, the corresponding location in memory may or may not get the write unless and until the next flushroc2mem, and after a value in memory changes, the corresponding location in the cache may or may not get the write until the next flushmem2roc. If, between flushes, both: a value is written to the cache, and also the corresponding location in memory changes to a different value, then at any time until the next flush, both the cache and memory may take on either of these values, and may update between these at arbitrary times. If the base register (in Boot, the ROC base register is always the stack pointer), changes after a write to cache but before the next flushroc2mem, the write in the cache may or may not be lost. If the base register changes, and then a location in the cache is read before either it is written to or a flushmem2roc, the value read is arbitrary. The only times that memory and cache are guaranteed to contain the same values is a period of time after a flush and before any writes have been done to either one.

The following example strategies show some simple ways for Boot implementations to implement the register-offset cache:
- example strategy #1: don't cache at all. lwc/lwp and swc/swp just load from/store to memory, and flushroc2mem and flushmem2roc are no-ops
- example strategy #2: maintain a 16-item cache. lwc/lpc/swc/spc operate on the cache, and flushroc2mem/flushmem2roc flush the entire cache to/from memory. Before any of those instructions, check if R2 has changed, and if so, execute flushmem2roc (even if the program doesn't execute flushmem2roc, in this implementation strategy it is necessary upon invalidation, so that the garbage in the cache isn't written back into memory later with the next flushroc2mem).

More complex implementations could add features such as:
- dirty lists to minimize unnecessary memory traffic
- early flushing concurrently in the background
- not always resetting the entire cache whenever R2 changes

The following sections describe more example strategies, give a semi-formal description of the behavioral guarantees given above for the register-offset cache, and show example Boot code involving the cache and explain how these guarantees apply to it.

END mv'd from boot_reference

TODO add some stuff about how to efficiently store the stack cache, if the implementation chooses to have different sizes in memory for ints and ptrs. Namely: on a system with 64-bit word size, either pack 32-bit things (2 into each register), or call use each 'cache' register for storing 32-bits of something, and store 64-bit items (INT64, F64, and PTR) over 2 registers (during computation, you can also use the first of those two regs to store the entire 64-bit item in the first register, so that you don't have to burn an additional scratch register to reconstruct the 64-bit item). Now you can blindly copy stuff to/from memory without remembering which size each item on the cache had.
TODO note that the implementation may choose to just store everything padded to max(INT64_SIZE, PTR_SIZE) to make it easy

Boot implementations may but are not required to implement a register-offset cache. If you are implementing Boot, and you don't want to bother to implement register-offset cache, just implement lwc/lpc/swc/spc/pushc/popc/cpc the same way as l32/lp/s32/sp/push/pop/cp, and implement flushroc2mem and flushmem2roc as no-ops; you don't need to read any further in this section 'Register-offset cache behavior'.

(this subsection is copied from the Boot reference, with minor changes; if you have already read that, skip ahead to the subsection 'Differences in register-offset cache between Boots and Boot')

After a program writes to a register-offset cache ("cache"), it can execute flushroc2mem to make sure that outstanding writes are flushed to main memory, and after it writes to main memory, it can execute flushmem2roc to make sure that outstanding writes are flushed to the cache.

After a program writes to a cache, the corresponding location in memory may or may not get the write unless and until the next flushroc2mem, and after a value in memory changes, the corresponding location in the cache may or may not get the write until the next flushmem2roc. If, between flushes, both: a value is written to the cache, and also the corresponding location in memory changes to a different value, then at any time until the next flush, both the cache and memory may take on either of these values, and may update between these at arbitrary times. If the base register changes after a write to cache but before the next flushroc2mem, the write in the cache may or may not be lost; except that the write is never lost if the base register was changed by stack addressing mode, or by pushc, or by popc, or by cpc. The only times that memory and cache are guaranteed to contain the same values is a period of time after a flush and before any writes have been done to either one.

The following example strategies show some simple ways for Boot implementations to implement the register-offset cache:
- example strategy #1: don't cache at all. lwc/lwp and swc/swp just load from/store to memory, and flushroc2mem and flushmem2roc are no-ops
- example strategy #2: maintain a 16-item cache for two base register, sp and fp (stack pointer and frame pointer). lwc/lpc/swc/spc operate on the cache, and flushroc2mem/flushmem2roc flush the entire cache to/from memory. Before any of those instructions, check if the base register has changed, and if so, execute flushmem2roc.

More complex implementations could add features such as:
- dirty lists to minimize unnecessary memory traffic
- early flushing concurrently in the background
- not always resetting the entire cache whenever the base register changes

TODO: after cache invalidation, need to mandate flushmem2roc if cache will be read from; this allows programs that are only going to write to skip this step. Simple implementations need to do this anyways upon cache invalidation to avoid writing garbage into memory upon the next flushroc2mem, but more complex ones might only write back the dirty values in which case they can avoid unasked for loads upon invalidation.

#### Differences in register-offset cache between Boots and Boot
- In Boots, there is only one register-offset cache (associated with the stack register); in Boot, any or every register may have an associated register-offset cache.
  - an implementation may implement register-offset cache for some registers and not others (accessing memory directly when R.O.C. commands are encountered for other registers)
  - if the same memory address would fall into multiple register-offset caches, it may hold a different value in each cache; the caches cannot be directly flushed to one another, and instead each cache must be flushed to/from main memory in turn
- In Boots, the register-offset cache is of size 16; in Boot, the register-offset caches are of implementation-dependent size.
  - register-offset cache operations beyond the bounds of the actually implemented cache may be implemented as direct accesses to memory
- In Boots, any change of the base register may invalidate the R.O.C. associated with that register, potentially losing writes in the cache. In Boot, changes to the stack pointer from the stack addressing mode, or any change to any register using the pushc/popc instructions, do not invalidate the cache or lose writes.
  - changes to registers by any other means may or may not lose writes, as in Boots.

#### Example implementation strategies

Boot implementations may use one of the following strategies for implementing register-offset cache:

When there are changes to the memory stack that have not yet been flushed to the stack cache, we say that the stack cache is 'stale' (otherwise we say it is 'fresh'). When there are changes to the stack cache that have not yet been flushed to memory, we say that the stack cache is 'dirty' (otherwise we say it is 'clean').

The following example strategies show some ways how Boot implementations could implement the register-offset cache:
- example strategy #1 (the simplest strategy): don't cache at all. lwc/lwp and swc/swp just load from/store to memory, and flushroc2mem and flushmem2roc are no-ops
- example strategy #2 (the simplest caching strategy: lwc/lpc/swc/spc operate on the cache): lwc/lpc/swc/spc operate on the cache, and flushroc2mem/flushmem2roc flush the entire cache to/from memory. pushc, popc, and stack addressing mode implicitly cause a flushroc2mem before their primary operations. Before any of those instructions, check if the base register has changed, and if so, execute flushmem2roc.
- example strategy #3 (cache with a dirty list, minimizing the work done by flushes): maintain a record of which locations in the cache have been written to and not yet flushed ("dirty list"). swc/spc operate on the cache, but lwc/lpc only load dirty locations from the cache and otherwise load from memory. flushroc2mem only writes the dirty locations, and flushmem2roc merely clears the dirty list. Before any of those instructions, check if the base register has changed, and if so, clear the dirty list.
- example strategy #4 (cache with a dirty list, lwc/lpc/swc/spc operating on the cache): same as #3 except flushmem2roc reloads the entire cache from memory, and lwc/lpc always loads from the cache

TODO: replace 'R2' 'register 2' 'stack pointer' 'SP' with 'base register'
TODO: deal with non-invalidating changes to base register
TODO: provide an example that deals smartly with base register updates

#1: don't cache at all:
- flushroc2mem and flushmem2roc are no-ops.
- To execute lwc/lwp and swc/swp, just do an ordinary load-store to the corresponding memory location.

#2: lwc/lpc/swc/spc only operate on the cache:
- Make space to store a cache of size 16*PTR_SIZE, as well as a copy of register 2 "R2_OLD".
- When flushmem2roc is executed, copy the contents of memory locations R2..R2+15 into the cache, and set R2_OLD=R2.
- When flushroc2mem is executed, check if R2 is equal to R2_OLD and if so, copy the cache into memory locations R2..R2+15, and otherwise copy the contents of memory locations R2..R2+15 into the cache (same as flushmem2roc); and in any case set R2_OLD=R2.
- lwc/lpc and swc/spc first check if R2_OLD=R2. If they are not equal, flushmem2roc is executed. Now, they load from and store to the cache.

#3: maintain a record of which locations in the cache have been written to and not yet flushed ("dirty"). Loads from locations that haven't been written to bypass the cache:
- Make space to store a cache of size 16*PTR_SIZE, as well as a copy of register 2 "R2_OLD", as well as 16 bits to record which locations in the cache are dirty
- When flushmem2roc is executed, clear the dirty list, and set R2_OLD=R2
- When flushroc2mem is executed, check if R2 is equal to R2_OLD. If they are equal, then copy the dirty items in the cache into the corresponding memory locations. Clear the dirty list, and set R2_OLD=R2
- When lwc/lpc is executed, check if R2 is equal to R2_OLD. If they are not equal, then clear the dirty list, and set R2_OLD=R2. Now, if the location requested is on the dirty list, serve it from the cache, otherwise serve it from memory
- When swc/spc is executed, check if R2 is equal to R2_OLD. If they are not equal, then clear the dirty list, and set R2_OLD=R2. Now, store the given value to the cache, and mark that location as dirty

#4: lwc/lpc/swc/spc only operate on the cache, but also with dirty list. Same as #3 except:
- When flushmem2roc is executed, copy the contents of memory locations R2..R2+15 into the cache, clear the dirty list, and set R2_OLD=R2
- When lwc/lpc is executed, check if R2 is equal to R2_OLD. If they are not equal, then clear the dirty list, and set R2_OLD=R2. Now, load the given value from the cache.
- strategy #4 permits the dirty list to be approximate, in the direction of reporting some items as dirty which are not actually dirty; for example, the dirty list could be just a single integer tracking the highest offset which has been written to



#### register-offset cache behavior guarantees

**ROC-G-1 (ROC Guarantee 1)** (cached reads read cached writes):

If an swc/spc instruction is executed, followed by an lwc/lpc to the same offset and:
- there have been no swc/spcs to that offset in the meantime
- there have been no writes to register 2 in the meantime 
- since the last flushmem2roc or flushroc2mem instruction, there have been no writes to memory location &2 + offset*PTR_SIZE
then:
- the lwc/lpc is guaranteed to read the same value stored by that previous swc/spc

**ROC-G-2** (flushmem2roc):

If a flushmem2roc instruction is executed, followed by an lwc/lpc, and:
- there have been no swc/spcs to that offset in the meantime
- there have been no writes to register 2 in the meantime
- there have been no writes to memory location &2 + offset*PTR_SIZE in the meantime
then:
- the value held by written by memory location &2 + offset*PTR_SIZE at the time of the flushmem2roc will be read by the lwc/lpc

**ROC-G-3** (flushroc2mem):
If an swc/spc instruction is executed, followed by a flushroc2mem, and:
- there have been no swc/spcs to that offset in the meantime
- there have been no writes to register 2 in the meantime
- there have been no writes to memory location &2 + offset*PTR_SIZE in the meantime
then:
- the value written by the swc/spc is guaranteed to be written to the memory location &2 + offset*PTR_SIZE

**ROC-G-4** (catchall):
If ROC Guarantees 1 thru 3 (in combination with the other non-register-offset-cache related semantics of the rest of Boot) do not determine the value read by an lwc/lpc or written to a memory location, then that value is guaranteed to be either the value immediately after the last applicable flush, or one of the values written to that offset by a swc/spc to that offset since the last applicable flush, or one of the values stored at the corresponding memory location since the last applicable flush.

**ROC-G-5** (flush synchronization and finality):
After either a flushmem2roc or a flushroc2mem, and before any writes to cache or to corresponding memory locations, the values of cache and corresponding memory locations match, and do not change.


#### Examples of register-offset cache behavior
Examples:

    swc $0 &2 9     ; store 0 to memory location        (&2 + 9*PTR_SIZE), cached
    lwc $7 &2 9     ; R7 = the value at memory location (&2 + 9*PTR_SIZE), cached
    ; R7 is guaranteed to be 0 by ROC-G-1
    
In this case, a cached write is followed by a cached read, with no intervening writes to cache or to register 2 or to the corresponding memory location, so the destination register R7 is now guaranteed to hold the written value.

    swc $0 &2 9    ; store 0 to memory location        (&2 + 9*PTR_SIZE), cached
    flushroc2mem   ; flush cache
    li $5 9        ; R5 = 9
    sinfo $6 1     ; R6 = PTR_SIZE
    mul32 $7 $5 $6 ; R7 = 9*PTR_SIZE
    ap &5 &2 $7    ; R5 = (&2 + 9*PTR_SIZE)
    l32 $6 &5 0    ; R6 = the value at memory location (&2 + 9*PTR_SIZE)
    ; R6 is guaranteed to be 0 by ROC-G-3

In this case, a cached write is followed by a flush, with no intervening writes to register 2 or to the corresponding memory location, so the memory location is now guaranteed to hold the written value.

    swc $0 &2 0    ; store 0 to memory location        &2, cached
    flushroc2mem   ; flush cache
    l32 $6 &2 0    ; R6 = the value at memory location &2
    ; R6 is guaranteed to be 0 by ROC-G-3

Same as the previous, except with offset 0.

    li  $5 3       ; R5 = 3
    s32 $5 &2 0    ; store 3 to memory location        &2
    ; the memory location is guaranteed to be 3 at this point
    li  $5 4       ; R5 = 4
    swc $5 &2 0    ; store 4 to memory location        &2, cached
                   ; no cache flush
    l32 $6 &2 0    ; R6 = the value at memory location &2
    ; R6 is guaranteed to either 3 or 4 by ROC-G-4

In this case, a cached write is followed by a read from memory, without an intervening flush. The value in memory may or may not reflect the cached write.


    li  $5 3       ; R5 = 3
    s32 $5 &2 0    ; store 3 to memory location        &2
    sinfo $7 1     ; R7 = PTR_SIZE
    ap &7 &2 $7    ; R7 = (&2 + PTR_SIZE)
    s32 $5 &7 0    ; store 3 to memory location        (&2 + PTR_SIZE)
    ; the memory location &2            is guaranteed to be 3 at this point
    ; the memory location (&2+PTR_SIZE) is guaranteed to be 3 at this point
    li  $5 4       ; R5 = 4
    swc $5 &2 0    ; store 4 to memory location        &2             , cached
    swc $5 &7 0    ; store 4 to memory location        (&2 + PTR_SIZE), cached
                   ; no cache flush
    l32 $8 &2 0    ; R8 = the value at memory location &2
    l32 $9 &7 0    ; R9 = the value at memory location (&2 + PTR_SIZE)
    ; R8 is guaranteed to either 3 or 4 by ROC-G-4
    ; R9 is guaranteed to either 3 or 4 by ROC-G-4
    ; note that any of the following combinations are possible:
    ;  R8 = 3, R9 = 3
    ;  R8 = 3, R9 = 4
    ;  R8 = 4, R9 = 3
    ;  R8 = 4, R9 = 4

This example is the similar to the previous one, except this time we do the same thing to two separate locations, again without any flushing. Note that it's possible for one of the cache writes to affect its corresponding memory location even if the other cache write does not.

    li $5 3        ; R5 = 3
    swc $5 &2 0    ; store 3 to memory location        &2, cached
    li $5 4        ; R5 = 4
    swc $5 &2 0    ; store 4 to memory location        &2, cached
    flushroc2mem   ; flush cache
    l32 $6 &2 0    ; R6 = the value at memory location &2
    ; R6 is guaranteed to be 4 by ROC-G-3

In this case, the 2nd cached write of value 4 is followed by a flush, with no intervening writes to register 2 or to the corresponding memory location, so the memory location is now guaranteed to hold 4. The earlier cached write of value 3 was overwritten by an intervening cached write at the same offset, so ROC-G-3 does not apply to that write and this read.


    s32 $0 &2 0    ; store 0 to memory location        &2
    flushmem2roc   ; flush cache
    lwc $6 &2 0    ; R6 = the value at memory location &2, cached
    ; R6 is guaranteed to be 0 by ROC-G-2

In this case, a write to memory is followed by a flush followed by a cached read. Because of the flush, the cached read is guaranteed to read the value held by memory at the time of the flush.

    li  $5 3       ; R5 = 3
    s32 $5 &2 0    ; memory location &2 = 3
    li  $5 4       ; R5 = 4
    swc $5 &2 0    ; store 4 to memory location        &2, cached
    ; NOTE: no cache flush
    l32 $6 &2 0    ; R6 = the value at memory location &2
    lwc $7 &2 0    ; R7 = the value at memory location &2, cached
    ; R6 could be either 3 or 4 by ROC-G-4
    ; R7 must be 4 by ROC-G-1

In this case, the memory location held 3, then the value 4 was written to the cache, then the memory location was read, then the cache of the same memory location was read. For the memory location uncached read, there was no cache flush to ensure that the store-cache was written to memory, so none of the specific guarantees hold, so the catch-all guarantee kicks in and tells us that the memory location might be either the previous value in memory or the value written to the cache. For the cached read, there was a store-cache followed by a load-cache with no intervening writes, so the cached read is guaranteed to read the most recent cached write.

    li  $5 4       ; R5 = 4
    swc $5 &2 0    ; store 4 to memory location        &2, cached
    li  $5 3       ; R5 = 3
    s32 $5 &2 0    ; memory location &2 = 3
    ; NOTE: no cache flush
    l32 $6 &2 0    ; R6 = the value at memory location &2
    lwc $7 &2 0    ; R7 = the value at memory location &2, cached
    ; R6 is guaranteed to be 3 by the ordinary semantics of memory
    ; R7 could be either 3 or 4 by ROC-G-4

In this case, there was a cached write of 4, followed by an uncached write of 3. The memory location is definitely overwritten by the uncached write of 3, but the cached value may or may not have been, because there was no flush.

    li  $5 4       ; R5 = 4
    s32 $5 &2 0    ; store 4 to memory location        &2
    li  $5 3       ; R5 = 3
    swc $5 &2 0    ; store 3 to memory location        &2, cached
    lwc $6 &2 0    ; R6 = the value at memory location &2, cached
    ; R6 is guaranteed to be either 4 or 3 by ROC-G-4
    ; ROC-G-1 does not apply here because there has been a write to memory since the last flush
    lwc $7 &2 0    ; R7 = the value at memory location &2, cached
    sub32 $8 $7 $6 ; R8 = R7 - R6
    ; $8 is not guaranteed to be 0; R6 could be 3 and R7 could be 4

In this case, there are conflicting writes to memory and cache without a flush. There is no guarantee that value will not change even after the writes are finished (for example, perhaps the write of 4 to memory eventually was pushed to the cache by a background thread, after a delay).

    li  $5 4       ; R5 = 4
    s32 $5 &2 0    ; store 4 to memory location        &2
    li  $5 3       ; R5 = 3
    swc $5 &2 0    ; store 3 to memory location        &2, cached
    ; R6 is guaranteed to be either 4 or 3 by ROC-G-4
    flushroc2mem
    lwc $6 &2 0    ; R6 = the value at memory location &2, cached
    lwc $7 &2 0    ; R7 = the value at memory location &2, cached
    sub32 $8 $7 $6 ; R8 = R7 - R6
    ; $8 is guaranteed to be 0 by ROC-G-5, which states that the
    ; values don't change in between a flush and the next write

In this case, there is a guarantee that values stop changing after a flush.

    li  $5 4       ; R5 = 4
    s32 $5 &2 0    ; store 4 to memory location        &2
    flushmem2roc
    li  $5 1
    ap  &9 &2 $5   ; &9 = &2 + 1
    li  $5 4       ; R5 = 5
    s32 $5 &9 0    ; store 5 to memory location        &2+1
    swc $0 &2 0    ; store 0 to memory location        &2, cached
    li  $5 1
    ap  &2 &2 $5   ; &2 = &2 + 1
    flushroc2mem   ; flush cache
    l32 $5 &2 0    ; R5 = the value at memory location &2
    ; ROC-G-3 does not apply because there was a write to register 2 in the meantime
    ; ROC-G-4 guarantees that $5 is either 4 or 0
    l32 $6 &9 0    ; R6 = the value at memory location &2+1
    ; R6 is still guaranteed to be 5; the fact that register 2 was changed
    ; in between the write of 0 to offset 0 and the flushroc2mem only affects offset 0
    ; (both offset 0 in cache, and the corresponding memory location)
    
In this case, we see that when register 2 is written to before a flush, only those cache locations, and corresponding memory locations, that were actually written to take on unknown values; this doesn't mess up other memory locations, located at other offsets in the cache.

TODO: give an example showing that cache invalidation doesn't guarantee that previous writes won't show up in memory later -- gotta flush for that. That is, write to the cache; change the base pointer; read from memory into register A; value might be the initial value, or it might be the value written; read from memory a second time into register B; register A and register B might not be equal

TODO: restate the above examples into simple language like in the previous TODO, so that ppl don't have to read the assembly code (but provide the assembly code also); eg "write to the cache; change the base pointer; read from memory into register A; value might be the initial value, or it might be the value written; read from memory a second time into register B; register A and register B might not be equal"

### Guidelines for register-offset cache sizes, performance, and usage

In practice, register-offset cache is only expected to be widely implemented or used as up to 16 locations associated with each of base registers sp and fp. Pushc and popc are only expected to be widely used with sp, and cpc is only expected to be widely used in one of the forms 'cpc sp fp' or 'cpc fp sp'. These are the uses that are specified/implied by the Boot calling convention; implementing caches associated with sp and fp would allow arguments to be passed between Boot functions without accessing main memory. Most Boot implementations probably won't implement register-associated cache beyond these.

For example, a Boot implementation on a physical 16-register machine might use 8 registers to cache the first 3 offsets above each of sp and fp in registers; such an implementation would allow Boot function calls and returns that pass 3 arguments or less to avoid accessing main memory.

Boot implementations with a hardcoded mapping of Boot registers->platform registers on platforms with at least 16 physical registers are encouraged but not required to consider caching the 3 words at and above each of the registers sp and fp. Boot programs are encouraged to consider that access to offsets 0 thru 2 from sp/fp via register-offset cache instructions may be faster than access to registers above ~r8.

Boot implementations with a hardcoded mapping of Boot registers->platform registers on platforms with at least 32 physical registers are encouraged but not required to consider caching the 7 words at and above each of the registers sp and fp. Boot programs are encouraged to consider that access to offsets 0 thru 6 to from sp/fp via register-offset cache instructions may be faster than access to registers above ~r16.

That is, register/cache access speed is likely to follow something like the following:

expected fastest on platforms with 8 general-purpose registers and 8 floating-point registers:
- r0 .. r6, f0 (=0, ra, sp, gp, tp, fp, t0)
  - 0, ra, sp, gp, tp, fp
  - t0
- ft0..ft6

expected fastest on platforms with 16 general-purpose registers and 16 floating-point registers:
- r0 .. r8, f0 (=0, ra, sp, gp, tp, t0, t1, fp, e0)
  - 0, ra, sp, gp, tp, fp
  - t0, t1
  - e0
- s0 .. s2
- a0 .. a2
- ft0..ft7
- fe0..fe7

expected fastest on platforms with 32 general-purpose registers and 32 floating-point registers:
- r0 .. r16, f0
  - 0, ra, sp, gp, tp, fp
  - t0..t4
  - e0..e5
- s0..s6
- a0..a6
- ft0..ft15
- fe0..fe14

expected slower on most platforms:
- t5..t12
- e6..e12
- s7..r15
- a7..a15

The above assumes that implementations only need to reserve one platform register for implementation use (in addition to whatever 'dual use' they can get out of 0, ra, sp, gp, tp, fp). Note that some implementations may need to reserve additional platform registers for implementation use, in which case some of the items may have slower access speed than indicated above.

TODO: actually should assume there is at least one more register used (to keep track of stale and dirty r.o. cache locations)




## Boot assembly

TODO

Boot assembly
+
addr modes and type sigils in operand arguments
+
literals
+
labels
+
macros

mb:
optional macroish pragmas for fast/simple variable-name-to register assignment (just assign next free register number, or error if no free ones; requires pragmas for linear delineation of scopes, too)

### Operand arguments
TODO

### Literals

All numeric literals are interpreted as hexadecimal (so "10" means decimal 16, not decimal 10), and letters within numerical literals must be lowercase.

### Labels

Label definitions are of the form ':LABELNAME'. The : must be the first character of a line.

References to labels are of the form '@-LABELNAME' (for backwards references) or '@LABELNAME' (for forwards references). If the same label is defined multiple times, the reference resolves to the closest one in the indicated direction ('closest' in terms of distance in the static instruction stream).

Label names must start with one of the characters a-z or _, and may contain any of the characters a-z or _ or 0-9. Label names are only seen after one of :, @-, or @.


TODO nah just make refs to labels the same, but not at the first character of a line; :LABELNAME

TODO also have a syntax to define global labels, which are what immediate addressing mode means on operands of type label. If you define more than 128 of them, can use 'lkel' to access the rest. Is the 'label constant table', and 'jk' (whose target can be dynamic, not just static) the same as these, or do these exist as compile-time 'macros' only?

## Tips for implementors and programmers

### What is tearing?

Boot does not define very much in the way of concurrent semantics for Boot programs, however in some cases the system that a program is run on may have other processes concurrently accessing the same memory that the Boot program is accessing. When a Boot program reads or writes a value from memory which spans multiple memory locations, if another process is concurrently accessing the same set of locations and at least one of these processes is writing, on some systems is it possible to experience 'tearing', in which an arbitrary value is observed in the memory location (typically as a result of one process reading a partial write when the write has written to some of the memory locations but not all of them).

When using naturally aligned pointers, Boot guarantees that Boot instructions will not cause tearing when writing.

Note that if the other process (rather than your Boot program) is doing the writes, and it does not take precautions to avoid tearing, tearing might still be observed by the Boot process.

### What is natural alignment?
"Natural alignment" on systems in which pointers are just integers is usually defined as when the memory location is divisible by the size in bytes of the object being read or written. For example, on a system in which pointers are 8 bytes long, a pointer stored beginning at memory location 16 is naturally aligned, but a pointer stored beginning at memory location 17 is not naturally aligned. However, in Boot, pointers are opaque and are not integers, so we need to provide natural alignment guarantees more abstractly.

Note that natural alignment is a relation between a pointer and a size (and size is an integer). So, for any pair of (pointer, integer), we can ask, is that pointer naturally aligned for this size? And the answer is boolean; either the pointer is naturally aligned for that size, or it isn't. So, natural alignment can be thought of as a function from (pointers, integers) -> {true, false}.

Use the 'palign' instruction to align things. For example, if you have an int32 that you want to write to memory and you want it to be naturally aligned, and you have a memory location &ptr but you don't know if it is aligned, then palign can be used to find how much padding should be inserted starting at location &ptr before writing your int32, by passing in &ptr and INT32_SIZE, and then using 'ap' to incrementing &ptr by the result that 'palign' returns. Note that there is no guarantee that the result of adding the padding value returned by palign will not take the pointer outside of accessible/allocated/valid memory; the Boot program is responsible for ensuring this.


## Table of opcodes

TODO changed the opcodes in Boot; update here to match

The following CSV-formatted table contains tuples of the form:

    (opcode (as found in the opcode field of the instruction),
    reference_opcode (a number uniquely identifying the instruction)
    mnemonic,
    1 if the instruction has an embedded 32-bit immediate word following it and 0 otherwise,
    type of op0,
    type of op1,
    type of op2,
    1 if the instruction might write to the register specified by op0,
    1 if the instruction might read from the register specified by op0,
    1 if the instruction might write to the register specified by op1,
    1 if the instruction might read from the register specified by op1,
    1 if the instruction might write to the register specified by op2,
    1 if the instruction might read from the register specified by op2,
    1 if the instruction might write to the PC,
    1 if the instruction might read the PC,
    1 if the instruction might write to memory
    1 if the instruction might read from memory,
    )

Type identifiers in the following table:
- u7, u14, u21: unsigned immediate of the specified bitwidth
- ri, rp, rl, rh: register specifier and type of int32, ptr, label, handle, respectively
- 0: must be 0
- _: this operand does not exist / is concatenated with the previous one
- ?: this operand type is unspecified

Note that the opcode field is written in decimal notation (not hexadecimal).


    opcode, mnemonic, has_i32_data, op0_type, op1_type, op2_type, op0_w, op0_r, op1_w, op1_r, op2_w, op2_r, PC_w, PC_r, mem_w, mem_r
    0,ann,0,?,?,?,0,0,0,0,0,0,0,0,0,0
    1,j32,1,0,0,0,0,0,0,0,0,0,1,1,0,0
    2,li32,1,ri,0,0,0,0,0,0,1,0,0,0,0,0
    3,ll32,1,rl,0,0,0,0,0,0,1,0,0,1,0,0
    8,j21b,0,u21,_,_,0,1,0,0,0,0,1,1,0,0
    9,j21f,0,u21,_,_,0,1,0,0,0,0,1,1,0,0
    10,sys,0,u21,_,_,0,0,0,0,0,0,1,1,1,1
    16,llb,0,rl,u14,_,1,0,0,0,0,0,0,1,0,0
    17,llf,0,rl,u14,_,1,0,0,0,0,0,0,1,0,0
    18,li,0,ri,u14,_,1,0,0,0,0,0,0,0,0,0
    19,lin,0,ri,u14,_,1,0,0,0,0,0,0,0,0,0
    20,sinfo,0,ri,u14,_,1,0,0,0,0,0,0,0,0,0

    # was: backwards conditional branches
    
    32,beqf,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    33,bnef,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    34,bltf,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0
    35,bltuf,0,ri,ri,u7,0,1,0,1,0,0,1,1,0,0

    # was: le forwards conditional branches
    
    36,beqpf,0,rp,rp,u7,0,1,0,1,0,0,1,1,0,0
    # every other one was: bne conditional branches
    37,beqlf,0,rl,rl,u7,0,1,0,1,0,0,1,1,0,0
    38,beqhf,0,rh,rh,u7,0,1,0,1,0,0,1,1,0,0

    # was: roc

    # was: apm's
    
    128,s32,0,ri,rp,0,0,1,0,1,0,0,0,0,1,0
    129,s16,0,ri,rp,0,0,1,0,1,0,0,0,0,1,0
    130,s8,0,ri,rp,0,0,1,0,1,0,0,0,0,1,0
    131,sp,0,rp,rp,0,0,1,0,1,0,0,0,0,1,0
    132,sl,0,rl,rp,0,0,1,0,1,0,0,0,0,1,0
    133,sh,0,rh,rp,0,0,1,0,1,0,0,0,0,1,0
    134,shl,0,ri,ri,0,1,0,0,1,0,0,0,0,0,0
    135,shru,0,ri,ri,0,1,0,0,1,0,0,0,0,0,0
    136,shrs,0,ri,ri,0,1,0,0,1,0,0,0,0,0,0
    137,add32,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    138,sub32,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    139,mul32,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    140,and,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    141,or,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    142,xor,0,ri,ri,ri,1,0,0,1,0,1,0,0,0,0
    143,addp,0,rp,rp,ri,1,0,0,1,0,1,0,0,0,0
    143,subp,0,rp,rp,ri,1,0,0,1,0,1,0,0,0,0

    # was: aps
    
    # 147 empty (was palign)
    148,l32,0,ri,rp,0,1,0,0,1,0,0,0,0,0,1
    149,l16,0,ri,rp,0,1,0,0,1,0,0,0,0,0,1
    150,l16u,0,ri,rp,0,1,0,0,1,0,0,0,0,0,1
    151,l8,0,ri,rp,0,1,0,0,1,0,0,0,0,0,1
    152,l8u,0,ri,rp,0,1,0,0,1,0,0,0,0,0,1
    153,lp,0,rp,rp,0,1,0,0,1,0,0,0,0,0,1
    154,ll,0,rl,rp,0,1,0,0,1,0,0,0,0,0,1
    155,lh,0,rh,rp,0,1,0,0,1,0,0,0,0,0,1
    156,cp,0,ri,ri,0,1,0,0,1,0,1,0,0,0,0
    157,cpp,0,rp,rp,0,1,0,0,1,0,1,0,0,0,0
    158,cpl,0,rl,rl,0,1,0,0,1,0,1,0,0,0,0
    159,cph,0,rh,rh,0,1,0,0,1,0,1,0,0,0,0
    160,jy,0,rl,0,0,0,0,0,0,0,1,1,0,0,0
    161,in1,0,ri,ri,0,1,0,0,1,0,0,0,0,0,0
    162,out1,0,ri,ri,0,0,1,0,1,0,0,0,0,0,0
    163,break,0,?,?,?,0,0,0,0,0,0,0,0,0,0
    


The above opcodes are numbered in such a way so that the instructions which take various kinds of immediates are clustered together, for easier decoding:

    Opcode range      immediate
    1  thru 7          32-bit
    8  thru 15         21-bit
    16 thru 31         14-bit
    32 thru 127         7-bit
    128+               no immediate
    
Later extensions may mess this up, however.



--------
--------
--------

TODO

- boots is no longer a subset of boot; it has a different encoding now. The INSTRUCTIONS are a subset, though, with the same opcodes and semantics.


- if we pop the cache, now there's junk at the bottom of it; if that location later gets read, is the implementation responsible for noticing there's junk there? Similarly, if we push more than the depth of the stack, is the implementation responsible for noticing that and spilling to memory? The answer is Yes; b/c the program can't know the cache size; need to specify this clearly.

ideas:
- a way to write Boot code into memory and then jump into it?

- make note that Boot defines interop, library and system calling, calling convention, file ops, and that ppl should use parts of that rather than reinventing the wheel
- define a default 'accumulator/result' register



- it looks like, with the possible exception of the 'store' instructions, every Boot instruction in which op0 is separate from op1 and op2 could be constrained to be a register without loss of functionality; it's either a dest register, or it's one of two branch sources. This suggests that we remove at least one bit from that register (because LOVM won't need immediates there) and use it somewhere else (possibly even in Boot). Alternately, we could let LOVM define some more addressing modes with that bit. Alternately, we could let LOVM define semantics to assignments to immediates.

- addr mode stuff:
- different immediate addr mode interpretation for destination operations, since immediate addr mode is not useful there (except for R0)
- different immediate addr mode interpretation for address-typed operands, since immediate addr mode is not useful there (except for R0)
  - and what if it is an address-typed destination operand?
- some ideas for addr modes for these (we have 6 bits):
  - offset addr mode (split into 2 3-bits, one specifying a register 1-8, and the other specifying an immediate offset 1-8; add the address in the first register with the integer immediate offset in the second register)
  - index addr mode (split into 2 3-bits, each specifying a register 1-8, and add the address in the first register with the integer index in the second register)
  - pre-decrement and post-increment mode (one bit to specify the mode, now you have 5 bits left to choose any register)
  - PC-relative address; 1 bit is the mode (the index register is to be added to the PC, or subtracted from the PC); the other 5 bits are the register than an integer index is in
  - DP (data segment pointer)-relative address; 1 bit is the mode (the index register is to be added to the DP, or subtracted from the DP); the other 5 bits are the register than an integer index is in
  - scaled combinations of the above (take away some bits to multiple the index by, or to multiply the increment by; ideally we'd have two scale bits, for x1,2,4,8)
- bigger displacments (https://passlab.github.io/CSCE513/notes/lecture03_ISA_Principles.pdf "iron-code summary" suggests at least 12 bits
- ordinary loads of smaller bitwidths into integer registers should be zero extended by default (not sign extended (https://gist.github.com/erincandescent/8a10eeeea1918ee4f9d9982f7618ef68))
- should define a 'vanilla' encoding of FP values, and of stack frames etc (so that vanilla implementations are interoperable)
- https://passlab.github.io/CSCE513/notes/lecture03_ISA_Principles.pdf suggests adding register-memory addr modes (but with only one memory operand allowed per instruction)

- consider this comment: "e.g. RV32I 32-bit ADD and RV64I 64-bit ADD share encodings, and RVI64 adds a different ADD.W encoding. This is needless complication for a CPU which implements both instructions - it would have been preferable to add a new 64-bit encoding instead" [https://gist.github.com/erincandescent/8a10eeeea1918ee4f9d9982f7618ef68]

- consider having a separate stack of return addresses, for security. The return address stack could be allocated at the bottom of the stack space, and grow upwards. Not sure if that meshes with linux/windows tho, b/c i heard they watch the stack ptr and expand the allocated space on the fly? not sure if that's true

when doing the 8- and 16- bit encodings, read:
- chapter 5, pdf page 61 of https://people.eecs.berkeley.edu/~krste/papers/EECS-2016-1.pdf
- https://www2.eecs.berkeley.edu/Pubs/TechRpts/2016/EECS-2016-130.pdf

- should we have restricted profiles without various syscalls, without idiv, without f32, without f64, without either f32/f64?

- consider making Boot instruction set more minimalistic (more like the lowest common denominator of WASM/old Cranelift, LLVM, QBE, MIR, etc), (plus what we need for context saves? or not), and moving eg csel, a lot of bitwise stuff, structure programming helpers to LOVM
- mb also move some syscalls into LOVM (eg TUI)

- any semantics or other wording or ideas to be taken from boot_reference.adoc, or boot_extended_refernce.adoc?

- add at least the following syscalls (text from my old design doc):
 "The reason for (malloc, mdealloc, mfree, open, close, read, write), is that they are fundamental and common. (walk) because it is fundamental. (poll) because it is called a lot in tight loops and so you want them to be efficient, and (time rand) because both they are called in tight loops, and they are fundamental."
- see also those other lists of popular linux syscalls that i made, and my ideas regarding instructions for epoll, completion ports, etc
- figure out fork/forkdone (notes from an old proposal: i'm not clear on if 'fork' is easily supported by all platforms; also 'forkdone' seems like something we don't need a separate opcode for, something that we could do via channels; also there's just a general fuzziness in my head around the exact way these two would work)
- (notes from an old proposal: do we really need/want walk? if it's just 'subopen', can't we just combine it with 'open'? Should we combine 'walk' into 'open' and then have 'list/query' instead of 'walk'? In HTTP and WebDav it seems like 'list' is a fundamental)
- (notes from an old proposal: We should have a list of special pre-opened Open File Descriptors, like stdin, stdout, stderr (in which case mb get rid of LOG? actually i think LOG is better than stderr, timestamps and loglevels are good), rand, and possibly one or more clocks)


- should we have optional, implementation-dependent pointer arithmetic e.g. cast-uint-ptr, cast-ptr-uint (and other casts, i guess)?

- define BootS subset that compiles to Boots

- might want to have one of the sinfos be a pointer to a constant table of pointers. Recall that this dovetails with our plans for the first 8 Boot pointer immediates to be constant pointers (i'm thinking it would hold things like gp, tp, and a pointer to a constant table of pointers; it wouldn't hold 'ordinary' constant table items itself). That way there's a way for the implementation to inject things without having to put something of an unknown size in the instruction stream. If labels remain PTR_SIZE, then the implementation could cram a jump table too, and the program could use jy on this jump table to get around the 24-bit (or 21-bit) jump range limitation if we got rid of the 32-bit immediate datas.

- sinfo for max(PTR_SIZE, INT32_SIZE)
- find and replace stuff elsewhere in this document that talks about the stack caches in the old s0..s15, a0..s15 notation

- note: the availability of displacement addressing suggests that we should have gp, tp in registers after all

- with the offset problem, also consider:
  - Use a scale and only allow homogeneous arrays to use offsets
  - just don't use offsets (RESERVE them for lovm/lava)

- probably need PC-relative loads and stores eg https://smist08.wordpress.com/2021/01/08/apple-m1-assembly-language-hello-world/

- "The C global errno exists for historical compatibility and because C has no easy way to return multiple values; the natural modern API is Go's approach of returning the result and the errno, which is intrinsically thread safe and has no pseudo-global variables"

- in the addr mode section, for clarity, put a table at the top with the ranges on the left and the addr mode name only on the right. The longer descriptions can go below.

- consider again Bocaniciu's favorite addr modes; do we have these already? if not, should we add them?: "There are only 2 possible choices for a set of addressing modes that would allow writing a loop with a minimum number of instructions":
- "3 components, a base register, a scaled index register and an offset included in the instruction"
- "only 2 components, a base register and an offset either in a register or in the instruction, but to allow updating the base register with the computed address"

- consider adding a 'stop bit' to allow encoding (some of the staticly known) ILP into every 32-bit instruction. Perhaps the cleanest way to do this would be to reduce the opcode from 8 bits to 7 -- this would mean settling for 128 instructions


- instead of 32 GPRish regs, how about:
  - 8 special regs (6502-like)
  - 8 mutable GPRs
  - 8 cached values from the stack
    - note: you could just use these as more GPRs if you like, by never pushing and popping; so we really have 24 regs here (16 GPRs plus 8 specials)
  - 8 immutable SSA values (oldest is overwritten, and all are renumbered, whenever you write a new SSA value to the store)
    - need an instruction to throw out some instructions on the front and move the stuff from the back, back to the front.. possibly while permuting too
      - is that much better than just allowing mutation tho? after all, a mutation could be phrased as permuting so that the old value is at the back about to be dropped, then pushing the new value to the front, then permuting again. Mb just allow the user to 'cp' if they want to do that sorta stuff?
    - should we invalidate all of the SSA values at any potential entry point (COMEFROM), and/or at every branch, eg invalidate in between basic blocks? in SSA form, Phi nodes "are required when a variable can be assigned a different value based on the path of control flow" [http://www.llvmpy.org/llvmpy-doc/dev/doc/llvm_concepts.html]. If we don't invalidate, then a static compiler can't easily map each use of an immutable SSA value back to the place it was assigned, which i'm assuming is kinda the point of SSA. In other words, we want the following property; "at any line in the program, each accessible SSA value could have been set by at most 1 line" or in other words "there is a function f(current_line_number, ssa_value_spot) -> unique_line_that_set_that_ssa_value". To put another way, in SSA form we want to assign unique numbers to each SSA value, and that could be difficult if the same 'spot' is used differently across branches/loops. Instead of invalidating ALL of the SSA values, you could instead just say that access to any SSA values that could have been set by more than one line returns an arbitrary value -- this allows things like usage of older SSA values that are the same across both sides of a branch
      - this sort of argues FOR some sort of permutation operation, because that's just changing the mapping between the local names for the SSA values, and the actual unique numbering
    - would it be better for the oldest SSA values to just 'drop off', as described above, or should we just have an immutable stack that is pushed and popped? benefit of immutable stack is that in some cases it makes it clear when a value is not needed anymore. cost is that, either you can only read the top value, which i don't think we should do, or you end up reading values in the middle and then having no way to pop them without popping others which means it often won't be noted when a value is not needed anymore anyways. i think the current way (oldest values drop off) is best.
    - in addition to permute, and to the instruction which drops the NEWEST few values instead of the oldest, could also have an 'undef' that drops a value in the middle. That's just a special case of permute though (permute the undef'd value to the end)
    - a third alternative would be a real 'SSA cache', where whenever you access one of the values, it promotes it back to the first position, so that it won't fall off so soon. However that makes the numbering of the values dependent upon reads instead of just upon writes, which i think is worse.
    - benefit here is that if we use these for things that dont need to be mutated, the CPU understands that and so can do more concurrency black magic (also probably the circuitry for these 8 demands less power than circuitry for 8 GPRs; and there's probably some superlinear stuff in there for total # of GPRs) -- also conventional high-perf CPUs have a bunch of architecturally hidden renaming registers so that the CPU can dynamically detect when one register is being used to store temporary values and being overwritten over and over again, and place each of those instances in a separate 'rename register'; but offering both a few GPRs and a few SSA regs may allow the CPU to not bother with register renaming at all, trusting that the compiler will just use the SSA regs when it intends to use something as a short-lived temporary
- NOTE: this would imply reducing the number of registers in BootS too!!! Mb to 8 or 16! actually this would put the nail in the coffin of the idea of being able to translate from Boot to BootS without either (a) having more GPRs in BootS, or (b) storing some of Boot regs in memory in BootS
  - i guess the most practical soln is to give up on the idea of having every BootS program work on Boot, and just keep having 32 GPRs in Boot. Now BootS programs which use regs above 24 fail to work on Boot.
- could still do integer and pointer banks, too... what was the problem with that? oh yeah, having to blindly copy memory..
   - mb 7 instead of 8 for some of these so that the implementation has 2 more regs to spare to keep indexes into 2 ring buffers for the SSA cache regs and the stack regs
 - note however that it's much easier to do real SSA numbering for cache-like SSA, b/c (unless you support permute), you have an index for the ring buffer and whenever you push onto the cache, you increment that index (to give the real SSA number), and then take the mod of that index to get the position within the ring buffer. With the stack, you have to actually keep track of the SSA number associated with each stack position
 - or mb just make top 2 stack values directly readable, and have more GPRs instead (on some platforms this may allow you to more easily implement the rest of the stack efficiently, and also gives you more GPRs)? Or mb make them mutable (so you can use them all as GPRs)?
   - not sure how much 'have more GPRs' helps if you want to keep an implementation able to fit everything into 32 regs anyhow, tho. So i guess the extra 'space' is used for yet more addr modes or special pseudoregisters.
   - also do we want to leave room within those 32 regs to store a cache of the call stack, too?

(later 211017) actually, so as to leave room for caching the in-memory stack; and replacing the immutable SSA values with a smallstack (fixed-size stack); and having a few more GPRs; and leaving room for the implementation; how about:

  - ~4 special regs (6502-like? or just RISC-V/forwardcom like eg stuff like in-memory stack pointer, in-memory frame pointer, mb thread data pointer, mb base memory/data section pointer)
  - 4 callee-save regs
  - 4 caller-save regs (temporaries)
  - 4 stack regs, of which only the topmost can be written to, but all can be read. Pushing more than 4 values is NOT an error, but leads to the deepest values being lost; popping causes the deepest stack register to take on an arbitrary value (reading these arbitrary values is NOT an error; this allows OSs and debuggers etc to blindly save/restore/dump the stack regs). All temporaries.
  - 4 more stack regs, all callee-saves (so, two separate smallstacks)
  - for function calls, arguments are passed on the in-memory stack, not the smallstack
  - the remaining registers spaces up to 32 can be used for special regs that don't have to be saved or directly accessed, eg R0=0 reg, R31=PC reg, and mb for special functionality, eg. push/pop on various stacks


- instead of 1,2,3,4 format bits for 8- 16- 32- 64- bit instructions, just have 2 format bits for 8-, 16-, and 32-, and 3 format bits for 64 (to allow further extensibility). I guess we're not defining 64- bit instructions right now anyway so that doesnt matter yet. So what i am proposing is: add one more format bit to the 8-bit format so that we can get rid of one format bit in the 32-bit format; this allows us to add a 'stop' bit to each instruction while retaining 256 opcodes, at the cost of having only 64 one-byte opcodes instead of 128.

- do we have enuf opcodes? right now we have 256 opcodes (8 opcode bits; no opcode addressing mode bits). How many does WASM have? https://en.wikipedia.org/wiki/WebAssembly says "...the number of opcodes a bit over 200. The WebAssembly SIMD proposal...bring an additional 236 instructions...(for a total of around 436 instructions)" So yeah i guess 256 is just enuf for us... 128 might be too little

- just as the ERR register is always used when an instruction needs to return a second value, it should also be used whenever an instruction needs to pass in more than 2 source operands (or, maybe have a separate register that is always used for that). Apparently the hardware design can be easier if special registers are used, rather than having to put a new 'read port' and 'write port' onto the entire 'register file' so that any register can be used

- add with carry
- add but in case of overflow, branch and/or skip
- cmov
- MAC

- i dont remember how the register-memory addr mode works but:
  - compare it to the "load dest addr index" instruction that i had before
  - compare it to displacement (constant; is this multiplied by scale too?), base (register), index (register; will be multiplied by scale), scale (2-bit constant: 1,2,4,8)
- mb have a Calculate Effective Address (like LEA) since i doubt we can fit all those into one operand
  - also, remember we can use the stack regs as extra operands...
  - also, remember we have that third operand for loads and for stores. Maybe we can have multiple types of loads/stores that use that third operand differently; as an index, as a displacement.
  - also, we could have some registers be displacement regs and some be associated index regs and some be associated scale regs, have multiple groups of these 'associated registers', and then the actual operand need only specify the group being used

- mb specify a max distance down the memstack that may be cached. Maybe 32 (32 items or 32 bytes? Why were we ever considering addressing the memstack in units larger than bytes again? oh yeah, i think it was so that programs could abstract away from ptr size, for portability).

- if op0 can't do immediate mode addressing, then instead of inventing another crazy addressing mode for that, could just use that bit as a format bit. That means that there would be two format bits in op0 alone. If we then also said that 16-bit instruction encoding had two format bits, now we don't need a third format bit, and so all format bits are taken care of in OP0 alone. This means that we have room for 8 bit immediate in each of OP1 and OP2, but only if we don't need any addressing mode bits for that. Still, this allows for 16-bit immediate in a two operand format, and for 22-bit jump offsets while still having eight opcodes. Alternately we could have a 7-bit op code and have three format bits, allowing us to only have one format bit in the 16-bit encoding.
  otoh, when we are using register mode in op1 and/or op2, what are we doing with all of those extra bits in those operands?
    RISC-V's weird immediate fields are making more and more sense...

- 
how about:
- 8 opcode bits
- 2 format bits
- 1 immediate addr mode bit
- 5 op0 bits
- 3 op1 addr mode bits (part of 16-bit immediate when 2 operand instruction and immediate addr mode bit set)
- 5 op1 bits
- 3 op2 addr mode bits (part of 16-bit immediate when 2 operand instruction and immediate addr mode bit set, part of 8-bit immediate when 3 operand instruction and immediate addr mode bit set)
- 5 op2 bits

- one question is, for the deep smallstack regs, should they be writable? i think yes

-
i'm thinking that for stack addressing, we just want two components, instead of 4 like i currently have in boot_reference:
- #ptrs  (1 bits)
- #bytes (4 bits)


