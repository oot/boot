# Boots design

This document explains the 'why' behind Boots design choices.

[[_TOC_]]

# Big picture


## Context

Boots is part of the LOVM project, which aims to create a portable target language that high-level languages can be written on top of.

Two of the goals of LOVM are to be:
* easily portable
* an interoperable glue language

The idea is that a high-level language can target LOVM, and write its runtime (if any) in LOVM, and in this way be able to be easily ported to many platforms. These platforms can themselves be high-level languages, and code targetting LOVM will be able to interoperate with the underlying platform (calling platform functions, being called by platform functions, and passing high-level data structures).

The plan is to build two layers:
- Boot, an assembly language
- LOVM,  a flexible target language (LOVM stands for LOw-level Virtual Machine) which is an extension of Boot, and can be compiled to Boot

To make porting easier, Boots (Boot Simple)
- is even easier to implement
- offers enough functionality to write compilers in (also other batch processing programs) 

An implementation of Boots can be incrementally extended to an implementation of Boot.

So the overall picture is:
- Boots, a spartan low-level assembly-language-like VM which is dead simple to implement
- Boot, a suitable target for a general-purpose high-level language which can be compiled to Boots
- LOVM, a flexible target language which is an extension of Boot
- Lo,   a low-level human-readable language to make programming in LOVM less tedious

Boots is a subset of Boot which has just enough functionality to assist in bootstrapping LOVM to a new platform. The LOVM project toolchain can be compiled to Boots executables, and Boots is so simple that a Boots interpreter or compiler can be easily implemented on a new platform.

It is not the case that all implementations of LOVM will be built on top of Boots. In fact, good interoperability and performance can most likely only be achieved by implementing LOVM directly on a target platform, or even by skipping the LOVM project entirely and directly implementing higher-level-languages. However, Boots provides a 'good enough' platform that is quicker to port. In addition, because Boot and LOVM are extensions of Boots, the effort spent to create a Boots implementation can for the most part be re-used towards creating Boot and LOVM implementations.


### Why not just use LLVM, WASM, RISC-V, OpenRISC, ARM, x86, MIPS, SPARC, Lua, .NET, JVM, C, Zig, etc? ===
Even the simplest of these is harder to implement (to port or to create a new backend for) than i wanted. I want Boots to be really really easy to implement, so that it is easy to port to many platforms. I want to port it to run on top of each of: hardware ISAs (like ARM and x86), common VMs (like JVM and CLR), and high-level languages (like Python and Haskell)).

In addition, for greater portability and easier implementation on top of a variety of other platforms (including high-level languages), i wanted Boots to be somewhat agnostic as to the representation of integers and pointers, so that the implementation can just use whatever native number and pointer types are provided.

In addition, many of the existing virtual machines are not 'assembly-language-like', but rather, make assumptions about the sorts of control flow that might occur in the high-level languages that they support (typically they make 'structured programming' sorts of assumptions and encapsulate some aspects of the callstack); this can make it hard (or at least annoying and/or inefficient) for languages written on top of these VMs to provide things like tailcalls, continuations, coroutines (which may have to save/restore the stack when switching contexts), resumable exceptions, debugging facilities that inspect the stack, etc. Assembly language circumvents these restrictions by providing first-class stacks and 'goto'; therefore Boots is an 'assembly-language-like' VM.

I doubt Boots will be as efficient as more serious efforts such as WASM and RISC-V, but this is not a critical flaw, because although performance is a goal of Boot, easy portability/easy implementation is a higher priority goal for Boots. In situations demanding the highest level of performance, you may want to skip both Boots and Boot and target native code directly.


### Why have all of Boots and Boot?
Originally I planned to just have Boot, but when I tried to implement it, I found that it was too complicated for me to implement in a weekend.

Boots is simpler to implement but Boot is probably more performant and probably simpler to target. Boot can be easily compiled to Boots.

### Why not only Boots, and not Boot?

- Boots is so spartan that it would be a pain to write and debug LOVM in Boots
- Boot can probably achieve higher code density and performance
- Boot is probably easier to target

### Should I target Boots if I want to run something on a Boots platform?

Boots is not designed to be directly targeted. Rather, Boots is designed to be easy to implement, so that the whole stack is easy to port. A Boot program can be more easily efficiently compiled than a Boots program. If you target Boot instead of Boots, then if a Boot implementation later becomes available on your platform, you can take advantage of the efficiency of Boot. Also, the toolchain is written with the assumptions that only Boot will be targeted, and Boots is only produced as a result of the Boot tooling.

The lowest level that is designed to be directly written by humans is Lo.

### Should I implement Boots or Boot on my platform?
You should probably implement Boots first and then if you have time, extend it to Boot. Boots and Boot were designed so that an implementation of Boots can be incrementally extended into an implementation of Boot. A direct implementation of Boot will probably be able to compile or run programs much more efficiently than a direct implementation of Boots.






## History
Boots was originally created to serve as a portable target language for the Oot high-level programming language. In fact, this is still the plan; however, the LOVM stack is being split off into a separate project, because it could serve as a substrate for many different higher-level-languages.


# Overall Architecture

## Encoding

### Why are immediate ranges limited to 127, 16383, and 2097151? Since we have 3 operand fields, why not 255, 65535, 16777215?
We want all of the possible instructions in Boots to be equivalent to instructions in Boot. Boot has a different encoding format that only has 7 bits per operand field.


### What are some disadvantages of this encoding?
- We are optimizing for ease of implementation over code density -- so we focus on keeping the encoding format uniform and simple rather than dense
- We can't represent absolute address literals

### What are some disadvantages of this instruction set?
- We are optimizing for ease of implementation over code density -- so many common operations require more instructions and use more registers than competing ISAs. For example, we don't provide an ADD-IMMEDIATE instruction, so to add an immediate constant to a register, you must first load the immediate into a temporary register, and then add it. This sort of thing is one reason why we say that you should target Boot, not Boots.
- Because pointers are opaque and we have typed instructions, we spend multiple opcodes on some things that would only consume one opcode in other ISAs (for example, instead of just cp we have cp, cpp, cpl, cph)

### Why do you allow li32, ll32, j32 to have embedded immediates after the instruction?
The alternatives seemed to be:

- 2MB binary size limitation (with 21 bit immediates in one instruction), unless the compiler or program can do something complicated like 'jump islands'
- embedded 32-bit label and jump data after instructions (what we chose)
- load immediates over two instructions (load low immediate, load high immediate)
- change the 7-bit limit on Boots operands to 8-bits, but then Boot wouldn't be able to directly represent all Boots instructions, and you still have a 16MB binary size limitation
- introduce a constant table of labels
  - in this case the program would be able to access via llk/jk and/or via pointer to the constant table via a pointer-returning-version-ofsinfo (and then, in Boot, immediate mode on labels)
  - the problem here is how to portably define the contents of the constant table of labels via standardized Boots. Also, labels are most efficiently implementation-dependent, although we could use 32-bit integer offsets into bytes of Boots code
    - we could say that the table is 'at the beginning'
    - have a Boots 'pragma' instruction and have a pragma marking the beginning of the constant table
    - accept non-portability
    - have globals labels in the assembly (requiring the assembler to do a global scan)
  - also, would we have global labels in the assembly (eg @1 @32) or do we encode offsets using .d?
- 64-bit Boots instruction encoding

The problems with each alternative are:

- 2MB binary size limitation (with 21 bit immediates in one instruction), unless the compiler or program can do something complicated like 'jump islands'
  - a lot of real world programs are larger than this
- embedded 32-bit label and jump data after instructions (what we chose)
  - possibility of an instruction and its data spanning a page boundary, causing inefficiency, and making hardware implementations more complex
- load immediates over two instructions (load low immediate, load high immediate)
  - either there are special instructions for this (and for jumping to the immediate) and they are restricted to only appear next to each other, or jump immediate constants get executed as jy, obscuring the immediateness of the jump target (and so making optimization harder)
- change the 7-bit limit on Boots operands to 8-bits
  - now Boot can't directly represent all Boots instructions, and you still have a 16MB binary size limitation
- introduce a constant table of labels
  - program can access via llk/jk and/or via pointer to the constant table via a pointer-returning-version-ofsinfo (and then, in Boot, immediate mode on labels)
  - the problem here is how to portably define the contents of the constant table of labels via standardized Boots. Also, labels are most efficiently implementation-dependent, although we could use 32-bit integer offsets into bytes of Boots code
    - we could say that the table is 'at the beginning'
      - but that seems suboptimal, also, it prevents you from having a purely 'streaming' implementation
    - have a Boots 'pragma' instruction and have a pragma marking the beginning of the constant table
      - a little ugly, probably no easier to implement than embedded label and jump data, and again, prevents easy 'streaming' b/c global state during assembly
    - accept non-portability
    - have globals labels in the assembly
      - requires the assembler to do a global scan
  - also, would we have global labels in the assembly (eg @1 @32) or do we encode offsets using .d?
- 64-bit Boots instruction encoding
  - big waste of space



## Why not just a stack machine?
If a goal of Boots is easy implementation, then since Boots has a stack, why is it not just a stack machine, instead of also having registers?

- since Boot has local variables, this allows an implementation of Boots to be more easily incrementally extended into an implementation of Boot
- in some contexts register machines can have higher performance than stack machines
- having both registers and a stack may allow Boot to have higher code density

## Why undefined behavior?
If the target platforms that we want to compile Boots to, like C, have undefined behavior. We want to make Boots easy to correctly implement, and on some platforms with undefined behavior, the most obvious/easy implementation of many operations makes undefined behavior possible.


## Which kinds of instructions are needed for Turing completeness?
In the model of computation that we are using, the following are needed for Turing completeness:
  - load constant
  - addition
  - some sort of conditional
  - jumps

## Is Boots a RISC instruction set?
Yes.

- it is a 'load/store architecture', that is, only a few special instruction access main memory: l8 l8u l16 l16u l32 lp s8 s16 s32 sp ll sl lh sh (and depending on how you define it, li32, ll32, j32, and/or in1, out1, sys), with no instruction accessing memory more than once (again, depending on how you define it, possibly excepting sys)
- the encoding format is regular and fixed-width
- the number of instructions is not terribly many
- the amount of work done by each instruction (aside from sys) is small.

# Immediates



# Arithmetic and comparisons

## Integer bitwidth/representation

### Why 32-bit arithmetic rather than 16-bit?

Conceptually, I would have preferred 16-bit, but 32-bit is more pragmatic because:
- almost all commodity platforms support it
- there are many programs larger than 64KiB in size, would makes jumping between parts of the program awkward if you only have 16-bit pointers
- there are many programs that access data larger than 64KiB in size, which is awkward if you only have 16-bit pointers

### Why 32-bit arithmetic rather than 64-bit?

There are many embedded platforms that don't support 64-bit, and it is not necessary for many programs. We are trying to be minimalistic.


### Why 32-bit arithmetic rather than not defining a maximum bitwidth of integers?

- although C's "int" type only has a minimum, not a maximum, bitwidth, both LLVM and WASM integer types specify exact bitwidths, so I figure that other people who have thought about this decided that's better
- on a platform with a larger native integer bitwidth, to get an operation in 32-bit bitwidth, you just have to do the same operation on the native bitwidth and then mask out the unwanted high-order bits with a bitwise AND against 2^31-1. The impact on performance is relatively small; the type of platform where you most care about performance would be compilation to a hardware ISA (eg x86-64, or ARM), and in hardware, bitwise AND tends to be cheap.
- this makes the specification of the semantics slightly shorter than if you had to specify operations on an unknown bitwidth

### Why wraparound arithmetic?

- Wraparound arithmetic is the only primitive in many targets
- On a platform without wraparound arithmetic, 32-bit wraparound arithmetic can usually be accomplished efficiently by doing non-wraparound arithmetic in a 2x larger bitwidth, then doing a bitwise AND against 2^31

## Integer operations

### Why sub32?
Subtraction isn't strictly necessary for Turing completeness (it could be implemented by using addition in a loop to count up from zero until first one and then the other argument is hit). But it is (almost?) always available on target platforms, and it is extremely useful.

### Why mul32?
MUL seems to be fairly common among even higher-end embedded processors and where it is not present, it is simple for a Boots implementor to write an unsigned integer multiplication routine.

Many platforms which lack a native multiplication operation are so resource-constrained that Boots may not be appropriate; either programmers will be writing directly in platform-native assembly, or they will be using a higher-level language which directly targets platform-native assembly (without using Boots as an intermediate language). However, for situations in which a subset of Boots is desired that is easy to implement on extremely simple platforms, the Tiny Boots subset is defined.

#### Why does mul32 only return the lower 32 bits, without any (included) way to get the high-order bits?
Some target platforms only offer multiplication in this form.

## Why no division instruction?
To facilitate ease of implementation on many platforms, in Boots we're looking for the lowest common denominator. We can implement these other things at the Ovm level.

## Comparison operations

### Why if-equal and if-less-than?
Having both if-equal and if-less-than isn't strictly necessary (for example, if-less-than could be implemented from if-equal and subtraction) but are extremely common on target platforms and are extremely useful.

## Bitwise arithmetic

### Why bitwise arithmetic?
These are not strictly necessary but are common across many platforms. They are also easy to implement on platforms that have other arithmetic but not these.


# Pointers
## Why pointer addition?
Addition of integers to pointers allows us to access a field within a struct when we are passed a struct.

## Why is there no way to coerce integers to pointers?
Some target platforms may not support this. On platforms that do support it, the structure of a pointer may differ across platforms. For example, different platforms may represent a Boots 'pointer' as:
- an offset into a linear array of memory
- a pair where the first item specifies one of many pools of memory (for example, an index into a list of native pointers), and the second item specifies an index into the chosen memory pool
- a 'fat pointer' that concatenates a native pointer with information about the bounds of the associated chunk of memory that the Boots program is allowed to access

These differences in internal pointer structure would make it impossible to write portable programs that manipulate pointers as integers, even if Boots allowed this. Since Boots's purpose is writing portable programs, and since some platforms may not support manipulating pointers as integers, there is no need to make the lives of people implementing Boots on those platforms more difficult.

However, for situations in which a Boots-like language is desired which allows direct manipulation of pointers as offsets into a linear array of memory, the Vanilla Boots extensions are defined.

# Control flow

## Why compare-and-branch instructions instead of separate comparison and branching instructions?

RISC-V's Andrew Waterman criticizes the lack of compare-and-branch instructions in some other ISAs, for example in Chapter 2 of [https://people.eecs.berkeley.edu/~krste/papers/EECS-2016-1.pdf].


## Why jy?
jy is needed for fully dynamic control flow.

Technically, jy is not needed for Turing completeness because without it you could still implement an interpreter, and then programs running within that interpreter could have dynamic control flow. But we want Boots programs themselves to be able to directly express dynamic control flow.

For situations in which a subset of Boots is desired that is easy to compile into a language which doesn't support fully dynamic control flow, the Static Control Flow Boots subset is defined.

## Why do branch instructions allow branching to useless offsets -2, -1, or 0?
It is true that branching to offsets -2, -1, or 0 are useless. If we had specified that branch offsets were to be interpreted by a special encoding which omitted these values, we could have increased the branch range slightly without reducing expressiveness. However, that would have been more complicated.


# Misc instructions
## Why annotations?
Annotations are useful for tooling. A compiler can elide them, so they needn't have a significant performance impact in situations where high performance is needed.

## Why sinfo?
Sinfo is useful for expressing platform constants known at compile-time. A compiler can replace each instance of sinfo with a load-constant instruction loading the relevant constant, and/or can propagate those constants.



# Syscalls

## Why halt?
Halt is not strictly necessary (the end of the program code could implicitly signify HALT) but it's commonly available in other languages, and it allows code to specify a halt at any point in more distinctive way, compared to having to jump past the end of the program (which would depend on where the end of the program is).

## Why memcpy?
memcpy provides a simple platform-independent way of blindly copying memory.

By comparison, one might imagine blindly copying memory by iterating over memory locations, loading them as integers, and then storing them as integers to the destination. But some target platforms may not allow pointers to be coerced to/from integers, causing difficult when one attempts to read/write pointers from/to memory using integer operations.


# Tooling
## Why doesn't the textual assembly language provide labels?
Since Boots is not designed to be written by humans, ease of implementation trumps ease of use. The assembly language for Boot provides labels, and Boot compiles to Boots.


