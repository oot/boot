# Boots Extension Reference

Version: unreleased (0.0.0-0)

VERY EARLY NOTES. PLANNING FOR A FUTURE PROJECT.

This document defines various extensions to Boots.

## Introduction

Various numbered 'features' are defined so that programs can test whether an implementation supports desired extensions by testing for the presence or absence of 'features' on a Boots implementation.

Features 0 thru 15 are reserved to indicate the presence of various groupings of extensions. 
Features 0 thru 8 form a linear chain of 'profiles', profile 0, profile 1, etc, which may be referred to as "Boots0", "Boots1", etc. The presence of a profile implies the presence of all lesser profiles.

An implementation may support, and a program may require the presence of, any subset of features (provided inter-feature dependencies are respected). However, when possible, to reduce fragmentation, we suggest that both implementators and program-writers support profiles rather than arbitrary subsets of features.


## Table of Contents
[[_TOC_]]

## Overview of profiles

0: **base Boots**

1: **32-bit static control flow interactive programs**
- 32-bit arithmetic
- signed arithmetic
- multiply instruction
- exit syscall

must be present but might not have resources:
- I/O
- memgrow

2: **64-bit**
- dynamic control flow
- 64-bit
- divide instruction
- floating point arithmetic
- 32-bit immediates embedded in instruction stream

3: **Single-threaded systems**
- triglog
- rand
- rc atomics

must be present but might not have resources:
- malloc, clocks
- filesystem
- logging

4: **IPC and TUI**

must be present but might not have resources:
- IPC (inter-process communication)
- TUI (text user interface)

5: **Headless server**

must be present but might not have resources:
- process control
- blocking and non-blocking I/O
- networking

6: **Desktop**

must be present but might not have resources:
- vector and raster graphics
- event loop, keyboard and pointer devices
- CGI/GPU stuff

Misc features not in any profile:
- **Vanilla**


## Profile 0: base Boots

The base profile, standard Boots without extensions, is called "Boots0" and is defined in boots_reference.md. This profile is suitable for 15-bit unsigned integer calculations without multiplication, dynamic control flow, I/O, or memory growth.

## Profile 1: 32-bit static control flow interactive programs

Profile 1 adds 32-bit arithmetic, I/O, floating-point of unspecified bitwidth, multiply, exit, and memgrow. Profile 1 is suitable for 32-bit integer programs less than 1 MB in size without dynamic control flow. Profile 1 includes everything in profile 0 and adds:

instruction extensions:
- 32_bit_integers
- signed arithmetic
- integer_multiply
- io1, out1

syscall extensions:
- exit
- memgrow
- io
- in, out
- exit


TODO OUT-OF-DATE NOTES:
NOTE: Profile 1 is what pyboot3 implements, except without floats 

NOTE: Our Lo toolchain will require dynamic control flow but not floats or 64-bits.

## Profile 2: 64-bit

Profile 2 adds 32-bit and 64-bit floating-point, 64-bit integers, embedded 32-bit immediates (which allows jump instructions to have 2^31 byte offsets), divide, and dynamic control flow. Profile 2 includes everything in profile 1 and adds:

instruction extensions:
- f32
- f64
- 64_bit_integers
- dynamic_control_flow
- integer_divide
- embedded_immediates



It is guaranteed that:
- 1 <= INT64_SIZE <= 8
- INT32_SIZE <= INT64_SIZE <= 2*INT32_SIZE (an int64 is not smaller than an int32, and an int64 can fit within the size of two int32s)
- INT32_SIZE <= PTR_SIZE (an int32 can fit within the size of a pointer)

Note: because INT32_SIZE <= PTR_SIZE, a program can opt to use fixed-size 'slots' of size PTR_SIZE to hold things. A slot of size PTR_SIZE is guaranteed to fit any of: an int8, and int16, an int32, a ptr, a label, a handle. Two slots of PTR_SIZE can fit an int64.

TODO NOTE: In some ways this profile is comparable to WASM.


## Profile 3: Single-threaded systems

Profile 3 adds filesystems, malloc, RC atomics, math, and various system functionality. Profile 3 includes everything in profile 2 and adds:

instruction extensions:
- rc_atomics

syscall extensions:
- filesystem
- malloc
- clocks
- rand
- logging
- triglog (math.h type stuff; stuff that is not in WASM or not in RISC-V but is in math.h)




## Profile 4: TUI, single-threaded but with IPC

Profile 4 adds higher-level IPC and text-based user interface I/O. Profile 4 is suitable for single-threads within a larger concurrent computation and for single-threaded non-networked interactive programs. Profile 4 includes everything in profile 3 and adds:

instruction extensions:
- seqsc_atomics

syscall extensions:
- ipc
- tui

## Profile 5: server (multi-threaded with filesystem, networking)

Profile 5 adds process control, blocking and non-blocking I/O, and network I/O. Profile 5 is suitable for headless server programs. Profile 5 includes everything in profile 4 and adds:

syscall extensions:
- process_control
- networking
- blocking_io
- aio (TODO: polling? callback? both?)


TODO:

  - callback-only aio (like https://emscripten.org/docs/porting/guidelines/api_limitations.html https://emscripten.org/docs/porting/files/file_systems_overview.html , where it sounds like you can't poll for completeness, you can only register a callback). For full aio, see the next profile (where you can poll for completeness, meaning that you can effectively block)
    - eh, some systems may want to offer poll-based aio but not callback-based. Rethink this.

TODO Note: the idea with aio is that above, io1 and io provide I/O which may or may not be blocking (implementation-dependent). At this level you get to choose if you want blocking or not.


## Profile 6: desktop

Profile 6 adds more complex user interfaces. Profile 6 is suitable for interactive programs with a graphical user interface. Profile 6 includes everything in profile 5 and adds:


syscall extensions:
- vector_graphics
- raster_graphics
- event_loop
- keyboard_input_devices
- pointer_input_devices
- cgi/3d graphics card stuff


## Misc features not in any profile

### Vanilla feature



## Comments
The Boot->Boots compiler and interpreter requires Boots1. The Boot toolchain requires Boots3.


# TODO
- flesh out the actual features, also assign feature numbers
- i guess there's no reason not to shovel in all the things that we're expecting to implement in LOVM/in LOVM's stdlib, even data structures, garbage collection, etc, at least insofar as it can be expressed as syscalls rather than structurally. That way implementors can provide some level of platform interop without implementing LOVM. LOVM can do feature-test-or-branch to then use its own implementations when its Boot doesn't provide.
- let the I/Os indicate unavailability if the desired device type doesn't exist on this system
  - mb each such I/O feature has a paired secondary feature to indicate if an I/O is actually supported, vs. just being a set of API calls that always fail gracefully
- check out WASI: https://github.com/WebAssembly/WASI/blob/master/phases/snapshot/docs.md



