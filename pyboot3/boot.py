#!/bin/python3

import pyboot

import argparse, sys, io, dataclasses

parser = argparse.ArgumentParser(description='Run a Boot program')
parser.add_argument('infile', type=str)
parser.add_argument('--verbose', '-v', action='count', default=0)
#parser.add_argument('--stacksize', '-ss', type=int, default=256)
args = parser.parse_args()

if args.infile == '-':
    infile = sys.stdin.buffer
else:
    infile = open(args.infile, 'rb') # todo untested
allcode = infile.read()
#print(repr(allcode))
code = io.BytesIO(allcode)
#print(repr(code))



@dataclasses.dataclass
class Pointer:
    memory_block_idx: int
    offset: int

    def memory_block(self):
        return all_memory_blocks[self.memory_block_idx]
    
    def read(self):
        return self.memory_block()[self.offset]

    def write(self, value):
        self.memory_block()[self.offset] = value

    def add(self, addend):
        return dataclasses.replace(self, offset=self.offset+addend)

    def clone(self):
        return dataclasses.replace(self)
    
def read_register(index):
    return registers[index]

def write_register(index, value):
    if index != 0:
        registers[index] = value
    else:
        return

def print_pos(instruction_maybe_w_data, pos):
        print(f"At {pos}:", file=sys.stderr)

def print_disassembly_at_pos(instruction_maybe_w_data, pos):
        disassembled_string = "  " + pyboot.instruction_to_assembly(instruction_maybe_w_data.instruction, pos) + "\n"
        if instruction_maybe_w_data.data is not None:
            disassembled_string += "  " + pyboot.data_to_assembly(instruction_maybe_w_data.data) + "\n"
        print(disassembled_string, end='', file=sys.stderr)

def print_state():
    print_regs()
    print_mem()
    
def print_mem():
    for memory_block_idx, memory_block in enumerate(all_memory_blocks):
        print(f'    memblock {memory_block_idx}:\n    ' + repr(memory_block), file=sys.stderr)

def print_regs():
    print('    regs:\n    ' + repr(registers), file=sys.stderr)


def u32_to_signed(data):
    assert(data < 2**32)
    assert(data >= 0)
    
    if data < 2**31:
        return data
    else:
        return -(2**32 - data)


def u16_to_signed(data):
    assert(data < 2**16)
    assert(data >= 0)
    
    if data < 2**15:
        return data
    else:
        return -(2**16 - data)


def u8_to_signed(data):
    assert(data < 2**8)
    assert(data >= 0)
    
    if data < 2**7:
        return data
    else:
        return -(2**8 - data)
    
def signed_to_u32(data):
    assert(data < 2**31)
    assert(data >= -2**31)
    
    if data >= 0:
        return data
    else:
        return 2**32 + data

    
all_memory_blocks = []
registers = [0]*32
#stackmem  = [0]*args.stacksize
ret = -1


running = True
while (running):
    pos = code.tell()
    instruction_maybe_w_data, num_bytes_read = pyboot.decode_instruction_from_file(code, pos)
    if instruction_maybe_w_data is None:
        #EOF reached
        running = False
        break

    if args.verbose >= 1:
        print_pos(instruction_maybe_w_data, pos)

    if args.verbose >= 2:
        print_regs()

    if args.verbose >= 3:
        print_mem()

    if args.verbose >= 1:
        print_disassembly_at_pos(instruction_maybe_w_data, pos)
        
    opcode = instruction_maybe_w_data.instruction.opcode
    op0    = instruction_maybe_w_data.instruction.op0
    op1    = instruction_maybe_w_data.instruction.op1
    op2    = instruction_maybe_w_data.instruction.op2
    data = instruction_maybe_w_data.data
    if data is not None:
        data   = instruction_maybe_w_data.data.data
    
    
    if args.verbose >= 4:
        print((opcode, op0, op1, op2, data))

    if opcode == pyboot.opcode2mnemonic.index('ann'):
        pass

    if opcode == pyboot.opcode2mnemonic.index('hint'):
        pass

    if opcode == pyboot.opcode2mnemonic.index('break'):
        print(f'  BREAKPOINT AT {pos}', file=sys.stderr)
        print_state()

    if opcode == pyboot.opcode2mnemonic.index('impl'):
        pass

    if opcode == pyboot.opcode2mnemonic.index('j32'):
        offset = u32_to_signed(data)
    
        if args.verbose >= 4:
            print(f'  offset: {offset}')
        code.seek(offset, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('li32'):
        write_register(op0, data)

    if opcode == pyboot.opcode2mnemonic.index('ll32'):
        write_register(op0, data)

    #TODO
    if opcode == pyboot.opcode2mnemonic.index('feat'):
        pass
    
    if opcode == pyboot.opcode2mnemonic.index('j21b'):
        code.seek(-op0, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('j21f'):
        code.seek(op0, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('syscall'):
        if op0 == 0:
            # halt
            ret = registers[5]
            running = False
            if args.verbose >= 1:
                print(f"  halt with exit code: {ret}", file=sys.stderr)
                
        if op0 == 1:
            # memcpy
            dst, src, size = registers[5], registers[6], registers[7]
            for i in range(size):
                dst.write(src.read())
                dst = dst.add(1)
                src = src.add(1)
        if op0 == 2:
            # malloc
            size = registers[5]
            try:
                # search for a free spot in the block list
                idx = all_memory_blocks.index(None)
            except ValueError:
                # no free spot in the blocklist, add a new item at the end
                idx = len(all_memory_blocks)
                all_memory_blocks.append(None)
            # allocate a new memory block and put a reference to it into the blocklist
            all_memory_blocks[idx] = [0]*size
            registers[5] = Pointer(memory_block_idx=idx, offset=0)
        if op0 == 3:
            # free
            ptr = registers[5]
            all_memory_blocks[ptr.memory_block_idx]= None

    if opcode == pyboot.opcode2mnemonic.index('llb'):
        write_register(op0, code.tell() - op1)

    if opcode == pyboot.opcode2mnemonic.index('llf'):
        write_register(op0, code.tell() + op1)

    if opcode == pyboot.opcode2mnemonic.index('li'):
        write_register(op0, op1)

    if opcode == pyboot.opcode2mnemonic.index('lin'):
        write_register(op0, 2**32-op1)

    if opcode == pyboot.opcode2mnemonic.index('lih'):
        if op1 == 0:
            # STDIN/STDOUT device
            write_register(op0, 0)
        
    if opcode == pyboot.opcode2mnemonic.index('sinfo'):
        if op1 == 0:
            # VERSION
            write_register(op0, 0)
        if op1 == 2:
            # PTR_SIZE
            write_register(op0, 1)
        if op1 == 3:
            # INT16_SIZE
            write_register(op0, 1)
        if op1 == 4:
            # INT32_SIZE
            write_register(op0, 1)
        else:
            pass

    if opcode == pyboot.opcode2mnemonic.index('beqf'):
        if read_register(op0) == read_register(op1):
            code.seek(op2, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('bnef'):
        if read_register(op0) != read_register(op1):
            code.seek(op2, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('bltf'):
        if u32_to_signed(read_register(op0)) < u32_to_signed(read_register(op1)):
            code.seek(op2, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('bltuf'):
        if read_register(op0) < read_register(op1):
            code.seek(op2, io.SEEK_CUR)

    if opcode == pyboot.opcode2mnemonic.index('beqpf'):
        if read_register(op0) == read_register(op1):
            code.seek(op2, io.SEEK_CUR)
        # totest

    if opcode == pyboot.opcode2mnemonic.index('beqlf'):
        if read_register(op0) == read_register(op1):
            code.seek(op2, io.SEEK_CUR)
        # totest

    if opcode == pyboot.opcode2mnemonic.index('beqhf'):
        if read_register(op0) == read_register(op1):
            code.seek(op2, io.SEEK_CUR)
        # totest

    if opcode == pyboot.opcode2mnemonic.index('l32'):
        write_register(op0, read_register(op1).read())

    if opcode == pyboot.opcode2mnemonic.index('l16'):
        value_u16 = read_register(op1).read() & (2**16-1)
        value_signed = u16_to_signed(value_u16)
        value_u32 = signed_to_u32(value_signed)
        write_register(op0, value_u32)

    if opcode == pyboot.opcode2mnemonic.index('l16u'):
        write_register(op0, read_register(op1).read() & (2**16-1))

    if opcode == pyboot.opcode2mnemonic.index('l8'):
        value_u8 = read_register(op1).read() & (2**8-1)
        value_signed = u8_to_signed(value_u8)
        value_u32 = signed_to_u32(value_signed)
        write_register(op0, value_u32)

    if opcode == pyboot.opcode2mnemonic.index('l8u'):
        write_register(op0, read_register(op1).read() & (2**8-1))

    if opcode == pyboot.opcode2mnemonic.index('lp'):
        write_register(op0, read_register(op1).read())

    if opcode == pyboot.opcode2mnemonic.index('ll'):
        write_register(op0, read_register(op1).read())

    if opcode == pyboot.opcode2mnemonic.index('lh'):
        write_register(op0, read_register(op1).read())

    if opcode == pyboot.opcode2mnemonic.index('s32'):
        read_register(op1).write(read_register(op0))

    if opcode == pyboot.opcode2mnemonic.index('s16'):
        value = read_register(op0) & (2**16-1)
        read_register(op1).write(value)

    if opcode == pyboot.opcode2mnemonic.index('s8'):
        value = read_register(op0) & (2**8-1)
        read_register(op1).write(value)

    if opcode == pyboot.opcode2mnemonic.index('sp'):
        read_register(op1).write(read_register(op0))

    if opcode == pyboot.opcode2mnemonic.index('sl'):
        read_register(op1).write(read_register(op0))

    if opcode == pyboot.opcode2mnemonic.index('sh'):
        read_register(op1).write(read_register(op0))

    if opcode == pyboot.opcode2mnemonic.index('cp'):
        write_register(op0, read_register(op1))

    if opcode == pyboot.opcode2mnemonic.index('cpp'):
        write_register(op0, read_register(op1))

    if opcode == pyboot.opcode2mnemonic.index('cpl'):
        write_register(op0, read_register(op1))

    if opcode == pyboot.opcode2mnemonic.index('cph'):
        write_register(op0, read_register(op1))

    if opcode == pyboot.opcode2mnemonic.index('shl'):
        write_register(op0, (read_register(op1) << 1) & (2**32 - 1))

    if opcode == pyboot.opcode2mnemonic.index('shru'):
        write_register(op0, (read_register(op1) >> 1))

    if opcode == pyboot.opcode2mnemonic.index('shrs'):
        value = signed_to_u32(u32_to_signed(read_register(op1)) >> 1)
        write_register(op0, value)

    if opcode == pyboot.opcode2mnemonic.index('add32'):
        write_register(op0, (read_register(op1) + read_register(op2)) & (2**32 - 1))

    if opcode == pyboot.opcode2mnemonic.index('sub32'):
        write_register(op0, (read_register(op1) - read_register(op2)) & (2**32 - 1))

    if opcode == pyboot.opcode2mnemonic.index('mul32'):
        write_register(op0, (read_register(op1) * read_register(op2)) & (2**32 - 1))

    if opcode == pyboot.opcode2mnemonic.index('and'):
        write_register(op0, (read_register(op1) & read_register(op2)))

    if opcode == pyboot.opcode2mnemonic.index('or'):
        write_register(op0, (read_register(op1) | read_register(op2)))

    if opcode == pyboot.opcode2mnemonic.index('xor'):
        write_register(op0, (read_register(op1) ^ read_register(op2)))

    # should this be unsigned?
    if opcode == pyboot.opcode2mnemonic.index('addp'):
        write_register(op0, (read_register(op1).add(u32_to_signed(read_register(op2)))))

    # should this be unsigned?
    if opcode == pyboot.opcode2mnemonic.index('subp'):
        write_register(op0, (read_register(op1).add(-u32_to_signed(read_register(op2)))))

    if opcode == pyboot.opcode2mnemonic.index('jy'):
        code.seek(read_register(op0), io.SEEK_SET)

    if opcode == pyboot.opcode2mnemonic.index('in1'):
        dst_reg = op0
        device = read_register(op1)
        if device != 0:
            raise(pyboot.UnknownDeviceException(f'At position {code.tell()}, unknown device at in1 {op0} {op1}'))
        input = sys.stdin.buffer.read(1)
        if args.verbose >= 1:
            print(f"  read: {repr(input)}", file=sys.stderr)
        if len(input) == 0:  # this happens upon EOF
            result = 0
        else:
            result = input[0]
        write_register(op0, result)
        
    if opcode == pyboot.opcode2mnemonic.index('out1'):
        src_reg = op0
        device = read_register(op1)
        if device != 0:
            raise(pyboot.UnknownDeviceException(f'At position {code.tell()}, unknown device at out1 {op0} {op1}'))
        sys.stdout.write(chr(read_register(op0)))
        sys.stdout.flush()

sys.exit(ret)
