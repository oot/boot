import re, struct, dataclasses, sys

opcode2mnemonic = ['ann', 'hint', 'break', 'impl', 'j32', 'li32', 'll32', 'RESERVED_7', 'feat', 'j21b', 'j21f', 'syscall', 'RESERVED_11', 'RESERVED_12', 'RESERVED_13', 'RESERVED_14', 'RESERVED_15', 'llb', 'llf', 'li', 'lin', 'lih', 'sinfo', 'RESERVED_21', 'RESERVED_22', 'RESERVED_23', 'RESERVED_24', 'RESERVED_25', 'RESERVED_26', 'RESERVED_27', 'RESERVED_28', 'RESERVED_29', 'RESERVED_30', 'RESERVED_31', 'beqf', 'bnef', 'bltf', 'bltuf', 'beqpf', 'beqlf', 'beqhf', 'RESERVED_39', 'RESERVED_40', 'RESERVED_41', 'RESERVED_42', 'RESERVED_43', 'RESERVED_44', 'RESERVED_45', 'RESERVED_46', 'RESERVED_47', 'RESERVED_48', 'RESERVED_49', 'RESERVED_50', 'RESERVED_51', 'RESERVED_52', 'RESERVED_53', 'RESERVED_54', 'RESERVED_55', 'RESERVED_56', 'RESERVED_57', 'RESERVED_58', 'RESERVED_59', 'RESERVED_60', 'RESERVED_61', 'RESERVED_62', 'RESERVED_63', 'RESERVED_64', 'RESERVED_65', 'RESERVED_66', 'RESERVED_67', 'RESERVED_68', 'RESERVED_69', 'RESERVED_70', 'RESERVED_71', 'RESERVED_72', 'RESERVED_73', 'RESERVED_74', 'RESERVED_75', 'RESERVED_76', 'RESERVED_77', 'RESERVED_78', 'RESERVED_79', 'RESERVED_80', 'RESERVED_81', 'RESERVED_82', 'RESERVED_83', 'RESERVED_84', 'RESERVED_85', 'RESERVED_86', 'RESERVED_87', 'RESERVED_88', 'RESERVED_89', 'RESERVED_90', 'RESERVED_91', 'RESERVED_92', 'RESERVED_93', 'RESERVED_94', 'RESERVED_95', 'RESERVED_96', 'RESERVED_97', 'RESERVED_98', 'RESERVED_99', 'RESERVED_100', 'RESERVED_101', 'RESERVED_102', 'RESERVED_103', 'RESERVED_104', 'RESERVED_105', 'RESERVED_106', 'RESERVED_107', 'RESERVED_108', 'RESERVED_109', 'RESERVED_110', 'RESERVED_111', 'RESERVED_112', 'RESERVED_113', 'RESERVED_114', 'RESERVED_115', 'RESERVED_116', 'RESERVED_117', 'RESERVED_118', 'RESERVED_119', 'RESERVED_120', 'RESERVED_121', 'RESERVED_122', 'RESERVED_123', 'RESERVED_124', 'RESERVED_125', 'RESERVED_126', 'RESERVED_127', 'l32', 'l16', 'l16u', 'l8', 'l8u', 'lp', 'll', 'lh', 's32', 's16', 's16u', 's8', 's8u', 'sp', 'sl', 'sh', 'cp', 'cpp', 'cpl', 'cph', 'shl', 'shru', 'shrs', 'add32', 'sub32', 'mul32', 'and', 'or', 'xor', 'addp', 'subp', 'jy', 'in1', 'out1']


class AssemblerException(Exception):
    pass
class SyntaxError(AssemblerException):
    pass
class RangeError(AssemblerException):
    pass
class UnknownMnemonicError(AssemblerException):
    pass

class DisassemblerException(Exception):
    pass
class UnknownOpcode(DisassemblerException):
    pass


class BootRuntimeException(Exception):
    pass
class UnknownDeviceException(BootRuntimeException):
    pass

class UnknownOpcode(DisassemblerException):
    pass


class BootInstructionOrData:
    pass

@dataclasses.dataclass
class BootInstruction(BootInstructionOrData):
    opcode: int
    op0: int
    op1: int
    op2: int

    def immediate_spans_howmany_fields(self):
        return immediate_spans_howmany_fields(self.opcode)
    
    def has_data(self):
        return has_data(self.opcode)

@dataclasses.dataclass
class BootData(BootInstructionOrData):
    data: int
    numbytes: int

# ins=BootInstruction, data=None|BootData
@dataclasses.dataclass
class BootInstructionMaybeWithData:
    instruction: BootInstruction
    data: BootData


# todo: deal with feat    
def num_operands(opcode):
    imm_span = immediate_spans_howmany_fields(opcode)
    if   imm_span == 3:
        return 1
    elif imm_span == 2:
        return 2
    elif imm_span == 1:
        return 3
    elif imm_span == 0:
        return 3
    else:
        assert(False)
    
# todo: deal with feat    
def immediate_spans_howmany_fields(opcode):
    if   (opcode < 8):
        return 0
    if   (opcode >= 8) and (opcode <= 15):
        return 3
    elif (opcode >= 16) and (opcode <= 31):
        return 2
    elif (opcode >= 32) and (opcode <= 127):
        return 1
    elif (opcode >= 128):
        return 0
    else:
        assert(False)

def has_data(opcode):
    if (opcode >= 4) and (opcode <= 7):
        return True
    else:
        return False
    

data_line_regexp = re.compile(r'^\.d (([0-9a-f][0-9a-f]){1,4})\s*(;.*)?$')
instruction_line_regexp = re.compile(r'^([a-z][a-z0-9]+) ([0-9a-f]+)( ([0-9a-f]+))?( ([0-9a-f]+))?\s*(;.*)?$')

def assemble_line(line, lineno):
    parsed_line = parse_line(line, lineno)
    if parsed_line is None:
        return b''
    #sys.stderr.write(repr(parsed_line ))
    if isinstance(parsed_line, BootData):
        return encode_data(parsed_line.data, parsed_line.numbytes)
    else:
        assert(isinstance(parsed_line, BootInstruction))
        #print(f'instruction line: {parsed_line.opcode} {parsed_line.op0} {parsed_line.op1} {parsed_line.op2} ')
        #print()
        
        return encode_instruction(parsed_line.opcode, parsed_line.op0, parsed_line.op1, parsed_line.op2)

#unused
def disassemble_instruction(encoded_instruction, pos):
    instruction = decode_instruction(encoded_instruction)
    print(instruction)
    return (instruction_to_assembly(instruction, pos), instruction.has_data())

def instruction_to_assembly(instruction, pos):
    assert(instruction.opcode >= 0)
    if instruction.opcode >= len(opcode2mnemonic):
        raise UnknownOpcode(f"At position {pos}: Unknown opcode {instruction.opcode}:\n  {instruction}")
    mnemonic = opcode2mnemonic[instruction.opcode]
    if   instruction.op2 is not None:
        return f'{mnemonic} {instruction.op0} {instruction.op1} {instruction.op2}'
    elif instruction.op1 is not None:
        return f'{mnemonic} {instruction.op0} {instruction.op1}'
    else:
        return f'{mnemonic} {instruction.op0}'

def decode_instruction(encoded_instruction):
    opcode, opfield0, opfield1, opfield2 = encoded_instruction
    #sys.stderr.write(f'{opcode}, {opfield0}, {opfield1}, {opfield2}\n')
    imm_span = immediate_spans_howmany_fields(opcode)
    #sys.stderr.write(f'{imm_span}\n')
    
    if   imm_span == 3:
        
        op0 = (opfield2 << 16) + (opfield1 << 8) + opfield0
        op1 = None
        op2 = None
    elif imm_span == 2:
        op0 = opfield0
        op1 = (opfield2 << 8) + opfield1
        op2 = None
    elif imm_span == 1:
        op0 = opfield0
        op1 = opfield1
        op2 = opfield2
    else:
        op0 = opfield0
        op1 = opfield1
        op2 = opfield2
    return BootInstruction(opcode, op0, op1, op2)
    

def disassemble_data(encoded_data):
    boot_data = decode_data(encoded_data)
    return data_to_assembly(boot_data)

def data_to_assembly(boot_data):
    hexified = hex(boot_data.data)[2:]
    len_should_be = boot_data.numbytes*2
    zeros_to_pad = len_should_be - len(hexified)
    hexified = ('0'*zeros_to_pad) + hexified
    return f'.d {hexified}'

def decode_data(encoded_data):
    if len(encoded_data) == 1:
        return BootData(struct.unpack('<B', encoded_data)[0], 1)
    if len(encoded_data) == 2:
        return BootData(struct.unpack('<H', encoded_data)[0], 2)
    if len(encoded_data) == 4:
        return BootData(struct.unpack('<I', encoded_data)[0], 4)
    
            

def encode_instruction(opcode, op0, op1, op2):
    if op1 is None:
        assert(op2 is None)
        opfield2 = op0 >> 16
        opfield1 = (op0 >> 8) & (2**8-1)
        opfield0 = op0 & (2**8-1)
    elif op2 is None:
        opfield0 = op0
            
        opfield2 = op1 >> 8
        opfield1 = op1 & (2**8-1)
    else:
        opfield0 = op0
            
        opfield1 = op1
            
        opfield2 = op2
        
    return (opcode, opfield0, opfield1, opfield2)
        
def encode_data(data, numbytes):
    if numbytes == 1:
        return struct.pack('<B', data)
    if numbytes == 2:
        return struct.pack('<H', data)
    if numbytes == 4:
        return struct.pack('<I', data)
        
                      
                      
def parse_line(line, lineno):
    if line[0] in ' \t\n\r\f\v':
        return None
    
    elif line[0] == '.':
        if line[1] == 'd':
            # it's a data line
            m = data_line_regexp.match(line)
            if not m:
                raise SyntaxError(f"At line {lineno}: Malformed data line:\n  {line}")
            #print(f'data line re: {m.group(0)} {m.group(1)}')
            
            data = int(m.group(1), 16)
            #print(f'data line: {data}')
            if len(m.group(1)) == 2:
                return BootData(data, 1)
            elif len(m.group(1)) == 4:
                return BootData(data, 2)
            elif len(m.group(1)) == 8:
                return BootData(data, 4)
            else:
                raise SyntaxError(f"At line {lineno}: Data line data must be either 2, 4, or 8 characters:\n  {line}")
                
    else:
        # it's an instruction line
        m = instruction_line_regexp.match(line)
        if not m:
            raise SyntaxError(f"At line {lineno}: Malformed instruction line:\n  {line}")
        mnemonic = m.group(1)
        first_operand  = int(m.group(2), 16)
        try:
            opcode = opcode2mnemonic.index(mnemonic)
        except ValueError:
            raise UnknownMnemonicError(f"At line {lineno}: Unknown mnemonic:\n  {line}")
            
        if m.group(4) is None:
            assert(m.group(6) is None)
            #sys.stderr.write(f'instruction line: {mnemonic} {first_operand}\n')
            
            if num_operands(opcode) != 1:
                raise SyntaxError(f"At line {lineno}: Mnemonic {mnemonic} takes {num_operands(opcode)} operands but 1 found\n  {line}")
            if (first_operand < 0) or (first_operand > 2**21 - 1):
                raise RangeError(f"At line {lineno}: first operand is out of range (0 <= first_operand <= 1fffff):\n  {line}")

            return BootInstruction(opcode, first_operand, None, None)
            
        elif m.group(6) is None:
            second_operand = int(m.group(4), 16)
            #sys.stderr.write(f'instruction line: {mnemonic} {first_operand} {second_operand}\n')
            
            if num_operands(opcode) != 2:
                raise SyntaxError(f"At line {lineno}: Mnemonic {mnemonic} takes {num_operands(opcode)} operands but 2 found\n  {line}")
            if (first_operand < 0) or (first_operand > 2**7 - 1):
                raise RangeError(f"At line {lineno}: first operand is out of range (0 <= first_operand <= 7f):\n  {line}")
            if (second_operand < 0) or (second_operand > 2**14 - 1):
                raise RangeError(f"At line {lineno}: second operand is out of range (0 <= second_operand <= 3fff):\n  {line}")

            return BootInstruction(opcode, first_operand, second_operand, None)
        else:
            second_operand = int(m.group(4), 16)
            third_operand  = int(m.group(6), 16)
            #sys.stderr.write(f'instruction line: {mnemonic} {first_operand} {second_operand} {third_operand}\n')
            
            if num_operands(opcode) != 3:
                raise SyntaxError(f"At line {lineno}: Mnemonic {mnemonic} takes {num_operands(opcode)} operands but 3 found\n  {line}")
            if (first_operand < 0) or (first_operand > 2**7 - 1):
                raise RangeError(f"At line {lineno}: first operand is out of range (0 <= first_operand <= 7f):\n  {line}")
            if (second_operand < 0) or (second_operand > 2**7 - 1):
                raise RangeError(f"At line {lineno}: second operand is out of range (0 <= second_operand <= 7f):\n  {line}")
            if (third_operand < 0) or (third_operand > 2**7 - 1):
                raise RangeError(f"At line {lineno}: third operand is out of range (0 <= third_operand <= 7f):\n  {line}")

            return BootInstruction(opcode, first_operand, second_operand, third_operand)
            
# returns disassembled_string, num_bytes_read
# also disassembles the following immediate data payload, if there is one
def disassemble_instruction_from_file(file, pos):
    instruction_maybe_w_data, num_bytes_read = decode_instruction_from_file(file, pos)
    if instruction_maybe_w_data is None:
        return '', num_bytes_read
    disassembled_string = instruction_to_assembly(instruction_maybe_w_data.instruction, pos) + "\n"
    if instruction_maybe_w_data.data is not None:
        disassembled_string += data_to_assembly(instruction_maybe_w_data.data) + "\n"
    return disassembled_string, num_bytes_read
        

#unused
def old_disassemble_instruction_from_file(file, pos):
    
    
    disassembled_string = ''
    num_bytes_read = 0
    
    encoded_instruction = file.read(4)
    if len(encoded_instruction) <= 0:
        return disassembled_string, num_bytes_read
    
    if len(encoded_instruction) < 4:
        raise SyntaxError(f"At position {pos}: EOF in middle of instruction:\n  {encoded_instruction}")

    num_bytes_read += 4
    instr_assembly, has_data = disassemble_instruction(encoded_instruction, pos)
    disassembled_string += instr_assembly + "\n"
    
    if has_data:
        encoded_data = file.read(4)
        if len(encoded_data ) < 4:
            raise SyntaxError(f"At position {pos}: EOF in middle of instruction data:\n  {encoded_data}")
        num_bytes_read += 4
        data_assembly = disassemble_data(encoded_data)
        disassembled_string += data_assembly + "\n"

    return disassembled_string, num_bytes_read


def disassemble_instructions_from_file(file, pos):
    result, total_bytes_read = '', 0

    num_bytes_read = 1
    while(num_bytes_read > 0):
        disassembled_string, num_bytes_read = disassemble_instruction_from_file(file, pos)
        result += disassembled_string
        total_bytes_read += num_bytes_read
        
    return result, total_bytes_read
        
        
    

    

# returns (BootInstructionMaybeWithData|None, num_bytes_read)
# also decodes the following immediate data payload, if there is one
def decode_instruction_from_file(file, pos):
    encoded_instruction = file.read(4)
    if len(encoded_instruction) <= 0:
        return None, 0
    
    if len(encoded_instruction) < 4:
        raise SyntaxError(f"At position {pos}: EOF in middle of instruction:\n  {encoded_instruction}")

    num_bytes_read = 4

    instruction = decode_instruction(encoded_instruction)
    result = BootInstructionMaybeWithData(instruction=instruction, data=None)
    
    if instruction.has_data():
        encoded_data = file.read(4)
        if len(encoded_data ) < 4:
            raise SyntaxError(f"At position {pos}: EOF in middle of instruction data:\n  {encoded_data}")
        num_bytes_read += 4
        result.data = decode_data(encoded_data)
        
    return result, num_bytes_read
