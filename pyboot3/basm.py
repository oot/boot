#!/bin/python3

import pyboot
import argparse, sys

parser = argparse.ArgumentParser(description='Assemble Boot code')
args = parser.parse_args()

result = bytearray(b'')
for lineno, line in enumerate(sys.stdin.readlines()):
    if len(line) == 0:
        continue
    #print(repr(pyboot.assemble_line(line, lineno)))
    
    result.extend(pyboot.assemble_line(line, lineno))
        
        
#print(result)
sys.stdout.buffer.write(result)
