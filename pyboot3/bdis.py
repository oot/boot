#!/bin/python3

import pyboot
import argparse, sys

parser = argparse.ArgumentParser(description='Disassemble Boot code')
args = parser.parse_args()

pos = 0
disassembled_string, num_bytes_read = pyboot.disassemble_instructions_from_file(sys.stdin.buffer, pos)
print(disassembled_string)
pos += num_bytes_read
