import sys
import enum

from constants import *
import encoding
#import encoding_compact

class BootRuntimeError(Exception):
    pass
class BootStackUnderflowError(BootRuntimeError):
    pass
class BootStackOverflowError(BootRuntimeError):
    pass
class BootUnreachableInstructionError(BootRuntimeError):
    pass
class BootIllegalMemoryAccessError(BootRuntimeError):
    pass
class BootIllegalMemoryAccessPCError(BootIllegalMemoryAccessError):
    pass
class BootIllegalMemoryAccessPCLessThanZeroError(BootIllegalMemoryAccessPCError):
    pass
class BootIllegalMemoryAccessPCGreaterThanMemorySizeError(BootIllegalMemoryAccessPCError):
    pass
class BootNonexistentPointerConstantError(BootRuntimeError):
    pass
class BootArgumentError(BootRuntimeError):
    pass
class BootProgramLayoutError(BootRuntimeError):
    pass


# todo: if main program, execute cmdline argument file    
# todo: facility to 'call in' from Python
# todo: calling convention for cmdline argument strings    
# todo: jump table should now be in terms of offsets

class BootVmRegisters():
    def __init__(self, stack):
        self.stack = stack
        self.regs = [0] * 6  # register 0, then regs 3-7
        
    def __getitem__(self, key):
        if key == 0:
            return self.regs[0]
        elif key == 1:
            return self.stack.pop()
        elif key == 2:
            return self.stack[0]
        else:
            return self.regs[key - 2]

    def __setitem__(self, key, value):
        if key == 0:
            return
        elif key == 1:
            self.stack.push(value)
        elif key == 2:
            self.stack[0] = value
        else:
            self.regs[key - 2] = value
            
    def set_zeroreg(self, value):
        self.regs[0] = value

    def __str__(self):
        return str(self.regs)

class RingBufferStack():
    def __init__(self, size, check_underflow_overflow=False):
        self.size = size
        self.stk  = [0] * size
        self.pos  = 0
        self.check_underflow_overflow = check_underflow_overflow
        
    def __getitem__(self, key):
        # note: underflow not possible; can get/set beyond the 'bottom' of the stack
        return self.stk[(self.pos - key) % self.size]

    def __setitem__(self, key, value):
        # note: underflow not possible; can get/set beyond the 'bottom' of the stack
        self.stk[(self.pos - key) % self.size] = value

    def push(self, value):
        if self.check_underflow_overflow:
            if self.pos + 1 >= self.size:
                raise BootStackOverflowError()
        
        self.pos = (self.pos + 1) % self.size
        self.stk[self.pos] = value
        
    def pop(self):
        if self.check_underflow_overflow:
            if self.pos - 1 < 0:
                raise BootStackUnderflowError()
        
        value = self.stk[self.pos]
        self.pos = (self.pos - 1) % self.size
        return value

    def __repr__(self):
        return repr((self.size, self.check_underflow_overflow, self.pos, self.stk))

    def __str__(self):
        return repr((self.pos, self.stk))

    
class BootVm():
    """
    A single memory location holds an i32 or a ptr.
    Both i32s and ptrs are internally stored as unsigned integers.

    note that b/c Boot uses twos complement arithmetic,
    even though we are doing add/sub/mul operations on the
    unsigned translations of the signed integers
    (according to Boot semantics, actually the integers
    in the registers and on the stack are signed),
    the result is the same
    """

    def __init__(self, program = None, memory = None, code_ptr = None, global_page_ptr = None, decoder=encoding.decode_boot_instruction):
        # argument checking:
        if not (
                ((program is not None) and ((memory is None) and (code_ptr is None) and (global_page_ptr is None)))
                or 
                ((program is None) and ((memory is not None) and (code_ptr is not None) and (global_page_ptr is not None)))
                ):
            raise BootArgumentError('BootVm() must be given either (1) a program argument, or (2) a memory, a code_ptr, and a global_page_ptr argument (but not both)')
        # end argument checking

        self.decoder = decoder
        
        if program is not None:
            memory = ([0] * 16) + program
            code_ptr = 16
            global_page_ptr = 0
            
        self.mem = memory
        self.global_page_ptr = global_page_ptr
        self.ptrconstants = self._read_ptrconstants(memory = memory, code_ptr = code_ptr, global_page_ptr = global_page_ptr)

        self.PTR_SIZE = 1
        self.I32_SIZE = 1
        self.I16_SIZE = 1
        self.INSTRUCTION_SIZE = 1
        
        self.stki = RingBufferStack(16, check_underflow_overflow=True)  # intstack
        self.stkp = RingBufferStack(16, check_underflow_overflow=True)  # ptrstack
    
        self.regi = BootVmRegisters(self.stki) # int registers
        self.regp = BootVmRegisters(self.stkp) # ptr registers

        self.regp.set_zeroreg(self.global_page_ptr)
        
        self.pc = 0

        self.malloc_blocks_in_use = {}
        self.malloc_blocks_free   = []
        self.callables_from_boot  = []

    def _read_ptrconstants(self, memory, code_ptr, global_page_ptr):
        # argument checking:
        # code_ptr points to constant table
        if (code_ptr is not None) and (code_ptr < 0):
            raise BootArgumentError('BootVm() given negative code pointer')
        if (code_ptr is not None) and (code_ptr > len(memory)-1):
            raise BootArgumentError('BootVm() given code pointer past the end of memory')
        # end argument checking

        ptrconstants_sz = signed_to_unsigned(memory[code_ptr], 16)
        print(ptrconstants_sz )
        if code_ptr + ptrconstants_sz > len(memory) - 1:
            if ptrconstants_sz > len(program) - 1:
                raise BootProgramLayoutError('Constant table size exceeds provided memory')

        ptr_constant_table_in_program = memory[code_ptr:(code_ptr + ptrconstants_sz + 1)]
        program_entry_point = code_ptr + ptrconstants_sz + 1
            
        ptrconstants = [global_page_ptr, program_entry_point] + ptr_constant_table_in_program
        return ptrconstants
    
    def execute(self, address = None):

        if address is not None:
            self.pc = address
        else:
            print(self.ptrconstants)
            print(self.mem)
            self.pc = self.ptrconstants[1] # program entry point constant

        while (1):
            if self.pc < 0:
                raise BootIllegalMemoryAccessPCLessThanZeroError(f"PC < 0: {self.pc}")
            if int(self.pc) > len(self.mem)-1:
                raise BootIllegalMemoryAccessPCGreaterThanMemorySizeError(f"PC > size of memory-1: PC = {self.pc}, len(mem) = {len(self.mem)}")
            
            instruction_encoded = self.mem[int(self.pc)]
            self.pc += 1

            instruction = self.decoder(instruction_encoded)
            
            #print(self.mem)
            #print(instruction)
            #print()

            instruction_id, op0, op1, op2 = instruction


            if (instruction_id == Instruction_id.bad0) or (instruction_id == Instruction_id.bad1):
                raise(BootUnreachableInstructionError)
            
            
            elif instruction_id == Instruction_id.add:
                self.regi[op0] = (self.regi[op1] + self.regi[op2]) % 2**32
                
            elif instruction_id == Instruction_id.addi:
                #print(instruction)
                #print(unsigned_to_signed(op2,3))
                self.regi[op0] = (self.regi[op1] + unsigned_to_signed(op2,3)) % 2**32
               
            elif instruction_id == Instruction_id['and']: # 'and' is a reserved word in Python
                self.regi[op0] = self.regi[op1] & self.regi[op2]
                
            elif instruction_id == Instruction_id.ann:
                pass

            elif instruction_id == Instruction_id.ap:
                self.regp[op0] = unsigned_to_signed(self.regi[op1],32) + self.regp[op2]

            elif instruction_id == Instruction_id.appi:
                self.regp[op0] = self.regp[op1] + unsigned_to_signed(op2,3)*self.PTR_SIZE

            elif instruction_id == Instruction_id.apwi:
                self.regp[op0] = self.regp[op1] + unsigned_to_signed(op2,3)*self.I32_SIZE
 
            #elif instruction_id == Instruction_id.bad:
            #    # already took care of this above (first; out of alphabetical order)
            #    pass
            
            elif instruction_id == Instruction_id.beq:
                if self.regi[op0] == self.regi[op1]:
                    self.pc += int(unsigned_to_signed(op2,3)*self.INSTRUCTION_SIZE/2)
            
            elif instruction_id == Instruction_id.beqp:
                if self.regp[op0] == self.regp[op1]:
                    self.pc += int(unsigned_to_signed(op2,3)*self.INSTRUCTION_SIZE/2)
            
            elif instruction_id == Instruction_id.blt:
                if self.regi[op0] < self.regi[op1]:
                    self.pc += int(unsigned_to_signed(op2,3)*self.INSTRUCTION_SIZE/2)
            
            elif instruction_id == Instruction_id.bne:
                if self.regi[op0] == self.regi[op1]:
                    self.pc += int(unsigned_to_signed(op2,3)*self.INSTRUCTION_SIZE/2)
            
            elif instruction_id == Instruction_id['break']: # 'break' is a reserved word in Python
                print('-----------')
                print(f'breakpoint at PC={self.pc}')
                print(self.regi)
                print(self.stki)
                print(self.regp)
                print(self.stkp)
                print()
                print(self.mem)
                print('-----------')
                print()
                print()
                print()

            elif instruction_id == Instruction_id.cas:
                # note: CAS must be atomic
                if self.mem[self.regi[op0]] == self.regi[op2]:
                   self.mem[self.regi[op0]] = self.regi[op1]
                   self.stki.push(-1)
                else:
                   self.stki.push(1) 
            
            elif instruction_id == Instruction_id.cp:
                self.regi[op0] = self.regi[op1]
            
            elif instruction_id == Instruction_id.cpp:
                self.regp[op0] = self.regp[op1]
            
            elif instruction_id == Instruction_id.devop:
                pass # devop is implementation-defined and we choose not to define any behavior

            elif instruction_id == Instruction_id.fence:
                # no-op because neither
                # instructions nor memory accesses
                # are reordered in this implementation
                pass

            elif instruction_id == Instruction_id.halt:
                return self.regi[3]
            
            elif instruction_id == Instruction_id['in']:  # 'in' is a reserved word in Python
                if self.regi[op1] == 0:
                    self.regi[op0] = getch()

            elif instruction_id == Instruction_id.inp:
                pass # for now let's just say that console doesn't support read/write of pointers
            
            elif instruction_id == Instruction_id.jy:
                self.pc = self.regp[op0]

            elif instruction_id == Instruction_id.jk: #I11
                if op1 > len(self.ptrconstants-1):
                    raise BootNonexistentPointerConstantError(f'Pointer constant does not exist at PC={self.PC}: {op1}')
                self.pc = self.ptrconstants[op1]
            
            elif instruction_id == Instruction_id.jpc: # I11
                self.pc += unsigned_to_signed(op0, 11)
                self.regp.set_zeroreg(self.pc)
            
            elif instruction_id == Instruction_id.lb:
                value_unsigned_8_bits = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)] % 256
                # sign-extend from 8-bits to 32-bits
                self.regi[op0] = signed_to_unsigned(unsigned_to_signed(value_unsigned_8_bits,8), 32)
                
            elif instruction_id == Instruction_id.lbu:
                self.regi[op0] = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)] % 256
            
            elif instruction_id == Instruction_id.lh:
                value_unsigned_16_bits = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)] % 2**16
                # sign-extend from 16-bits to 32-bits
                self.regi[op0] = signed_to_unsigned(unsigned_to_signed(value_unsigned_16_bits,16), 32)
            
            elif instruction_id == Instruction_id.lhu:
                self.regi[op0] = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)] % 2**16
            
            elif instruction_id == Instruction_id.li: #I8
                self.regi[op0] = op1

            elif instruction_id == Instruction_id.lk:
                k = self.regi[op1]
                if k > len(self.ptrconstants-1):
                    raise BootNonexistentPointerConstantError(f'Pointer constant does not exist at PC={self.PC}: {k}')
                self.regi[op0] = self.ptrconstants[k]
            
            elif instruction_id == Instruction_id.lp:
                self.regp[op0] = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)]

            elif instruction_id == Instruction_id.lpc:
                self.regp[op0] = self.pc

            elif instruction_id == Instruction_id.lsi:
                self.regi[op0] = self.stki[op1]
                
            elif instruction_id == Instruction_id.lsp:
                self.regp[op0] = self.stkp[op1]
                
            elif instruction_id == Instruction_id.ssi:
                self.stki[op0] = self.regi[op1]
                
            elif instruction_id == Instruction_id.ssp:
                self.stkp[op0] = self.regp[op1]
                
            elif instruction_id == Instruction_id.lw:
                self.regi[op0] = self.mem[self.regp[op1] + unsigned_to_signed(op2, 3)] % 2**32

            elif instruction_id == Instruction_id.malloc:
                requested_size = self.regi[op1]
                
                found = False
                for (block_size, block_address) in self.malloc_blocks_free:
                    if block_size >= requested_size:
                        new_block_address = block_address
                        if block_size > requested_size:
                            remaining_size = block_size - requested_size
                            remaining_address = new_block_address + requested_size
                            self.malloc_blocks_free.append((remaining_size, remaining_address))
                        found = True
                if not found:
                    new_block_address = len(self.mem)
                    self.mem.extend([0] * requested_size)
                    
                self.malloc_blocks_in_use[new_block_address] = requested_size
                self.regi[op0] = new_block_address
                
            elif instruction_id == Instruction_id.mfree:
                block_address = self.regi[op0]
                block_size = self.malloc_blocks_in_use[block_address]
                del self.malloc_blocks_in_use[block_address]
                self.malloc_blocks_free.append((block_size, block_address))
                
            elif instruction_id == Instruction_id.mul:
                self.regi[op0] = (self.regi[op1] * self.regi[op2]) % 2**32
            
            elif instruction_id == Instruction_id['or']:  # 'or' is a reserved word in Python
                self.regi[op0] = self.regi[op1] | self.regi[op2]

            elif instruction_id == Instruction_id.out:
                if self.regi[op1] == 0:
                    sys.stdout.write(self.regi[op0])
                    
            elif instruction_id == Instruction_id.outp:
                pass # for now let's just say that console doesn't support read/write of pointers
                
            elif instruction_id == Instruction_id.sai: #I8
                 self.regi[op0] = (self.regi[op0] << 8) + op1

            elif instruction_id == Instruction_id.sb:
                self.mem[self.regp[op0]+self.regi[op2]]= self.regi[op1]
                
            elif instruction_id == Instruction_id.sh:
                self.mem[self.regp[op0]+self.regi[op2]]= self.regi[op1]
            
            elif instruction_id == Instruction_id.sll:
                self.regi[op0] = (self.regi[op2] << self.regi[op1]) % 2**32
                
            elif instruction_id == Instruction_id.sp:
                self.mem[self.regp[op0]+self.regi[op2]]= self.regp[op1]

            elif instruction_id == Instruction_id.srs:
                sru_result = self.regi[op2] >> self.regi[op1]
                if self.regi[op1] >= 0:
                    self.regi[op0] = sru_result
                else:
                    self.regi[op0] = -sru_result

            elif instruction_id == Instruction_id.sru:
                self.regi[op0] = self.regi[op2] >> self.regi[op1]
                
            elif instruction_id == Instruction_id.sub:
                self.regi[op0] = (self.regi[op1] - self.regi[op2]) % 2**32
            
            elif instruction_id == Instruction_id.sw:
                self.mem[self.regp[op0]+self.regi[op2]]= self.regi[op1]

            elif instruction_id == Instruction_id.swapsi:
                self.stki[0], self.stki[1] = self.stki[1], self.stki[0]

            elif instruction_id == Instruction_id.swapsi:
                self.stkp[0], self.stkp[1] = self.stkp[1], self.stkp[0]
                
            elif instruction_id == Instruction_id.sysinfo:
                if op1 == 0:
                    self.regi[op0] = self.PTR_SIZE
                if op1 == 1:
                    self.regi[op0] = self.I32_SIZE
                if op1 == 2:
                    self.regi[op0] = self.I16_SIZE
                if op1 == 2:
                    self.regi[op0] = self.INSTRUCTION_SIZE

            # this is a little complicated, can we simplify?
            # maybe not; we're doing something a little inherently complicated;
            # we're dealing with a calling convention, with interop, and with
            # first-class type signatures, each of which tends to add complication
            elif instruction_id == Instruction_id.xcall:
                target_address = self.regp[op0]
                if target_address in self.callables_from_boot:
                    
                    (signature, callable_) = self.callables_from_boot[target_address]
                    return_signature, args_signature = signature

                    boot_mem_arg_ptr   = self.regp[8]

                    # if overflow arguments are present, check that boot_mem_arg_count matches our signature
                    int_arg_count = sum([1 for arg in args_signature if arg == Boot_types.int])
                    ptr_arg_count = sum([1 for arg in args_signature if arg == Boot_types.ptr])
                    if (int_arg_count > 3) or (ptr_arg_count > 3):
                        boot_mem_arg_count = self.regi[8]
                        # variadic fns not supported
                        int_arg_overflow_count = max(int_arg_count - 3, 0)
                        ptr_arg_overflow_count = max(ptr_arg_count - 3, 0)
                        if int_arg_overflow_count + ptr_arg_overflow_count != boot_mem_arg_count:
                            raise BootXcallSignatureException(f"Boot xcall doesn't match signature: PC: {self.pc}, target address: {target_address}, int_arg_overflow_count + ptr_arg_overflow_count != boot_mem_arg_count: {int_arg_overflow_count} + {ptr_arg_overflow_count} != {boot_mem_arg_count}")
                        
                    args = []; int_arg_index = 0; ptr_arg_index = 0; mem_arg_index = 0;
                    for arg_type in args_signature:
                        if arg_type == Boot_types.int:
                            if   int_arg_index == 0:
                                args.append(self.regi[5])
                            elif int_arg_index == 1:
                                args.append(self.regi[6])
                            else:
                                args.append(boot_mem_arg_ptr + mem_arg_index)
                                mem_arg_index += 1
                            int_arg_index += 1
                        elif arg_type == Boot_types.ptr:
                            if   ptr_arg_index == 0:
                                args.append(self.regp[5])
                            elif ptr_arg_index == 1:
                                args.append(self.regp[6])
                            else:
                                args.append(boot_mem_arg_ptr + mem_arg_index)
                                mem_arg_index += 1
                            ptr_arg_index += 1
                        else:
                            assert False

                    # do the actual call
                    ret = callable_(*args)
                    
                    if return_signature == Boot_types.int:
                        self.regi[5] = ret
                    elif return_signature == Boot_types.ptr:
                        self.regp[5] = ret
                    else:
                        assert False
                        
            
            elif instruction_id == Instruction_id.xentry:
                pass # when calling in, we setup the Boot calling convention on the Python side, so no need to do anything here
            
            elif instruction_id == Instruction_id.xpostcall:
                pass # when calling out, we setup the Boot calling convention on the Python side, so no need to do anything here
            elif instruction_id == Instruction_id.xret:
                # ignore return_address operand and return to caller
                return (self.regi[5], self.regp[5])
                    
            elif instruction_id == Instruction_id.xor:
                self.regi[op0] = self.regi[op1] ^ self.regi[op2]

            
        
        
        
class _Getch:
    """Gets a single character from standard input.  Does not echo to the
screen. PSF (Python Software Foundation) license. From https://github.com/ActiveState/code/blob/master/recipes/Python/134892_getchlike_unbuffered_character_reading_stdboth/recipe-134892.py"""
    def __init__(self):
        try:
            self.impl = _GetchWindows()
        except ImportError:
            self.impl = _GetchUnix()

    def __call__(self): return self.impl()


class _GetchUnix:
    def __init__(self):
        import tty, sys

    def __call__(self):
        import sys, tty, termios
        fd = sys.stdin.fileno()
        old_settings = termios.tcgetattr(fd)
        try:
            tty.setraw(sys.stdin.fileno())
            ch = sys.stdin.read(1)
        finally:
            termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
        return ch


class _GetchWindows:
    def __init__(self):
        import msvcrt

    def __call__(self):
        import msvcrt
        return msvcrt.getch()


getch = _Getch()
