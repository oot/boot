import pytest

from constants import *;
import encoding_compact;
encoding = encoding_compact

def test_ann():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.ann, op0=5,op1=1,op2=6))
        ) ==
            Boot_instruction(Instruction_id.ann, op0=5,op1=1,op2=6)
        )
            
def test_beq():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.beq, op0=5,op1=0,op2=4))
        ) ==
            Boot_instruction(Instruction_id.beq, op0=5,op1=0,op2=4)
        )

def test_beq_2():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.beq, op0=5,op1=0,op2=-6))
        ) ==
            Boot_instruction(Instruction_id.beq, op0=5,op1=0,op2=-6)
        )

def test_jpc():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.jpc, op0=-1024))
        ) ==
            Boot_instruction(instruction_id=Instruction_id.jpc, op0=-1024, op1=None, op2=None)
        )

def test_jpc_2():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.jpc, op0=1023))
        ) ==
            Boot_instruction(Instruction_id.jpc, op0=1023)
        )

def test_jpc_zero():
    assert (
        encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.jpc, op0=0)
        ) ==
            0
        )
    
# note this one is different; with encoding_compact, None is distinguished from 0
def test_sysinfo():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.sysinfo, op0=5,op1=1))
        ) ==
            Boot_instruction(Instruction_id.sysinfo, op0=5,op1=1)
        )

def test_sru():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.sru, op0=5,op1=1,op2=7))
        ) ==
            Boot_instruction(Instruction_id.sru, op0=5,op1=1,op2=7)
        )

# note this one is different; with encoding_compact, None is distinguished from 0
def test_halt():
    assert (
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(
            Boot_instruction(Instruction_id.halt, op0=5))
        ) ==
            Boot_instruction(Instruction_id.halt, op0=5)
        )

def test_beq_offset_0_err():
    with pytest.raises(BootOperandOutOfRange):
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(Boot_instruction(Instruction_id.beq, op0=5,op1=0,op2=0)))

def test_beq_offset_8_err():
    with pytest.raises(BootOperandOutOfRange):
        encoding.decode_boot_instruction(encoding.encode_boot_instruction(Boot_instruction(Instruction_id.beq, op0=5,op1=8,op2=4)))
    
