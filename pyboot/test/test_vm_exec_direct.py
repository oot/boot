import pytest

from constants import *;
import encoding;
import vm;

def test_ann_halt():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.ann, 5,1,6), 
        Boot_instruction(Instruction_id.halt, 2)     # halt $t
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 0
    
def test_li():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 2, 3),   # li $2 #3
        Boot_instruction(Instruction_id.ann, 5,1,6), 
        Boot_instruction(Instruction_id.halt, 2)     # halt $t
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 3

def test_break_halt():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id['break']), 
        Boot_instruction(Instruction_id.halt, 0),    # halt $0
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 0

def test_halt():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 0, 0),   # li $0 #0
        Boot_instruction(Instruction_id.halt, 0),    # halt #0
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 0

def test_break():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 3, 5),   # li $3 #5
        Boot_instruction(Instruction_id['break']), 
        Boot_instruction(Instruction_id.halt, 3),    # halt $3
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 5

def test_stki():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),   # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),   # li $s #6
        Boot_instruction(Instruction_id.halt, 1),    # halt $s
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6

def test_stki_cp():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),  # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),  # li $s #6
        Boot_instruction(Instruction_id.cp, 3, 1),  # li $3 $s
        Boot_instruction(Instruction_id.halt, 1),   # halt $1
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 5

def test_cp():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),  # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),  # li $s #6
        Boot_instruction(Instruction_id.cp, 3, 1),  # li $3 $s
        Boot_instruction(Instruction_id.halt, 3),   # halt $3
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6

def test_add():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),     # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),     # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),     # li $s #3
        Boot_instruction(Instruction_id.add, 1, 1, 1), # add $s $s $s
        Boot_instruction(Instruction_id.halt, 1),      # halt $s
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 9

def test_ap():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),     # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),     # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),     # li $s #3
        Boot_instruction(Instruction_id.ap, 1, 1, 0),  # ap &s $s &0
        Boot_instruction(Instruction_id.halt, 1),      # halt $s
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6
    assert avm.stkp[0] == avm.ptrconstants[0] + 3

def test_addi():
    avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),      # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),      # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),      # li $s #3
        Boot_instruction(Instruction_id.addi, 1, 1, 6), # addi $s $s #-2
        Boot_instruction(Instruction_id.halt, 1),       # halt $s
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 1

def test_memory_layout_constanttable():
    avm = vm.BootVm([2,1,4] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),      # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),      # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),      # li $s #3
        Boot_instruction(Instruction_id.ap, 1, 1, 0),   # ap &s $s &0
        Boot_instruction(Instruction_id.halt, 1),
    ]], decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6
    assert avm.stkp[0] == avm.ptrconstants[0] + 3

def test_memory_layout_globalpage_then_prog():
    avm = vm.BootVm(memory=([0] * 16) + [2,1,4] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),     # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),     # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),     # li $s #3
        Boot_instruction(Instruction_id.ap, 1, 1, 0),  # ap &s $s &0
        Boot_instruction(Instruction_id.halt, 1),      # halt $s
    ]], code_ptr = 16, global_page_ptr = 0, decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6
    assert avm.stkp[0] == avm.ptrconstants[0] + 3

def test_memory_layout_data_then_prog_then_data_then_page_then_data():
    avm = vm.BootVm(memory=[33, 99] + [2,1,4] + [encoding.encode_boot_instruction(instr) for instr in [
        Boot_instruction(Instruction_id.li, 1, 5),     # li $s #5
        Boot_instruction(Instruction_id.li, 1, 6),     # li $s #6
        Boot_instruction(Instruction_id.li, 1, 3),     # li $s #3
        Boot_instruction(Instruction_id.ap, 1, 1, 0),  # ap &s $s &0
        Boot_instruction(Instruction_id.halt, 1),      # halt $s
    ]] + [22] + ([0] * 16) + [77], code_ptr = 2, global_page_ptr = 11, decoder=encoding.decode_boot_instruction);
    assert avm.execute() == 6
    assert avm.stkp[0] == avm.ptrconstants[0] + 3

def test_pc_greater_than_mem_size_err():
    with pytest.raises(vm.BootIllegalMemoryAccessPCGreaterThanMemorySizeError):
        avm = vm.BootVm([0] + [encoding.encode_boot_instruction(instr) for instr in [
            Boot_instruction(Instruction_id.ann, 5,1,6),
            # no halt, so execution continues off the edge of memory
        ]], decoder=encoding.decode_boot_instruction);
        avm.execute()        
    
