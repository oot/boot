import enum
from constants import *

# this is the optional 16-bit encoding
# encoding.py contains the standard 32-bit encoding
# this 16-bit encoding is half the size, but it's more complicated

class Instruction_encoding_format(enum.Enum):
    p3 = enum.auto()
    p2 = enum.auto()
    p1 = enum.auto()
    p0 = enum.auto()
    i11 = enum.auto()
    i8 = enum.auto()
    r2 = enum.auto()
    r1 = enum.auto()
    r0 = enum.auto()

p3_instruction_ids  = [Instruction_id[name] for name in p3_instructions]
p2_instruction_ids  = [Instruction_id[name] for name in p2_instructions]
p1_instruction_ids  = [Instruction_id[name] for name in p1_instructions]
p0_instruction_ids  = [Instruction_id[name] for name in p0_instructions]
i8_instruction_ids  = [Instruction_id[name] for name in i8_instructions]
i11_instruction_ids  = [Instruction_id[name] for name in i11_instructions]
r2_instruction_ids  = [Instruction_id[name] for name in r2_instructions]
r1_instruction_ids  = [Instruction_id[name] for name in r1_instructions]
r0_instruction_ids  = [Instruction_id[name] for name in r0_instructions]
alias_instruction_ids = [Instruction_id[name] for name in alias_instructions]


p3_instruction_id_to_opcode = dict(zip(p3_instruction_ids, range(len(p3_instruction_ids))))
p3_opcode_to_instruction_id = dict(zip(range(len(p3_instruction_ids)), p3_instruction_ids))

p2_instruction_id_to_opcode = dict(zip(p2_instruction_ids, range(len(p2_instruction_ids))))
p2_opcode_to_instruction_id = dict(zip(range(len(p2_instruction_ids)), p2_instruction_ids))

p1_instruction_id_to_opcode = dict(zip(p1_instruction_ids, range(len(p1_instruction_ids))))
p1_opcode_to_instruction_id = dict(zip(range(len(p1_instruction_ids)), p1_instruction_ids))

p0_instruction_id_to_opcode = dict(zip(p0_instruction_ids, range(len(p0_instruction_ids))))
p0_opcode_to_instruction_id = dict(zip(range(len(p0_instruction_ids)), p0_instruction_ids))

i8_instruction_id_to_opcode = dict(zip(i8_instruction_ids, range(len(i8_instruction_ids))))
i8_opcode_to_instruction_id = dict(zip(range(len(i8_instruction_ids)), i8_instruction_ids))

i11_instruction_id_to_opcode = dict(zip(i11_instruction_ids, range(len(i11_instruction_ids))))
i11_opcode_to_instruction_id = dict(zip(range(len(i11_instruction_ids)), i11_instruction_ids))

r2_instruction_id_to_opcode = dict(zip(r2_instruction_ids, range(len(r2_instruction_ids))))
r2_opcode_to_instruction_id = dict(zip(range(len(r2_instruction_ids)), r2_instruction_ids))

r1_instruction_id_to_opcode = dict(zip(r1_instruction_ids, range(len(r1_instruction_ids))))
r1_opcode_to_instruction_id = dict(zip(range(len(r1_instruction_ids)), r1_instruction_ids))

r0_instruction_id_to_opcode = dict(zip(r0_instruction_ids, range(len(r0_instruction_ids))))
r0_opcode_to_instruction_id = dict(zip(range(len(r0_instruction_ids)), r0_instruction_ids))


# omit alias_instructions b/c they don't have their own opcodes

instruction_id_to_encoding_format = dict(
    list(zip(p3_instruction_ids, [Instruction_encoding_format.p3] * len(p3_instruction_ids))) +
    list(zip(p2_instruction_ids, [Instruction_encoding_format.p2] * len(p2_instruction_ids))) +
    list(zip(p1_instruction_ids, [Instruction_encoding_format.p1] * len(p1_instruction_ids))) +
    list(zip(p0_instruction_ids, [Instruction_encoding_format.p0] * len(p0_instruction_ids))) +
    list(zip(i8_instruction_ids, [Instruction_encoding_format.i8] * len(i8_instruction_ids))) +
    list(zip(i11_instruction_ids, [Instruction_encoding_format.i11] * len(i11_instruction_ids))) +
    list(zip(r2_instruction_ids, [Instruction_encoding_format.r2] * len(r2_instruction_ids))) +
    list(zip(r1_instruction_ids, [Instruction_encoding_format.r1] * len(r1_instruction_ids))) +
    list(zip(r0_instruction_ids, [Instruction_encoding_format.r0] * len(r0_instruction_ids)))
)

# omit alias_instructions





def encode_boot_instruction(instruction: Boot_instruction):
    format = instruction_id_to_encoding_format[instruction.instruction_id]


    ###
    ### encoding format R
    ###

    if format == Instruction_encoding_format.p3:
        # xxxyyyzzzooooo10

        if instruction.instruction_id in branch_instruction_ids:
            # branch instructions have a special encoding for branch offsets
            if (instruction.op2 < -6) or (instruction.op2 > 4) or (instruction.op2 == -2) or (instruction.op2 == -1) or (instruction.op2 == 0):
                raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: branch instruction op2 is {instruction.op2} which is not in the range [-6, -5, -4, -3, 1, 2, 3, 4]")
            op2 = [0, 1, 2, 3, None, None, None, 4, 5, 6, 7][instruction.op2+6]
            assert op2 is not None
            check_boot_instruction_operands(instruction, (0,7), (0,7), (-6,4))
        else:
            op2 = instruction.op2
            check_boot_instruction_operands(instruction, (0,7), (0,7), (0,7))
        return (
            instruction.op0*2**13 +
            instruction.op1*2**10 +
            op2*2**7 +
            p3_instruction_id_to_opcode[instruction.instruction_id]*2**2 +
            0b10
            )
    elif format == Instruction_encoding_format.p2:
        check_boot_instruction_operands(instruction, (0,7), (0,7), None)
        return (
            instruction.op0*2**13 +
            instruction.op1*2**10 +
            p2_instruction_id_to_opcode[instruction.instruction_id]*2**7 +
            31*2**2 +
            0b10
                )
            
    elif format == Instruction_encoding_format.p1:
        check_boot_instruction_operands(instruction, (0,7), None, None)
        return (
            instruction.op0*2**13 +
            p1_instruction_id_to_opcode[instruction.instruction_id]*2**10 +
            7*2**7 +
            31*2**2 +
            0b10
            )
    elif format == Instruction_encoding_format.p0:
        check_boot_instruction_operands(instruction, None, None, None)
        return (
            p0_instruction_id_to_opcode[instruction.instruction_id]*2**13 +
            7*2**10 +
            7*2**7 +
            31*2**2 +
            0b10
        )
           
    elif format == Instruction_encoding_format.i8:
        # xxxyyyyyyyyoo000, 8-bit signed immediate op1
        check_boot_instruction_operands(instruction, (0,7), (-2**7, 2**7-1), None)
        return (
            instruction.op0*2**13 +
            signed_to_unsigned(instruction.op1,8)*2**5 +
            i8_instruction_id_to_opcode[instruction.instruction_id]*2**4 +
            0b1000
            )
    elif format == Instruction_encoding_format.i11:
        # xxxxxxxxxxxoo000, 11-bit signed immediate op0
        check_boot_instruction_operands(instruction, (-2**10, 2**10-1), None, None)
        return (
            signed_to_unsigned(instruction.op0,11)*2**5 +
            i11_instruction_id_to_opcode[instruction.instruction_id]*2**4 +
            0b0000
            )
    elif format == Instruction_encoding_format.r2:
        check_boot_instruction_operands(instruction, (0,15), (0,15))
        return (
            instruction.op0*2**12 +
            instruction.op1*2**8 +
            r2_instruction_id_to_opcode[instruction.instruction_id]*2**3 +
            0b100
        )
    elif format == Instruction_encoding_format.r1:
        check_boot_instruction_operands(instruction, (0,15))
        return (
            instruction.op0*2**12 +
            r1_instruction_id_to_opcode[instruction.instruction_id]*2**8 +
            31*2**3 +
            0b100
        )
    elif format == Instruction_encoding_format.r0:
        check_boot_instruction_operands(instruction, None, None, None)
        return (
            r0_instruction_id_to_opcode[instruction.instruction_id]*2**12 +
            7*2**8 +
            31*2**3 +
            0b100
        )
        
    else:
        assert(False), 'Unknown result from instruction_id_to_encoding_format'



def decode_boot_instruction(x: int): # x is a 32-bit unsigned integer
    # all Boot instructions are of the form:
    # 0b???????????????0

    # to distinguish between R and I, we
    # look at the secondmost least-significant-bit:
    
    # R-format:   xxxyyyzzzooooo10
    # I-format:   xxxyyyzzziiooo00

    ###
    ### encoding formats P
    ### 
    if x & 3 == 0b10: # if the bit pattern ends in 10
        # P-format:  xxxyyyzzzooooo10
        
        x_shifted = (x >> 2)        # consume two least-significant bits, which
                                    # are constants in this format
            
        p3_opcode = x_shifted & 31  # 5 bits, so AND with 2^5 - 1 = 31
        x_shifted = x_shifted >> 5  # consume those 5 bits

        instruction_id = p3_opcode_to_instruction_id[p3_opcode]
        
        op2 = x_shifted & 7
        x_shifted = x_shifted >> 3
            
        op1 = x_shifted & 7
        x_shifted = x_shifted >> 3
            
        op0 = x_shifted

        #print('HERE');   print(instruction_id);   print('----')
        #print('op2');   print(op2);   print('----')
        
        if instruction_id == Instruction_id.RESERVED_PTWO:
            instruction_id = p2_opcode_to_instruction_id[op2]
            op2 = None
            #print('HERE2');   print(instruction_id);   print('----')
            
            if instruction_id == Instruction_id.RESERVED_PONE:
                
                instruction_id = p1_opcode_to_instruction_id[op1]
                op1 = None
                if instruction_id == Instruction_id.RESERVED_PZERO:
                    instruction_id = p0_opcode_to_instruction_id[op0]
                    op0 = None
        elif instruction_id in branch_instruction_ids:
            # branch instructions have a special encoding for branch offsets
            # we're in P3 again btw
            op2 = [-6, -5, -4, -3, 1, 2, 3, 4][op2]
        
        return Boot_instruction(instruction_id, op0, op1, op2)
    
    ###
    ### encoding formats R
    ###
    if x & 7 == 0b100: # if the bit pattern ends in 100
        # R-format: xxxxzzzzooooo100
        x_shifted = (x >> 3)        # consume three least-significant bits, which
                                    # are constants in this format
                                    
        r2_opcode = x_shifted & 31  # 5 bits, so AND with 2^5 - 1 = 31
        x_shifted = x_shifted >> 5  # consume those 5 bits

        instruction_id = r2_opcode_to_instruction_id[r2_opcode]
        
        op1 = x_shifted & 15
        x_shifted = x_shifted >> 4
            
        op0 = x_shifted

        #print('HERE');   print(instruction_id);   print('----')
        #print('op2');   print(op2);   print('----')
        
        if instruction_id == Instruction_id.rone:
            instruction_id = r1_opcode_to_instruction_id[op1]
            op1 = None
            #print('HERE2');   print(instruction_id);   print('----')
            if instruction_id == Instruction_id.rzero:
                instruction_id = r0_opcode_to_instruction_id[op0]
                op0 = None
        
        return Boot_instruction(instruction_id, op0, op1)

    ###
    ### encoding formats I
    ### 
    else: # x & 7 == 0
        # I-format:   xxxyyyzzziioo000
        #print(x)
        
        x_shifted = (x >> 3)        # consume three least-significant bits, which
                                    # are constants in this format


        if x == 0:
            # the all-zero-bit instruction is illegal/unreachable
            return Boot_instruction(Instruction_id.bad)

        if x_shifted & 1 == 1:
            # I8 format: xxxiiiiiiiio1000
            x_shifted = x_shifted >> 1
            
            i8_opcode = x_shifted & 1
            x_shifted = x_shifted >> 1

            instruction_id = i8_opcode_to_instruction_id[i8_opcode]
            
            op1 = unsigned_to_signed(x_shifted & 255, 8)
            x_shifted = x_shifted >> 8
            
            op0 = x_shifted
            return Boot_instruction(instruction_id, op0, op1, None)
        else: # x & 1 = 0
            # I11 format: iiiiiiiiiiio0000
            x_shifted = x_shifted >> 1
            
            i11_opcode = x_shifted & 1
            x_shifted = x_shifted >> 1

            instruction_id = i11_opcode_to_instruction_id[i11_opcode]
            
            return Boot_instruction(instruction_id, unsigned_to_signed(x_shifted, 11), None, None)
            
             
    

def check_boot_instruction_operands(instruction: Boot_instruction, op0_range_inclusive=None, op1_range_inclusive=None, op2_range_inclusive=None):
    check_boot_instruction_operand(instruction, 'op0', instruction.op0, op0_range_inclusive)
    check_boot_instruction_operand(instruction, 'op1', instruction.op1, op1_range_inclusive)
    check_boot_instruction_operand(instruction, 'op2', instruction.op2, op2_range_inclusive)
    
def check_boot_instruction_operand(instruction: Boot_instruction, operand_name, operand_value, operand_range_inclusive):
    if operand_range_inclusive is None:
        if operand_value is not None:
            raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: instruction.{operand_name} should be None but is {operand_value}")
    else:
        if (operand_value is None) or (operand_value < operand_range_inclusive[0]) or (operand_value > operand_range_inclusive[1]):
            raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: instruction.{operand_name} is {operand_value} which is not in the range [{operand_range_inclusive[0]}, {operand_range_inclusive[1]}]")

