import enum
from collections import namedtuple

    
Boot_types = enum.Enum('Boot_types', [
    'int',
    'ptr'
    ], module=__name__)



Boot_instruction = namedtuple('Boot_instruction', ['instruction_id', 'op0', 'op1', 'op2'] )
Boot_instruction.__new__.__defaults__ = (None, None, None)

class BootEncodingException(Exception):
    pass

class BootOperandOutOfRange(BootEncodingException):
    pass


# note: The 32-bit encoding doesn't care about P3/P2/P1/P0/R2/R1/R0,
#       these are just being used to build the instruction_mnemonics list below.
# 
#       The encoding only knows about i11, i8, the branch instructions, and
#       that the first 64 opcodes can only contain operands whose values are 0 thru 7.
#
#       I just like to group them like this for my own 
# 1-operand, 11-bit immediates ("I11")
i11_instructions = [
    'jrel',   # bit pattern must be all 0s so that the 'bad0' alias refers to something illegal (used to be called jpc)
    'jk',
]

# 2-operand, 8-bit immediates ("I8"); the register operand can only be one of the first 8 (integer) registers
i8_instructions = [
    'li',
    'sai',
]

# 3-operand instructions that can only take as operands the first 8 registers in each bank
p3_instructions = [
    'add',
    'addi',   # add constant to integer
    'and', 
    'ann',
    'ap',
    'appi',   # add (constant * INT_SZ) to pointer
    'apwi',   # add (constant * PTR_SZ) to pointer
    'casrmw',    # cas integer atomic, sequential consistency (AMO/RMW)
    'casrmwrc',  # cas integer atomic, release consistency (both acquire and release) (AMO/RMW)
    'casrmwrlx', # cas integer atomic, relaxed (AMO/RMW)
    'casprmw',    # cas pointer atomic, sequential consistency (AMO/RMW)
    'casprmwrc',  # cas pointer atomic, release consistency (both acquire and release) (AMO/RMW)
    'casprmwrlx', # cas pointer atomic, relaxed (AMO/RMW)
    
    'malloc_shared',
    'mul', 
    'or', 
    'sll',
    'srs', 
    'sru', 
    'sub',
    'xor',
    
    'addrmw', # add atomic, sequential consistency (AMO/RMW)   # todo mb remove?
    'aprmw',  # ap  atomic, sequential consistency (AMO/RMW)   # todo mb remove?
    'andrmw', # and atomic, sequential consistency (AMO/RMW)   ## todo mb remove?
    'orrmw',  # or  atomic, sequential consistency (AMO/RMW)   ## todo mb remove?
    'xorrmw', # xor atomic, sequential consistency (AMO/RMW)   ## todo mb remove?
    
    'RESERVED_P0_24', 
    'RESERVED_P0_25', 
    'RESERVED_P0_26', 
           
           # put the branches together so that the decoder can check for a simple
           # opcode range to decide if it needs to apply the special branch offset
           # interpretation to the op2 immediate
    'beq', 
    'beqp', 
    'bne', 
    'bnep',
    'blt',
    
    'RESERVED_PTWO', 
]

# 2-operand instructions that can only take as operands the first 8 registers in each bank
p2_instructions = [
    'sysinfo',
    'in', 
    'out', 
    'inp', 
    'outp', 
    'devop', 
    'RESERVED_P1_6'
    'RESERVED_PONE'
]

# 1-operand instructions that can only take as operand on the first 8 registers in each bank
p1_instructions = [
    'lki',
    'lkp',
    'mfree', 
    'lpc',
    'jt', 
    'jy',
    'fence',
    'RESERVED_PZERO',
]

# 0-operand instructions
p0_instructions = [
    'break',
    'RESERVED_P0_1', 
    'RESERVED_P0_2', 
    'RESERVED_P0_3', 
    'RESERVED_P0_4', 
    'RESERVED_P0_5', 
    'RESERVED_P0_6', 
    'RESERVED_P0_7', 
]
    


# 2-operand instructions that can take as operand any of the 16 registers in each bank
r2_instructions = [
    # MOVs/copies between registers
    'cp',
    'cpp',
    # MOVs/copies between registers and stack
    'crsi', # copy to register from stack, integer
    'crsp', # copy to register from stack, integer
    'csri', # copy to stack from register, integer
    'csrp', # copy to stack from register, pointer

    # loads
    'lb', 
    'lbu', 
    'lh', 
    'lhu', 
    'lp',
    'lpa',  # load pointer, atomic, sequential consistency. Must be aligned. Like a C atomic_load_explicit with memory_order_seq_cst.
    'lprc', # load pointer, atomic, release consistency RCpc (acquire). Must be aligned. Like a C atomic_load_explicit with memory_order_acquire.
    'lprlx', # load pointer, atomic, unordered. Must be aligned. Like a C atomic_load_explicit with memory_order_relaxed.
    'lw',
    'lwa',   # load word, atomic, sequential consistency. Must be aligned. Like a C atomic_load_explicit with memory_order_seq_cst.
    'lwrc',  # load word, atomic,  release consistency RCpc (acquire). Must be aligned. Like a C atomic_load_explicit with memory_order_acquire.
    'lwrlx',  # load word, atomic, unordered. Must be aligned. Like a C atomic_load_explicit with memory_order_relaxed.

    'malloc_local',
    
    # stores
    'sb', 
    'sh', 
    'sp',
    'spa',  # store pointer, atomic, sequential consistency. Must be aligned. Like a C atomic_store_explicit with memory_order_seq_cst.
    'sprc', # store pointer, atomic, release consistency RCpc (release). Must be aligned. Like a C atomic_store_explicit with memory_order_release.
    'sprlx', # store pointer, atomic, unordered. Must be aligned. Like a C atomic_store_explicit with memory_order_relaxed.
    'sw',

    # swaps
    'swapi', # swap two registers, integer (note: if both register are $1, this swaps the top two stack locs)
    'swapp', # swap two registers, pointer (note: if both register are &1, this swaps the top two stack locs)

    # more stores
    'swa',  # store word, atomic,  sequential consistency. Must be aligned. Like a C atomic_store_explicit with memory_order_seq_cst.
    'swrc', # store word, atomic,  release consistency RCpc (release). Must be aligned. Like a C atomic_store_explicit with memory_order_release.
    'swrlx', # store word, atomic, unordered. Must be aligned. Like a C atomic_store_explicit with memory_order_relaxed.

    'RESERVED_RONE',
]

# 1-operand instructions that can take as operand any of the 16 registers in each bank
r1_instructions = [
    'xrets0',
    'xretsi',
    'xretsp',
    'xretr5',
    'xretr12',
    'xcalls0',
    'xcallsi',
    'xcallsp',
    'xcallsii',
    'xcallspp',
    'xcallsip',
    'xcallspi',
    'xcallr5',
    'xcallr12',
    'xcallrv',
    'RESERVED_RZERO',
]


# 0-operand instructions
r0_instructions = [
    'xentrys0',
    'xentrysi',
    'xentrysp',
    'xentrysii',
    'xentryspp',
    'xentrysip',
    'xentryspi',
    'xentryr5',
    'xentryr12',
    'xentryrv',
    'xpostcalls0',
    'xpostcallsi',
    'xpostcallsp',
    'xpostcallr5',
    'xpostcallr12',
    'halt',
]    
    
instruction_mnemonics = i11_instructions + i8_instructions + p3_instructions + p2_instructions + p1_instructions + p0_instructions + ['RESERVED_60','RESERVED_61','RESERVED_62','RESERVED_63',] + r2_instructions + r1_instructions + r0_instructions

branch_instructions = [
    'blt', 
    'bne', 
    'beq', 
    'beqp', 
    ]

alias_instructions = [
    'bad0',  # the all-zero bit-pattern; same as JPC 0
    'bad1',  # the all-one bit-pattern; currently RESERVED (b/c the encoding length format bit would have to be 1)
    ]

    
Instruction_id = enum.Enum('Instruction_id', instruction_mnemonics +
                               alias_instructions,
                               module=__name__)

instruction_ids  = [Instruction_id[name] for name in instruction_mnemonics]
instruction_id_to_opcode = dict(zip(instruction_ids, range(len(instruction_ids))))
opcode_to_instruction_id = dict(zip(range(len(instruction_ids)), instruction_ids))

branch_instruction_ids = [Instruction_id[name] for name in branch_instructions]
i11_instruction_ids = [Instruction_id[name] for name in i11_instructions]
i8_instruction_ids = [Instruction_id[name] for name in i8_instructions]


# from https://stackoverflow.com/questions/1604464/twos-complement-in-python
def unsigned_to_signed(uval, bits):
    """compute the 2's complement of int value val"""
    if (uval & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        return uval - (1 << bits)       # compute negative value
    else:
        return uval                     # return positive value as is

def signed_to_unsigned(sval, bits):
    if sval < 0:
        return sval + (1 << bits)
    else:
        return sval
    
