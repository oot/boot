from constants import *

def encode_boot_instruction(instruction: Boot_instruction):
    opcode = instruction_id_to_opcode[instruction.instruction_id]

    instruction_id, op0, op1, op2 = instruction
    
    if instruction_id in i11_instruction_ids:
        return (signed_to_unsigned(op0, 11)*2**16 +
                opcode)
    
    if instruction_id in i8_instruction_ids:
        op1 = signed_to_unsigned(op1, 8)
    
    elif instruction_id in branch_instruction_ids:
        # branch instructions have a special encoding for branch offsets
        if (instruction.op2 < -6) or (instruction.op2 > 4) or (instruction.op2 == -2) or (instruction.op2 == -1) or (instruction.op2 == 0):
            raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: branch instruction op2 is {instruction.op2} which is not in the range [-6, -5, -4, -3, 1, 2, 3, 4]")
        op2 = [0, 1, 2, 3, -1, -1, -1, 4, 5, 6, 7][instruction.op2+6]
        assert op2 != -1
        check_boot_instruction_operands(instruction, (0,7), (0,7), (-6,4))
    elif opcode < 64:
        check_boot_instruction_operands(instruction, (0,7), (0,7), (0,7), enforce_not_none=False)
    else:
        check_boot_instruction_operands(instruction, (0,15), (0,15), None, enforce_not_none=False)

    if op0 == None:
        op0 = 0
    if op1 == None:
        op1 = 0
    if op2 == None:
        op2 = 0
        
    return (op0*2**24 +
            op1*2**16    +
            op2*2**8     +
            opcode)

            
        

def decode_boot_instruction(x: int): # x is a 32-bit unsigned integer
    rest = x
    opcode = rest & 255
    instruction_id = opcode_to_instruction_id[opcode]
    rest = rest >> 8
    
    op2 = rest & 255
    rest = rest >> 8
    
    op1 = rest & 255
    rest = rest >> 8
    
    op0 = rest

    if instruction_id in branch_instruction_ids:
        # branch instructions have a special encoding for branch offsets
        op2 = [-6, None, -4, None, None, 2, None, 4][op2]
        assert op2 is not None

    if instruction_id in i11_instruction_ids:
        op0 = unsigned_to_signed(op1 + op0*256, 11)
        op1 = None
        op2 = None
    
    return Boot_instruction(instruction_id, op0, op1, op2)



def check_boot_instruction_operands(instruction: Boot_instruction, op0_range_inclusive=None, op1_range_inclusive=None, op2_range_inclusive=None, enforce_not_none=True):
    check_boot_instruction_operand(instruction, 'op0', instruction.op0, op0_range_inclusive, enforce_not_none=enforce_not_none)
    check_boot_instruction_operand(instruction, 'op1', instruction.op1, op1_range_inclusive, enforce_not_none=enforce_not_none)
    check_boot_instruction_operand(instruction, 'op2', instruction.op2, op2_range_inclusive, enforce_not_none=enforce_not_none)
    
def check_boot_instruction_operand(instruction: Boot_instruction, operand_name, operand_value, operand_range_inclusive, enforce_not_none=True):
    if operand_range_inclusive is None:
        if operand_value is not None:
            raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: instruction.{operand_name} should be None but is {operand_value}")
    else:
        if (not enforce_not_none) and (operand_value is None):
            return
        if (operand_value is None) or (operand_value < operand_range_inclusive[0]) or (operand_value > operand_range_inclusive[1]):
            raise BootOperandOutOfRange(f"encode_boot_instruction {instruction}: instruction.{operand_name} is {operand_value} which is not in the range [{operand_range_inclusive[0]}, {operand_range_inclusive[1]}]")


